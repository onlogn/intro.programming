﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab1703
{
    class DynArray<T>
    {
        private T[] _items;
        private int _size;
        private static readonly T[] _emptyArr = new T[0];
        private const int DefaultCapacity = 4;
        public int Count => _size;

        public int Capacity
        {
            get { return _items.Length; }
            set
            {
                if (value < _size)
                {
                    throw new ArgumentOutOfRangeException("Capacity < _size");
                } 
                T[] tmp = new T[value];
                Array.Copy(_items,tmp,_items.Length);
                _items = tmp;
            }
        }

        public T this[int idx]
        {
            get
            {
                if (idx < _size)
                {
                    return _items[idx];
                }
                throw new ArgumentOutOfRangeException(); 
                
            }
            set
            {
                if (idx < _size)
                {
                    _items[idx] = value;
                } else throw new ArgumentOutOfRangeException();
            }
        }

        public DynArray()
        {
            _items = _emptyArr;
        }

        public DynArray(int size)
        {
            _items = new T[size];
        }

        public DynArray(T[] elements)
        {
            _size = elements.Length;
            Array.Copy(elements,_items,_size);
        }

        public void Add(T item)
        {
            EnsureCapacity(_size + 1);
            _items[_size++] = item;
        }

        private void EnsureCapacity(int min)
        {
            if (_items.Length < min)
            {
                int newCapacity = _items.Length == 0 ? DefaultCapacity : 2 * _items.Length;
                Capacity = newCapacity;
            }

        }


        public void Reverse()
        {
            Array.Reverse(_items,0,_size);
        }

        public override string ToString()
        {
            return String.Join(" ",_items.Take(_size));
        }
    }
}
