﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1120
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;

            while ((str = Console.ReadLine()) != "0 0")
            {

                string[] s = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                s[1] = s[1].Replace(s[0], "").TrimStart(new char[] {'0'});

                //s[1] = s[1].Remove(0, IdxZero(s[1]));

                Console.WriteLine(String.IsNullOrEmpty(s[1]) ? "0" : s[1]);
            }
        }

        //public static int IdxZero(string s)
        //{
        //    int ix = 0;

        //    foreach (char c in s)
        //    {
        //        if (c == '0')
        //        {
        //            ix++;
        //        }
        //        else break;
        //    }

        //    return ix;
        //}
    }
}
