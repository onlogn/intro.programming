﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem112151
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            string[] s = str.Split(new char[] {' '},StringSplitOptions.RemoveEmptyEntries);

            int a = int.Parse(s[0]);
            int b = int.Parse(s[1]);

            Console.WriteLine($"{a} {a+1} {a+2} {a+3} {b}");

        }
    }
}
