﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1138
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;

            while ((str = Console.ReadLine()) != "0 0")
            {
                Dictionary<char, int> res = new Dictionary<char, int>()
                {
                    {'0',0},
                    {'1',0},
                    {'2',0},
                    {'3',0},
                    {'4',0},
                    {'5',0},
                    {'6',0},
                    {'7',0},
                    {'8',0},
                    {'9',0}
                };
               
                int[] arr = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
                string tmp;
                for (int i = arr[0]; i <= arr[1]; i++)
                {
                    tmp = i.ToString();
                    foreach (var v in tmp)
                    {
                        res[v]++;
                    }
                }
                Console.WriteLine(String.Join(" ",res.Values));
            }
        }
    }
}
