﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2507
{
    class Program
    {
        static void Main(string[] args)
        {
            var secretPlace = new Dictionary<GameCell, Dictionary<string, int>>()
            {
                {
                    new GameCell {Row = 2, Col = 3},
                    new Dictionary<string, int> {{"bread", 10}, {"ak-47", 5}, {"water", 3}}
                },
                {
                    new GameCell {Row = 0, Col = 0},
                    new Dictionary<string, int> {{"medic", 5}, {"sniperka", 2}}
                }

            };

            string cmd;
            while ((cmd = Console.ReadLine()) != null)
            {
                switch (cmd)
                {
                    case "?":
                        CheckSecretPlace(secretPlace);
                        break;
                    case "#": 
                        PrintSecret(secretPlace);
                        break;
                    case "+":
                        AddToSecretPace(secretPlace);
                        break;
                }
            }

            
        }

        private static void AddToSecretPace(Dictionary<GameCell, Dictionary<string, int>> secretPlace)
        {
            int[] coords = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse)
                .ToArray();
            int row = coords[0];
            int column = coords[1];

            GameCell cell = new GameCell { Row = row, Col = column };

            string[] items = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => s.ToLower()).ToArray();

            if (secretPlace.ContainsKey(cell))
            {
                Dictionary<string, int> content = secretPlace[cell];
                foreach (string item in items)
                {
                    content[item] = content.TryGetValue(item, out int v) ? v + 1 : 1;
                }
                
            }
            else
            {
                var content = new Dictionary<string,int>();
                secretPlace.Add(cell,new Dictionary<string, int>());
                foreach (string item in items)
                {
                    content[item] = content.TryGetValue(item, out int v) ? v + 1 : 1;
                }
            }
        }

        private static void CheckSecretPlace(Dictionary<GameCell, Dictionary<string, int>> secretPlace)
        {
            int[] coords = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse)
                .ToArray();
            int row = coords[0];
            int column = coords[1];

            GameCell cell = new GameCell {Row = row, Col = column};
            if (secretPlace.ContainsKey(cell))
            {
                Dictionary<string, int> items = secretPlace[cell];
                foreach (var it in items)
                {
                    Console.WriteLine($"{it.Key}: {it.Value}");
                }
            }
            else
            {
                Console.WriteLine("Nothing here");
            }
        }

        private static void PrintSecret(Dictionary<GameCell, Dictionary<string, int>> secretPlace)
        {
            foreach (KeyValuePair<GameCell, Dictionary<string, int>> currentCell in secretPlace)
            {
                Console.WriteLine(currentCell.Key+" => ");
                foreach (var item in currentCell.Value)
                {
                    Console.WriteLine($"{item.Key}: {item.Value}");
                }
            }
        }
    }

}
