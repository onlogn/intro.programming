﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1367
{
    class Program
    {
        static void Main(string[] args)
        {
            int numT;
            while((numT = int.Parse(Console.ReadLine()))!=0)
            {
                int sum = 0;
                int counter = 0;
                Dictionary<char,int> d = new Dictionary<char, int>();
                for (int ix = 0; ix < numT; ix++)
                {
                    string str = Console.ReadLine();
                    string[] res = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                    char x = res[0][0];
                    if (!d.ContainsKey(x))
                    {
                        d[x] = 0;
                    }

                    if (res[2] == "incorrect")
                    {
                        d[x] += 20;
                    }
                    else
                    {
                        d[x] += int.Parse(res[1]);
                        sum += d[x];
                        counter++;
                    }
                }
                Console.WriteLine($"{counter} {sum}");
            }
        }
    }
}
