﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1253
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTests = int.Parse(Console.ReadLine());

            for (int ix = 0; ix < numOfTests; ix++)
            {
                string str = Console.ReadLine().Trim();
                int n = int.Parse(Console.ReadLine());

                Console.WriteLine(String.Join("", str.Select(s => (s - n - 65 < 0) ? (char)(s - n + 25 + 1) : (char)(s - n))));
            }
        }
    }
}
