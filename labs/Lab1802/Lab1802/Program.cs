﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lab1802
{
    class Program
    {
        static void Main(string[] args)
        {
            const int n = 1000000;

            Console.WriteLine($"Heap: {GC.GetTotalMemory(false)}");

            Stopwatch sw = Stopwatch.StartNew();
            long s1 = 0;
            for (int i = 0; i < n; i++)
            {
                RefType r = new RefType(i, i + 1);
                s1 += r.Compute();
            }
            sw.Stop();
            Console.WriteLine($"Sum = {s1}. Time = {sw.ElapsedMilliseconds}");
            Console.WriteLine($"Heap: {GC.GetTotalMemory(false)}");

            Stopwatch sw2 = Stopwatch.StartNew();
            long s2 = 0;
            for (int i = 0; i < n; i++)
            {
                ValType r = new ValType(i, i + 1);
                s2 += r.Compute();
            }
            sw2.Stop();
            Console.WriteLine($"Sum = {s1}. Time = {sw2.ElapsedMilliseconds}");

            Console.WriteLine($"Heap: {GC.GetTotalMemory(false)}");
        }
    }

    public class RefType
    {
        public int Field1;
        public int Field2;

        public RefType(int f1, int f2)
        {
            Field1 = f1;
            Field2 = f2;
        }

        public long Compute()
        {
            return (long)Field1 + Field2;
        }
    }

    public struct ValType
    {
        public int Field1;
        public int Field2;

        public ValType(int f1, int f2)
        {
            Field1 = f1;
            Field2 = f2;
        }

        public long Compute()
        {
            return (long)Field1 + Field2;
        }
    }
}
