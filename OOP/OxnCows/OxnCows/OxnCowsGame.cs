﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace OxnCows
{
    internal class OxnCowsGame
    {
        private int _oxes;

        private List<int> _resNum;

        private readonly Regex r = new Regex(@"\d{4}");

        public OxnCowsGame()
        {
            GenerateNumber();
        }

        public int NumberOfTries { get; private set; }

        private void GenerateNumber()
        {
            _resNum = new List<int>();
            List<int> src = Enumerable.Range(0, 10).ToList();
            Random rnd = new Random();
            for (int i = 0; i < 4; i++)
            {
                int idx = rnd.Next(0, src.Count);
                _resNum.Add(src[idx]);
                src.RemoveAt(idx);
            }
        }

        public Tuple<int, int> CheckNumber(List<int> numbers)
        {
            int ox = 0;
            int cow = 0;

            _oxes = 0;

            for (int i = 0; i < numbers.Count; i++)
                if (numbers[i] == _resNum[i])
                    ox++;
                else if (_resNum.Contains(numbers[i])) cow++;

            _oxes = ox;
            NumberOfTries++;
            return Tuple.Create(ox, cow);
        }

        public bool IsGameOver()
        {
            if (_oxes == 4) return true;
            return false;
        }

        public bool IsCorrectData(string str)
        {
            return r.IsMatch(str) && str.Length == 4 && str.Distinct().Count() == 4;
        }

        public string GetNumber()
        {
            return string.Join("", _resNum);
        }
    }
}