﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3062
{
    class Program
    {
        static void Main(string[] args)
        {
            double x = double.Parse(Console.ReadLine());
            double y = double.Parse(Console.ReadLine());
            int cnt = 1;

            while (x < y)
            {
                cnt++;
                x = x * 1.1;
            }
            Console.WriteLine(cnt);
        }
    }
}
