﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab0805
{
    public partial class Form1 : Form
    {
        List<Ball> balls = new List<Ball>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (var i in balls)
            {
                i.Check(Width, Height);
                i.MoveBall();
            }

            Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            balls.Add(new Ball(Width / 2, Height / 2, 30, 1, 1, Color.FromArgb(18, 178, 117)));
            balls.Add(new Ball(Width / 2 - 40, Height / 2 - 40, 30, 1, 1, Color.FromArgb(198, 0, 255)));
            balls.Add(new Ball(Width / 2 - 80, Height / 2 - 80, 30, 1, 1, Color.FromArgb(204, 149, 20)));
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            foreach (var i in balls)
            {
                Brush b1 = new SolidBrush(i.BallColor);
                g.FillEllipse(b1, i.X, i.Y, i.Diameter, i.Diameter);
            }

        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                balls.Add(Ball.GenerateRandomBall(e.X, e.Y));
            }

            if (e.Button == MouseButtons.Right)
            {
                for (int i = 0; i < balls.Count; i++)
                {
                    if (balls[i].IsInRange(e.X, e.Y))
                    {
                        balls.RemoveAt(i);
                        break;
                    }
                }
            }
        }
    }
}
