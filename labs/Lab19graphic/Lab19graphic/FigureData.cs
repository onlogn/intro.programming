﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace Lab19graphic
{
    [DataContract]
    public class FigureData
    {
        [DataMember]
        public int X { get; set; }

        [DataMember]
        public int Y { get; set; }

        [DataMember]
        public int Width { get; set; }

        [DataMember]
        public int Height { get; set; }

        [DataMember]
        public Color Color { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        public Type FigureType
        {
            get
            {
                return Type.GetType(TypeName);
            }
            set
            {
                TypeName = value.FullName;
            }
        }

        public FigureData()
        {
            
        }
    }
}
