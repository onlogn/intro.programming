﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1104
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            while ((s = Console.ReadLine()) != "0 0")
            {
                int[] a = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).Distinct().ToArray();
                int[] b = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Distinct().Select(int.Parse)
                    .ToArray();
                Console.WriteLine(Math.Min(a.Except(b).Count(), b.Except(a).Count()));
            }
        }
    }
}
