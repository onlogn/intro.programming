﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using Library.DataBaseModels;

namespace Library.DataBaseWork
{
    public static class UserDataBaseService
    {
        public static async Task<List<User>> GetUsersList()
        {
            return await GetUsersList(-1);
        }

        public static async Task<List<User>> GetUsersList(int categoryId)
        {
            List<UserData> tmp;
            using (LibraryContext db = new LibraryContext())
            {
                //tmp = await db.Users.Include(i => i.Categories).Where(u => u.Categories.Any(c=>c.Id == categoryId)).ToListAsync();
                var x = db.Users.Include(i => i.Categories);
                if (categoryId > 0)
                {
                    x = x.Where(u => u.Categories.Any(c => c.Id == categoryId));
                }

                tmp = await x.ToListAsync();
            }

            return tmp.Select(item => new User(item.Id, item.Name,
                item.Categories.Select(i => new Category(i.Id, i.Name)).ToList(), item.MessageType)).ToList();
        }

        public static async Task<int> AddUser(User u)
        {
            using (LibraryContext db = new LibraryContext())
            {
                UserData tmpUser = new UserData();
                tmpUser.Name = u.Name;
                tmpUser.MessageType = u.MessageType;
                //u.Categories (1,2,4)
                //SELECT * FROM CAtegories WHERE ID IN(1,2,4)
                //tmpUser.Categories = u.Categories.Select(i =>
                //{
                //    var c = new CategoryData() {Id = i.Id};
                //    db.Categories.Attach(c);

                //    return c;
                //}).ToList();
                int[] arrOfCatId = u.Categories.Select(i => i.Id).ToArray();
                tmpUser.Categories = await db.Categories.Where(item => arrOfCatId.Contains(item.Id)).ToListAsync();

                db.Users.Add(tmpUser);
                await db.SaveChangesAsync();
                return tmpUser.Id;
            }
        }

        public static async Task EditUser(User currentUser)
        {
            using (LibraryContext db = new LibraryContext())
            {
                UserData user = await db.Users.Include(u => u.Categories).FirstAsync(item => item.Id == currentUser.Id);
                if(user == null) throw new Exception("Current user is not exist!");
                user.Name = currentUser.Name;
                user.MessageType = currentUser.MessageType;
            
                user.Categories.Clear();
                List<int> arrOfCatId = currentUser.Categories.Select(i => i.Id).ToList();
                List<CategoryData> tmp = await db.Categories.Where(item => arrOfCatId.Contains(item.Id)).ToListAsync();
                foreach (var v in tmp)
                {
                    user.Categories.Add(v);
                }
                

                await db.SaveChangesAsync();

            }
        }

        public static async Task DeleteUser(User user)
        {
            using (LibraryContext db = new LibraryContext())
            {
                UserData res = await db.Users.FirstOrDefaultAsync(u => u.Id == user.Id);
                if (res == null) return;
                db.Users.Remove(res);
                await db.SaveChangesAsync();
            }
        }
    }
}
