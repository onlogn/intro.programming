﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0302
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("From: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("To: ");
            int b = int.Parse(Console.ReadLine());
            Console.Write("Input number: ");
            int x = int.Parse(Console.ReadLine());

            bool isInRange = x >= a && x <= b;

            string s = isInRange ? "in" : "not in";

            Console.WriteLine($"Number {x} {s} [{a};{b}], {isInRange}");
            
  
        }
    }
}
