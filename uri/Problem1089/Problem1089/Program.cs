﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1089
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1;
            while ((str1 = Console.ReadLine()) != "0")
            {
                string s = Console.ReadLine();
                string[] sr = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                var arr = sr.Select(i => int.Parse(i)).ToArray();

                int cnt = 0;

                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[(i + 1) % arr.Length] > arr[i] && arr[(i + 1) % arr.Length] > arr[(i + 2) % arr.Length])
                    {
                        cnt++;
                    }

                    if (arr[(i + 1) % arr.Length] < arr[i] && arr[(i + 1) % arr.Length] < arr[(i + 2) % arr.Length])
                    {
                        cnt++;
                    }

                }

                Console.WriteLine(cnt);
            }
        }
    }
}
