﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3070
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            List<int> nums = new List<int>();
            while ((s = Console.ReadLine()) != "0")
            {
                nums.Add(int.Parse(s));
            }

            var res = nums.OrderByDescending(i => i).Skip(1).Take(1).ToArray();
            Console.WriteLine(res[0]);
        }
    }
}
