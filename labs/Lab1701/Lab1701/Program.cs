﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1701
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[0];
            int size = 0;
            const int DefaultCapacity = 4;

            string str;
            while ((str = Console.ReadLine()) != null)
            {
                int x = int.Parse(str);

                if (size == arr.Length)
                {
                    int[] tmp = new int[size == 0 ? DefaultCapacity : arr.Length * 2];
                    for (int i = 0; i < size; i++)
                    {
                        tmp[i] = arr[i];
                    }

                    arr = tmp;
                }
                arr[size] = x;
                size++;
            }

            for (int i = 0; i < size / 2; i++)
            {
                int tmp = arr[i];
                int idx = size - i - 1;
                arr[i] = arr[idx];
                arr[idx] = tmp;
            }

            for (int i = 0; i < size; i++)
            {
                Console.Write($"{arr[i]} ");
            }

            
        }
    }
}
