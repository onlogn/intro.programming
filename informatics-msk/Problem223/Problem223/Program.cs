﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem223
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();
            string str = Console.ReadLine();
            int a = int.Parse(Console.ReadLine());

            string[] inputArr = str.Split(' ');

            var res = inputArr.Select(s=>int.Parse(s)).Where(s=>s==a).Count();

            Console.WriteLine(res);

        }
    }
}
