﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1110
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;

            while ((n = int.Parse(Console.ReadLine())) != 0)
            {

                Queue<int> q = new Queue<int>(Enumerable.Range(1, n));

                List<int> discardedCards = new List<int>();
                int tmp;

                while (q.Count != 1)
                {
                    discardedCards.Add(q.Dequeue());
                    tmp = q.Dequeue();
                    q.Enqueue(tmp);
                }

                int remainingCard = q.Peek();

                Console.WriteLine($"Discarded cards: {discardedCards.ArrToString()}");
                Console.WriteLine($"Remaining card: {remainingCard}");
            }
        }
    }

    public static class StringExtension
    {
        public static string ArrToString(this IEnumerable<int> ie)
        {
            return String.Join(", ", ie);
        }
    }
}
