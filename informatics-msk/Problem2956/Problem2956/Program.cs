﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem2956
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder str = new StringBuilder(Console.ReadLine());

            int len = str.Length;
            if (str.Length < 4)
            {
                for (int i = 0; i < 4 - len; i++)
                {
                    str.Insert(0,"0");
                }
            }
            bool res = true;
           

            for (int i = 0; i < str.Length / 2; i++)
            {
                if (str[i] != str[str.Length - i - 1])
                {
                    res = false;
                    break;
                }

            }
            Console.WriteLine(res ? "1" : "2");
        }
    }
}
