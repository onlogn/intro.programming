﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1302
{
    class Program
    {
        static void Main(string[] args)
        {
            int c;
            Console.WriteLine("Input char (Ctrl+Z): ");
            while ((c = Console.Read()) != -1)
            {
                if (Char.IsLetter((char) c))
                {
                    Console.WriteLine($"It's letter {c}");
                    Console.WriteLine($"lowercase: {Char.ToLower((char)c)}");
                    Console.WriteLine($"uppercase: {Char.ToUpper((char)c)}");
                }
                else if(Char.IsDigit((char)c))
                {
                    Console.WriteLine($"It's digit {c}");
                }
                else if (Char.IsControl((char) c))
                {
                    Console.WriteLine($"It's control symbol {c}");
                }
                else
                {
                    Console.WriteLine($"Something other");
                }
            }
        }
    }
}
