﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DataBaseWork;

namespace Library
{
    public class UsersNotifier
    {
        public event EventHandler<UsersBooksEventArgs> BookForUserAdded;

        public async void OnBookAdded(object sender, AddBookEventArgs e)
        {
            int c = e.Book.Category.Id;
            List<User> usersList = await UserDataBaseService.GetUsersList(c);
            foreach (User user in usersList)
            {
                UsersBooksEventArgs tmp = new UsersBooksEventArgs() {User = user, Book = e.Book};
                OnBookForUserAdded(tmp);
            }
        }

        protected virtual void OnBookForUserAdded(UsersBooksEventArgs e)
        {
            BookForUserAdded?.Invoke(this, e);
        }
    }
}
