﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1003
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int[] a = new int[n];
            int[] b = new int[n];
            int[] c = new int[n];
            int cnt = 0;

            Random rnd = new Random();

            for (int i = 0; i < n; i++)
            {
                a[i] = rnd.Next(-100, 101);
                b[i] = rnd.Next(-100, 101);
                Console.Write($"{a[i]} + {b[i]} = ");
                c[i] = int.Parse(Console.ReadLine());
            }

            Console.Clear();
            Console.WriteLine("Your results: ");

            for (int i = 0; i < n; i++)
            {
                int res = a[i] + b[i];
                if (res == c[i])
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{a[i]} + {b[i]} == {c[i]}");
                    cnt++;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{a[i]} + {b[i]} == {c[i]} ({res})");
                }
            }
            Console.ResetColor();
            Console.WriteLine($"right = {cnt}, mistakes = {n - cnt}");
        }
    }
}
