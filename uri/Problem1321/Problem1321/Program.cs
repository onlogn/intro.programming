﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1321
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            while ((s = Console.ReadLine()) != "0 0 0 0 0")
            {
                List<int> cards = new List<int>();
                cards.AddRange(s.Split(new []{' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse));

                List<int> princess = new List<int>();
                List<int> prince = new List<int>();
                for (int i = 0; i < 3; i++)
                {
                    princess.Add(cards[i]);
                }
                

                princess = princess.OrderByDescending(i=> i).ToList();
                bool flag = true;
                for (int i = 1; i <= 52; i++)
                {
                    prince.Clear();
                    for (int k = 3; k < 5; k++)
                    {
                        prince.Add(cards[k]);
                    }
                    if (!cards.Contains(i)) 
                    {
                        prince.Add(i);
                        prince = prince.OrderByDescending(c => c).ToList();
                        if (IsPrinceWin(princess, prince))
                        {
                            Console.WriteLine(i);
                            flag = false;
                            break;
                        } 
                    }
                }

                if (flag)
                {
                    Console.WriteLine("-1");
                }

                
            }
        }

        private static bool IsPrinceWin(List<int> sister, List<int> brother)
        {
            int cnt = 0;

            for (int i = 0; i < 3; i++)
            {
                bool flag = true;
                for (int j = 0; j < brother.Count; j++)
                {
                    if (brother[j] < sister[i])
                    {
                        brother.RemoveAt(j);
                        flag = false;
                        break;
                    }
                }

                if (flag)
                {
                    cnt++;
                    brother.RemoveAt(0);
                }
            }

            return cnt >= 2;
        }
    }
}
