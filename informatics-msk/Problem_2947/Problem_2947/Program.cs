﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_2947
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int hh = n / 60 % 24;
            int mm = n % 60 % 60;
            
            Console.WriteLine($"{hh} {mm}");
        }
    }
}
