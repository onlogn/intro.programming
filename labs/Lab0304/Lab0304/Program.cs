﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Lab0304
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = int.Parse(Console.ReadLine());
            int y = int.Parse(Console.ReadLine());
            
            bool isInRectangle = (x >= 40 && x <= 100) & (y >= 50 && y <= 100);
            string s = isInRectangle ? "in" : "not in";

            Console.WriteLine($"Point ({x};{y}) is {s} Rectangle");

        }
    }
}
