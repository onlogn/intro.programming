﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game135
{
    public partial class MainForm : Form
    {
        private Game15 _game;
        private int _size;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _size = 4;
            NewGame(_size);  
        }

        private void NewGame(int d)
        {
            _game = new Game15(d);
            _game.Shuffle(10000);
            panelField.Controls.Clear();
            GenerateField(_size, Game15.ItemSize);
            UpdateField();
            this.Refresh();
        }

        private void GenerateField(int dimension, int step)
        {
            int c = 1;
            panelField.Width = panelField.Height = step * dimension;
            panelField.Location = new Point(this.ClientSize.Width / 2 - panelField.Width / 2, this.ClientSize.Height / 2 - panelField.Height / 2 - 15);
            for (int i = 0; i < dimension; i++)
            {
                for (int j = 0; j < dimension; j++)
                {
                    PictureBox pic = new PictureBox
                    {
                        Location = new Point(j * step, i * step),
                        Anchor = AnchorStyles.None,
                        Width = step,
                        Height = step,
                        Tag = new Position(i,j)
                    };
                    pic.Click += PictureBox_Click;
                    panelField.Controls.Add(pic);
                }
            }
        }

        private void UpdateField()
        {
            int idx = 0;
            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    panelField.Controls[idx++].BackgroundImage = GetImage(i,j);
                }
            }

            lblInfo.Text = $"Количество шагов: {_game.Counter}";
            panelField.Refresh();
        }

        private Image GetImage(int row, int col)
        {
            if (_game.Elements[row,col] == _game.EmptyCell)
            {
                return Properties.Resources.Empty;
            }
            return (Image)Properties.Resources.ResourceManager.GetObject($"_{_game.Elements[row,col]}");
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            _game.Move((Position)c.Tag);
            UpdateField();
            if (_game.IsGameOver)
            {
                MessageBox.Show($"Поздравляю! Ты прошел игру! Кол-во ходов: {_game.Counter}");
            }
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            NewGame(_size);
        }

        private void MenuItemClick(object sender, EventArgs e)
        {
            ToolStripMenuItem t = (ToolStripMenuItem)sender;

            foreach (var i in mnuDifficulty.DropDownItems)
            {
                ((ToolStripMenuItem) i).Checked = false;
            }

            t.Checked = true;
            _size = int.Parse(t.Tag.ToString());
            NewGame(_size);
        }
    }
}
