﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1050
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            string s;
            Dictionary<int,string> d = new Dictionary<int, string>(5)
            {
                {61, "Brasilia"}, {71, "Salvador"}, {11, "Sao Paulo"}, {21, "Rio de Janeiro"},
                {32, "Juiz de Fora"}, {19, "Campinas"}, {27, "Vitoria"}, {31, "Belo Horizonte"}
            };

            Console.WriteLine(d.ContainsKey(n) ? d[n] : "DDD nao cadastrado");
        }
    }
}
