﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1103
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] a = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = rnd.Next();
            }
            
            Stopwatch t = new Stopwatch();

            t.Start();
            SelectSort(a);
            t.Stop();
            Console.WriteLine(t.ElapsedMilliseconds);

            int[] b = new int[a.Length];
            Array.Copy(a,b,a.Length);

            t.Reset();
            t.Start();
            Array.Sort(b);
            Console.WriteLine(t.ElapsedMilliseconds);
        }

        static void SelectSort(int[] arr)
        {
            if (arr == null)
            {
                throw new ArgumentNullException("SelectSort - Array is NULL");
            }

            for (int i = 0; i < arr.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[j] < arr[min])
                    {
                        min = j;
                    }
                }

                int tmp = arr[i];
                arr[i] = arr[min];
                arr[min] = tmp;
            }
        }
    }
}
