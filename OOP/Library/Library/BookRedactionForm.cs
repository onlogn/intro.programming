﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.DataBaseWork;

namespace Library
{
    public partial class BookRedactionForm : Form
    {
        public Book CurrentBook { get; private set; }

        public BookRedactionForm()
        {
            InitializeComponent();
        }

        public BookRedactionForm(Book b):this()
        {
            CurrentBook = new Book(b.Id, b.Name, b.Category);
            txtBookName.Text = CurrentBook.Name;
            
        }

        private async void BookRedactionForm_Load(object sender, EventArgs e)
        {
            var list = await CategoryDataBaseService.GetCategoryList();
            comboBoxBookCat.DataSource = list;
            SetComboBoxValue();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string name = txtBookName.Text.Trim();
            
            if (string.IsNullOrEmpty(name) || comboBoxBookCat.SelectedItem == null)
            {
                MessageBox.Show("You must input name and select category");
                return;
            }

            int idx = CurrentBook?.Id ?? -1;
            CurrentBook = new Book(idx,name,(Category)comboBoxBookCat.SelectedItem);
           
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void SetComboBoxValue()
        {
            if (CurrentBook != null)
                comboBoxBookCat.SelectedItem = CurrentBook.Category;
        }

    }
}
