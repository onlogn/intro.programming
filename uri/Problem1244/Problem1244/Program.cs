﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1244
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            for (int ix = 0; ix < n; ix++)
            {

                string[] res = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).ToArray();

                Console.WriteLine(String.Join(" ", res.OrderByDescending(i => i.Length)));
            }
        }
    }
}
