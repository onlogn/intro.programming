﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0105
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Input b = ");
            int b = int.Parse(Console.ReadLine());
            Console.Write("Input c = ");
            int c = int.Parse(Console.ReadLine());

            Console.WriteLine($"V = {a} * {b} * {c} = {a*b*c}");

        }
    }
}
