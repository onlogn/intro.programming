﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1502
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            Stopwatch s = new Stopwatch();

            s.Start();
            string str = String.Empty;
            for (int i = 0; i < n; i++)
            {
                str += 'a';
            }
            s.Stop();
            Console.WriteLine(s.ElapsedMilliseconds);

            s.Restart();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++)
            {
                sb.Append('a');
            }
            s.Stop();
            Console.WriteLine(s.ElapsedMilliseconds);
        }
    }
}
