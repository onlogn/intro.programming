﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab0803
{
    public partial class Form1 : Form
    {
        private int selfSize = 50;

        private int SelfSize
        {
            set
            {
                if (value >= 5 && value <= 100)
                {
                    selfSize = value;
                }
            }

            get { return selfSize; }
        }

        

        private const int zoomStep = 5;

        public Form1()
        {
            InitializeComponent();

            this.MouseWheel += Form1_MouseWheel;
        }

        private void Form1_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                SelfSize += zoomStep;
            }
            else
            {
                SelfSize -= zoomStep;
            }

            Refresh();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            
            Brush b1 = new SolidBrush(Color.FromArgb(127,0,48));
            
            Brush b2 = new SolidBrush(Color.FromArgb(255,76,144));

            int y = Height / 2 - 8 * SelfSize / 2;
            for (int j = 0; j < 8; j++)
            {
                int x = Width / 2 - 8 * SelfSize / 2;
                for (int i = 0; i < 8; i++)
                {
                    if ((i + j) % 2 == 0)
                    {
                        g.FillRectangle(b1, x, y, SelfSize, SelfSize);
                    }
                    else
                    {
                        g.FillRectangle(b2, x, y, SelfSize, SelfSize);
                    }
                    x += SelfSize;
                }
                y += SelfSize;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
