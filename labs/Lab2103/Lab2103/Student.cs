﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2001
{
    public class Student
    {
        public string Name { get; }
        public int BirthYear { get; }
        public double GPA { get;  }

        public Student(string name, int birthYear, double gpa)
        {
            Name = name;
            BirthYear = birthYear;
            GPA = gpa;
        }

        public override string ToString()
        {
            return $"{Name} - {BirthYear} ({GPA})";
        }
    }
}
