﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace ListArr
{
    public class ListArray<T>
    {
        private int _size = 1000;
        private T[] _arrOfElements;
        public int Length { get; private set; }

        public T this[int i]
        {
            get
            {
                if (i < Length)
                {
                    return _arrOfElements[i];
                }
                throw new IndexOutOfRangeException("Index is out of Range");
            }
            set
            {
                if (i < Length)
                {
                    _arrOfElements[i] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException("Index is out of Range");
                }
            }
        }

        public IEnumerator GetEnumerator()
        {
            return new ListArrEnumerator<T>(_arrOfElements,Length);
        }

        private void DoResize(int size)
        {
            if (_size < size)
            {
                while (_size < size)
                {
                    _size *= 2;
                }

                Array.Resize(ref _arrOfElements, _size);
            }
        }

        public ListArray()
        {
            _arrOfElements = new T[_size];
            Length = 0;
        }

        public ListArray(int capacity)
        {
            _arrOfElements = new T[capacity];
            _size = capacity;
            Length = 0;
        }

        public ListArray(T[] array): this()
        {
            Length = array.Length;
            DoResize(Length);
            array.CopyTo(_arrOfElements, 0);
        }

        public void Add(T item)
        {
            DoResize(Length + 1);
            _arrOfElements[Length++] = item;
        }

        public void AddRange(ICollection collection)
        {
            DoResize(Length + collection.Count);
            collection.CopyTo(_arrOfElements, Length);
            Length += collection.Count;
        }

        public int IndexOf(T item)
        {
            return IndexOf(item, 0);
        }

        public int IndexOf(T item, int startIndex)
        {
            return IndexOf(item, startIndex, Length);
        }

        public int IndexOf(T item, int startIndex, int count)
        {
            int res = -1;

            for (int i = startIndex; i < Length && i < count + startIndex; i++)
            {
                if (_arrOfElements[i].Equals(item))
                {
                    res = i;
                    break;
                }
            }

            return res;
        }

        public void Insert(int index, T item)
        {
            if (index > Length + 1)
            {
                throw new IndexOutOfRangeException("Index is out of Range");
            }
            DoResize(Length+1);
            for (int i = Length; i >= index; i--)
            {
                _arrOfElements[i + 1] = _arrOfElements[i];
            }

            ++Length;
            _arrOfElements[index] = item;
        }

        public bool Remove(T item)
        {
            int idx = IndexOf(item);

            if (idx != -1)
            {
                RemoveAt(idx);
                return true;
            }

            return false;
        }

        public void RemoveAt(int index)
        {
            RemoveAt(index, 1);
        }

        public void RemoveAt(int startidx, int count)
        {
            if (startidx + count > Length)
            {
                throw new Exception("Bound is out of range");
            }
            Length -= count;
            for (int i = startidx; i < Length; i++)
            {
                _arrOfElements[i] = _arrOfElements[i + count];
            }
        }

        public override string ToString()
        {
            return String.Join(" ",_arrOfElements.Take(Length));
        }
    }

    public class ListArrEnumerator<T> : IEnumerator
    {
        private T[] _items;
        private int _pos = -1;
        private int _len;

        public ListArrEnumerator(T[] arr, int count)
        {
            _items = arr;
            _len = count;
        }

        public Object Current
        {
            get
            {
                if (_pos == -1 || _pos >= _items.Length)
                {
                    throw new InvalidOperationException();
                }

                return _items[_pos];
            }
        }

        public bool MoveNext()
        {
            if (_pos < _len - 1)
            {
                _pos++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _pos = 0;
        }
    }
}
