﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0205
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Fahrenheit = ");
            double f = double.Parse(Console.ReadLine());
            double res = (f - 32) * 5 / 9;
            Console.WriteLine($"Fahrenheit = {res:F1} Celsius");
        }
    }
}
