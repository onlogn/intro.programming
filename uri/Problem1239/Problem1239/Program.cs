﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1239
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;

            string openItag = "<i>";
            string closeItag = "</i>";
            string openBtag = "<b>";
            string closeBtag = "</b>";

            while ((str = Console.ReadLine()) != null)
            {
                bool flagIopen = true;
                bool flagBopen = true;
                foreach (char c in str)
                {
                    if (c == '_')
                    {
                        if (flagIopen)
                        {
                            Console.Write(openItag);
                            flagIopen = false;
                        }
                        else
                        {
                            Console.Write(closeItag);
                            flagIopen = true;
                        }

                    }
                    else if (c == '*')
                    {
                        if (flagBopen)
                        {
                            Console.Write(openBtag);
                            flagBopen = false;
                        }
                        else
                        {
                            Console.Write(closeBtag);
                            flagBopen = true;
                        }
                    }
                    else
                    {
                        Console.Write(c);
                    }

                }

                Console.WriteLine();
            }


        }
    }
}
