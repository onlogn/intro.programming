﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game135
{
    public class Game15
    {
        public int[,] Elements { get; private set; }
        public int Counter { get; private set; }
        public bool IsGameOver => _winArr.Cast<int>().SequenceEqual(Elements.Cast<int>());

        public int FieldSize { get; }
        public int EmptyCell { get; }
        public const int ItemSize = 50;
        private int[,] _winArr;
        private Random _rnd = new Random();
        private readonly List<Position> _directions = new List<Position>()
        {
            new Position(-1, 0),
            new Position(1, 0),
            new Position(0, 1),
            new Position(0, -1)
        };

        public Game15(int fieldSize)
        {
            FieldSize = fieldSize;
            EmptyCell = FieldSize * FieldSize;

            Elements = new int[FieldSize, FieldSize];
            int c = 1;
            for (int i = 0; i < FieldSize; i++)
            {
                for (int j = 0; j < FieldSize; j++)
                {
                    Elements[i, j] = c++;
                }
            }
            _winArr = new int[FieldSize, FieldSize];
            Array.Copy(Elements, _winArr, FieldSize * FieldSize);
        }


        public void Move(Position current)
        {
            Position empty = FindEmptyCellPosition();
            if (current.Row == empty.Row && Math.Abs(empty.Col - current.Col) > 1)
            {
                int n = empty.Col - current.Col;
                int startIdx = n > 0 ? empty.Col - 1 : empty.Col + 1;
                int finishIdx = n > 0 ? current.Col - 1 : current.Col + 1;
                int dir = n > 0 ? -1 : 1;
                for (int i = startIdx; i != finishIdx; i += dir)
                {
                    Move(new Position(current.Row,i));
                }
            }
            
            else if (current.Col == empty.Col && Math.Abs(empty.Row - current.Row) > 1)
            {
                int n = empty.Row - current.Row;
                int startIdx = n > 0 ? empty.Row - 1 : empty.Row + 1;
                int finishIdx = n > 0 ? current.Row - 1 : current.Row + 1;
                int dir = n > 0 ? -1 : 1;
                for (int i = startIdx; i != finishIdx; i += dir)
                {
                    Move(new Position(i, current.Col));
                }
            }
            
            foreach (var d in _directions)
            {
                Position next = current + d;
                if (IsInRange(next) && Elements[next.Row, next.Col] == EmptyCell)
                {
                    Swap(current, next);
                    Counter++;
                    return;
                }
            }

        }

        public void Shuffle(int n)
        {
            for (int i = 0; i < n; i++)
            {
                Move(new Position(_rnd.Next(0, FieldSize), _rnd.Next(0, FieldSize)));
            }

            Counter = 0;
        }

        private void Swap(Position current, Position next)
        {
            int tmp = Elements[current.Row, current.Col];
            Elements[current.Row, current.Col] = Elements[next.Row, next.Col];
            Elements[next.Row, next.Col] = tmp;
        }

        private bool IsInRange(Position p)
        {
            return 0 <= p.Row && p.Row < FieldSize && 0 <= p.Col && p.Col < FieldSize;
        }

        private Position FindEmptyCellPosition()
        {
            for (int i = 0; i < FieldSize; i++)
            {
                for (int j = 0; j < FieldSize; j++)
                {
                    if(Elements[i,j] == EmptyCell)
                        return new Position(i, j);
                }
            }

            throw new Exception("Invalid data");
        }
    }
}
