﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab0902
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            DrawStar(g, Width/2, Height/2, 150, Color.FromArgb(255,106,14));
            DrawStar(g, Width/4, Height/4, 50, Color.FromArgb(25,250,255));
            DrawStar(g, Width/4, 3*Height/4, 50, Color.FromArgb(25, 250, 255));
            DrawStar(g, 3*Width/4, 3*Height/4, 50, Color.FromArgb(25, 250, 255));
            DrawStar(g, 3*Width/4, Height/4, 50, Color.FromArgb(25, 250, 255));

        }

        private static void DrawStar(Graphics g, int x, int y, int r, Color c)
        {
            Pen p = new Pen(c);

            List<Tuple<int, int>> directions = new List<Tuple<int, int>>(){
                Tuple.Create(0,r),
                Tuple.Create(0,-r),
                Tuple.Create(r,0),
                Tuple.Create(-r,0),
                Tuple.Create(-r/4,-r/4),
                Tuple.Create(r/4,r/4),
                Tuple.Create(r/4,-r/4),
                Tuple.Create(-r/4,r/4)

            };

            List<Tuple<int, int>> points = GetListOfCoordinates(x, y, directions);

            for (int i = 0; i < points.Count; i += 2)
            {
                g.DrawLine(p, points[i].Item1, points[i].Item2, points[i+1].Item1, points[i+1].Item2);
            }

            g.DrawLine(p, points[0].Item1, points[0].Item2, points[5].Item1, points[5].Item2);
            g.DrawLine(p, points[5].Item1, points[5].Item2, points[2].Item1, points[2].Item2);
            g.DrawLine(p, points[2].Item1, points[2].Item2, points[6].Item1, points[6].Item2);
            g.DrawLine(p, points[6].Item1, points[6].Item2, points[1].Item1, points[1].Item2);
            g.DrawLine(p, points[1].Item1, points[1].Item2, points[4].Item1, points[4].Item2);
            g.DrawLine(p, points[4].Item1, points[4].Item2, points[3].Item1, points[3].Item2);
            g.DrawLine(p, points[3].Item1, points[3].Item2, points[7].Item1, points[7].Item2);
            g.DrawLine(p, points[7].Item1, points[7].Item2, points[0].Item1, points[0].Item2);

        }

        private static List<Tuple<int, int>> GetListOfCoordinates(int x, int y, List<Tuple<int, int>> directions)
        {
            List<Tuple<int, int>> points = new List<Tuple<int, int>>();

            foreach (var i in directions)
            {
                points.Add(Tuple.Create(x + i.Item1, y + i.Item2));
            }

            return points;
        }
    }
}
