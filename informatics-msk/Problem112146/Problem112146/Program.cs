﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem112146
{
    class Program
    {
        static void Main(string[] args)
        {
            double r = double.Parse(Console.ReadLine());

            Console.WriteLine($"{Math.PI*r*r:f3}");
            Console.WriteLine($"{Math.PI*2*r:f3}");
        }
    }
}
