﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0805
{
    class Ball
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public int Diameter { get; private set; }
        public int Dx { get; private set; }
        public int Dy { get; private set; }
        public Color BallColor { get; private set; }

        public int CenterX
        {
            get { return X + Diameter / 2; }
        }

        public int CenterY
        {
            get { return Y + Diameter / 2; }
        }

        public Ball(int x, int y, int d, int dx, int dy, Color cl)
        {
            X = x;
            Y = y;
            Diameter = d;
            Dx = dx;
            Dy = dy;
            BallColor = cl;
        }

       
        public void MoveBall()
        {
            X += Dx;
            Y += Dy;
        }

        public void Check(int width, int height)
        {
            if (X + Dx >= width - Diameter || X + Dx <= 0)
            {
                Dx = -Dx;
            }
            if (Y + Dy >= height - Diameter || Y + Dy <= 0)
            {
                Dy = -Dy;
            }
        }

        public static Ball GenerateRandomBall(int posX, int posY)
        {
            Random rnd = new Random();

            int vx = rnd.Next(-5, 5);
            int resx = vx == 0 ? vx+=1 : vx;

            int vy = rnd.Next(-5, 5);
            int resy = vy == 0 ? vy += 1 : vy;

            return new Ball(posX, posY, 30, resx, resy,
                Color.FromArgb(rnd.Next(1, 256), rnd.Next(1, 256), rnd.Next(1, 256)));
        }

        public bool IsInRange(int x, int y)
        {
            double d = Math.Sqrt(Math.Pow(x - CenterX, 2) + Math.Pow(y - CenterY, 2));
            return d <= Diameter / 2;
        }
    }
}
