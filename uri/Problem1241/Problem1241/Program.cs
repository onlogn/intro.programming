﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1241
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());

            for (int ind = 0; ind < num; ind++)
            {
                string inputStr = Console.ReadLine();

                string[] words = inputStr.Split(new char[] {' '});

                int startIndex = words[0].Length - words[1].Length;

                if (startIndex >= 0)
                {
                    string res = words[0].Substring(startIndex);

                    if (res == words[1])
                    {
                        Console.WriteLine("encaixa");
                    }
                    else
                    {
                        Console.WriteLine("nao encaixa");
                    }
                }
                else
                {
                    Console.WriteLine("nao encaixa");
                }
            }
            
        }
    }
}
