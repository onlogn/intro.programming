﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DataBaseModels;

namespace Library.DataBaseWork
{
    public static class LogService
    {
        public static async Task WriteLog(Book book, User user)
        {
            using (LibraryContext db = new LibraryContext())
            {
                LogData tmp = new LogData(){BookName = book.Name, UserName = user.Name, Date = DateTime.Now, TypeOfMessage = user.MessageType.ToString()};
                db.Logs.Add(tmp);
                await db.SaveChangesAsync();
            }
        }

        public static async Task<List<LogData>> GetLogs()
        {
            using (LibraryContext db = new LibraryContext())
            {
                return await db.Logs.ToListAsync();
            }
        }
    }
}
