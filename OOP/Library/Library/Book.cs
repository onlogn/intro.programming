﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Book
    {
        public int Id { get; }
        public string Name { get; }
        public Category Category { get; }

        public Book(int id, string name, Category category)
        {
            Name = name;
            Category = category;
            Id = id;
        }

        private bool Equals(Book a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Id, Id) && Equals(a.Id, Id) && Equals(a.Name, Name) && Equals(a.Category, Category);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Book)) return false;
            return Equals((Book)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Id.GetHashCode();
            res = res * num ^ Name.GetHashCode();
            res = res * num ^ Category.GetHashCode();

            return res;
        }
    }
    
}
