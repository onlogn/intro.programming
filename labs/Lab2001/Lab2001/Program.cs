﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2001
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] arr = new[]
            {
                new Student("Vasya", 1990, 4.5),
                new Student("Petya", 1990, 4.7),
                new Student("Zasha", 1991, 4.3),
                new Student("Xasha", 1991, 4.0)
            };

            Console.WriteLine(arr.ArrToStr());

            Array.Sort(arr, new CompareStudentsByName());

            Console.WriteLine(arr.ArrToStr());

            Array.Sort(arr, new CompareStudentsByYear());

            Console.WriteLine(arr.ArrToStr());
            Console.WriteLine("\n");

            Student[] res = arr.OrderBy(item => item.BirthYear).ToArray();
            Console.WriteLine(res.ArrToStr());
        }
    }

    class CompareStudentsByYear : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.BirthYear.CompareTo(y.BirthYear);
        }
    }

    class CompareStudentsByName : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }

    class CompareStudentsByGPA : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.GPA.CompareTo(y.GPA);
        }
    }

    public static class StringExtension
    {
        public static string ArrToStr<T>(this T[] arr)
        {
            return String.Join("\n",arr);
        }
    }
}
