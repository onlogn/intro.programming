﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab19graphic
{
    public class Line: Figure
    {
        public Line(Point p, Color c) : base(p,c)
        {
        }

        public Line(Point p, int w, int h, Color c) : base(p, w, h, c)
        {
        }

        public Line(FigureData f):this(new Point(f.X, f.Y),f.Width,f.Height,f.Color)
        {
            
        }

        public override void SetSize(int w, int h)
        {
            Width = w;
            Height = h;
        }

        public override void Draw(Graphics g)
        {
            Pen p = new Pen(Color);
            g.DrawLine(p, Position.X, Position.Y, Position.X + Width, Position.Y + Height);
        }

        public override bool IsInFigureRange(Point p)
        {
            int x1 = Position.X; 
            int y1 = Position.Y;
            int x2 = Position.X + Width;
            int y2 = Position.Y + Height;
            return 
                (p.X - x1) * (p.X - x2) + (p.Y - y1) * (p.Y - y2) <= 0 &&
                Math.Round((double)(p.X - x1) / (x2 - x1),1) ==  Math.Round((double)(p.Y - y1) / (y2 - y1),1);
        }

        public override FigureData GetInfoForSerialization()
        {
            return new FigureData()
            {
                FigureType = typeof(Line),
                Color = Color,
                Height = Height,
                Width = Width,
                X = Position.X,
                Y = Position.Y
            };
        }
    }
}
