﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1202winform
{
    class Matrix
    {
        private int[,] _elems;
        private int _rows;
        private int _columns;

        public int this[int i, int j]
        {
            get { return _elems[i, j]; }
            set { _elems[i, j] = value; }
        }

        public Matrix(int rows, int cols)
        {
            _rows = rows;
            _columns = cols;

            _elems = new int[_rows, _columns];
        }

        public Matrix(int[,] arr)
        {
            _rows = arr.GetLength(0);
            _columns = arr.GetLength(1);
            
            //_elems = arr.Clone() as int[,];

            _elems = new int[arr.GetLength(0),arr.GetLength(1)];
            Array.Copy(arr,_elems,arr.Length);
        }

        public Matrix(Matrix a) : this(a._elems)
        {

        }

        public Matrix RotateToRight()
        {
            Matrix b = new Matrix(_columns, _rows);

            for (int i = 0; i < b._rows; i++)
            {
                for (int j = 0; j < b._columns; j++)
                {
                    b[i, j] = _elems[b._columns - j - 1, i];
                }
            }

            return b;
        }

        public Matrix RotateToLeft()
        {
            Matrix b = new Matrix(_columns, _rows);

            for (int i = 0; i < b._rows; i++)
            {
                for (int j = 0; j < b._columns; j++)
                {
                    b[i, j] = _elems[j, b._rows - i - 1];
                }
            }

            return b;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    if (_elems[i, j] == 1)
                    {
                        sb.Append($"*");
                    }
                    else
                    {
                        sb.Append(" ");
                    }

                    sb.Append(" ");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
