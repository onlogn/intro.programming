﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1206
{
    class Program
    {
        static void Main(string[] args)
        {

            while (Console.ReadLine() != null)
            {
                string chessStr = Console.ReadLine();
                string destStr = Console.ReadLine();

                string[] chess = chessStr.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                List<ChessFigure> figures = new List<ChessFigure>();
                List<Point> restricted = new List<Point>();

                for (int i = 0; i < chess.Length; i++)
                {
                    Point pos = new Point(chess[i][1], chess[i][2]);

                    ChessFigure f;

                    if (chess[i][0] == 'P')
                    {
                        f = new Pawn(pos);
                    }
                    else if (chess[i][0] == 'T')
                    {
                        f = new Rook(pos);
                    }
                    else if (chess[i][0] == 'B')
                    {
                        f = new Bishop(pos);
                    }
                    else if (chess[i][0] == 'R')
                    {
                        f = new Queen(pos);
                    }
                    else
                    {
                        f = new King(pos);
                    }

                    figures.Add(f);
                    restricted.Add(pos);
                }

                Point posEnemyKing = new Point(destStr[1], destStr[2]);
                ChessFigure k = new King(posEnemyKing);

                List<Point> moves = k.ListOfAllPositions(new List<Point>());
                moves.Add(new Point(posEnemyKing));

                bool flag = true;
                foreach (Point t in moves)
                {
                    flag = flag && figures.Any(v => v.CanGetToDestination(t,restricted));
                }

                Console.WriteLine(flag ? "SIM" : "NAO");
            }
        }
    }
}
