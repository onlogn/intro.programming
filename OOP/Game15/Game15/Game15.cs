﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Game15
{
    public class Game15
    {
        private int[,] _elements;
        public int Counter { get; private set; }
        public bool IsGameOver => _winArr.Cast<int>().SequenceEqual(_elements.Cast<int>());

        private int FieldSize = 4;
        private int EmptyCell = 16;
        public const int ItemSize = 100;

        private int[,] _winArr;
        private Random _rnd = new Random();
        private readonly List<Position> _directions = new List<Position>()
        {
            new Position(-1, 0),
            new Position(1, 0),
            new Position(0, 1),
            new Position(0, -1)
        };

        public Game15()
        {
            _elements = new int[FieldSize, FieldSize];
            int c = 1;
            for (int i = 0; i < FieldSize; i++)
            {
                for (int j = 0; j < FieldSize; j++)
                {
                    _elements[i, j] = c++;
                }
            }

            _winArr = new int[FieldSize, FieldSize];
            Array.Copy(_elements,_winArr,FieldSize * FieldSize);
        }


        public void Move(Position current)
        {

            foreach (var d in _directions)
            {
                Position next = current + d;
                if (IsInRange(next) && _elements[next.Row, next.Col] == EmptyCell)
                {
                    Swap(current, next);
                    Counter++;
                    return;
                }
            }
            
        }

        public void Shuffle(int n)
        {
            for (int i = 0; i < n; i++)
            {
                Move(new Position(_rnd.Next(0,FieldSize), _rnd.Next(0, FieldSize)));
            }

            Counter = 0;
        }

        public Bitmap GetImage()
        {
            Bitmap bm = new Bitmap(FieldSize * ItemSize, FieldSize * ItemSize);
            Graphics g = Graphics.FromImage(bm);

            for (int i = 0; i < FieldSize; i++)
            {
                for (int j = 0; j < FieldSize; j++)
                {
                    g.DrawImage((Image)Properties.Resources.ResourceManager.GetObject($"_{_elements[i, j]}"), new System.Drawing.Point(j * ItemSize, i* ItemSize));
                }
            }

            return bm;
        }

        private void Swap(Position current, Position next)
        {
            int tmp = _elements[current.Row, current.Col];
            _elements[current.Row, current.Col] = _elements[next.Row, next.Col];
            _elements[next.Row, next.Col] = tmp;
        }

        private bool IsInRange(Position p)
        {
            return 0 <= p.Row && p.Row < FieldSize && 0 <= p.Col && p.Col < FieldSize;
        }
    }
}
