﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1277
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTests = int.Parse(Console.ReadLine());

            for (int numOfTest = 0; numOfTest < numOfTests; numOfTest++)
            {
                Console.ReadLine();
                string str = Console.ReadLine();
                string pr = Console.ReadLine();

                string[] journal = pr.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                string[] students = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                StringBuilder res = new StringBuilder();

                for (int i = 0; i < journal.Length; i++)
                {
                    var r = journal[i].Count(x => x == 'P');
                    var m = journal[i].Count(x => x == 'M');

                    if ((double) r / (journal[i].Length-m) < 0.75)
                    {
                        if (res.Length != 0)
                        {
                            res.Append(" ");
                        }
                        res.Append(students[i]); 
                    }
                }

                Console.WriteLine(res);

            }
        }
    }
}
