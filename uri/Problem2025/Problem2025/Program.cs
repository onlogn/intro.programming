﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Problem2025
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {
                string s = Console.ReadLine();

                Regex r = new Regex(@"\woulupukk\w");

                string res = r.Replace(s, "Joulupukki");

                Console.WriteLine(res);
            }
        }
    }
}
