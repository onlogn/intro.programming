﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1245
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            while ((s = Console.ReadLine()) != null)
            {
                int n = int.Parse(s);
                List<int> left = new List<int>();
                List<int> right = new List<int>();
                for (int i = 0; i < n; i++)
                {
                    string[] arr = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if (arr[1] == "D")
                    {
                        left.Add(int.Parse(arr[0]));
                    }
                    else
                    {
                        right.Add(int.Parse(arr[0]));
                    }
                }

                int cnt = 0;

                for (int i = 0; i < left.Count; i++)
                {
                    int idx = right.IndexOf(left[i]);
                    if (idx != -1)
                    {
                        cnt++;
                        right.RemoveAt(idx);
                    }
                }

                Console.WriteLine(cnt);
                
            }

        }

        
    }
}
