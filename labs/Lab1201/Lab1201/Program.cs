﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1201
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Rows = ");
            int row = int.Parse(Console.ReadLine());
            Console.Write("Columns = ");
            int col = int.Parse(Console.ReadLine());

            if(row == 0 || col == 0) Console.WriteLine("Array is Empty");
            else
            {
                int[,] arr = new int[row, col];

                arr[0, 0] = 1;

                arr[0, arr.GetLength(1) - 1] = 2;

                arr[arr.GetLength(0) - 1, arr.GetLength(1) - 1] = 3;

                arr[arr.GetLength(0) - 1, 0] = 4;
                
                Console.WriteLine(arr.ArrToStr());
            }
        }
    }

    public static class StringExtension
    {
        public static string ArrToStr(this int[,] arr)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    sb.Append(arr[i, j]);
                    sb.Append(" ");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
