﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1069
{
    class Program
    {
        static void Main(string[] args)
        {
            int nt = int.Parse(Console.ReadLine());

            for (int it = 0; it < nt; it++)
            {

                string str = Console.ReadLine();

                int cnt = 0;
                int res = 0;

                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == '<')
                    {
                        cnt++;
                    }

                    if (str[i] == '>' && cnt > 0)
                    {
                        cnt--;
                        res++;
                    }
                }

                Console.WriteLine(res);
            }
        }
    }
}
