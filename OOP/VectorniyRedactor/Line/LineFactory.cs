﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Figures;
using Point = Figures.Point;

namespace Line
{
    public class LineFactory : IFigureFactory
    {
        public string FigureName => "Line";

        public Type FigureType => typeof(Line);

        public IFigure CreateFigure(Point p, int w, int h, Color c)
        {
            Line tmp = new Line(p, c);
            tmp.SetSize(w, h);
            return tmp;
        }

        public IFigure CreateFigure(Point p, Color c)
        {
            return new Line(p,c);
        }

        public IFigure CreateFigure(FigureData f)
        {
            return new Line(f);
        }
    }
}
