﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0501
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 0.0;
            decimal b = 0.0m;
            for (int i = 0; i < 10; i++)
            {
                a += 0.1;
                b += 0.1m;
            }

            Console.WriteLine($"{a} {a==1.0} / {b} {b==1.0m}");


        }
    }
}
