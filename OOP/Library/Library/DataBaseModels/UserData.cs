﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DataBaseModels
{
    [Table("Users")]
    public class UserData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<CategoryData> Categories { get; set; } = new HashSet<CategoryData>();
        public PreferredMessageType MessageType { get; set; }

        public UserData()
        {
        }


    }
}
