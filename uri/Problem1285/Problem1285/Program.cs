﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1285
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;

            while ((s = Console.ReadLine()) != null)
            {
                string[] str = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                int cnt = 0;
                for (int i = int.Parse(str[0]); i <= int.Parse(str[1]); i++)
                {
                    if (i.ToString().Length == i.ToString().Distinct().Count())
                        cnt++;
                }

                Console.WriteLine(cnt);
            }
        }
    }
}
