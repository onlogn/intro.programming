﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures
{
    public abstract class Figure : IFigure
    {
        public Point Position { get; protected set; }
        public int Width { get; protected set; }
        public int Height { get; protected set; }
        public Color Color { get; protected set; }

        protected Figure(Point p, int w, int h, Color c)
        {
            Width = w;
            Height = h;
            Position = p;
            Color = c;
        }

        protected Figure(Point p, Color c) : this(p, 0, 0, c)
        {

        }

        public abstract void SetSize(int w, int h);

        public abstract void Draw(Graphics g);

        public abstract bool IsInFigureRange(Point p);

        public virtual void Move(Point p)
        {
            Position += p;
        }

        public abstract FigureData GetInfoForSerialization();
    }
}
