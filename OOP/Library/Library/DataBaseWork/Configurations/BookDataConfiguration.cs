﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DataBaseModels;

namespace Library.DataBaseWork.Configurations
{
    class BookDataConfiguration : EntityTypeConfiguration<BookData>
    {
        public BookDataConfiguration()
        {
            this.ToTable("Books");

            this.HasRequired(c => c.Category).WithMany().HasForeignKey(c => c.CategoryId);
        }
    }
}
