﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1025
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            int cnt = 1;
            while ((str = Console.ReadLine()) != "0 0")
            {
                string[] s = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                int n = int.Parse(s[0]);
                int q = int.Parse(s[1]);

                List<int> num = new List<int>();

                for (int i = 0; i < n; i++)
                {
                    num.Add(int.Parse(Console.ReadLine()));
                }

                num.Sort();

                Console.WriteLine($"CASE# {cnt}:");
                cnt++;

                for (int i = 0; i < q; i++)
                {
                    int a = int.Parse(Console.ReadLine());
                    int res = num.IndexOf(a);
                    if (res != -1)
                    {
                        Console.WriteLine($"{a} found at {res + 1}");
                    }
                    else
                    {
                        Console.WriteLine($"{a} not found");
                    }
                }
            }
        }
    }
}
