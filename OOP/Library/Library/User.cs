﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class User
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public List<Category> Categories { get; private set; }
        public PreferredMessageType MessageType { get; private set; }

        public User(int id, string name, List<Category> categories, PreferredMessageType messageType)
        {
            Id = id;
            Name = name;
            Categories = categories;
            MessageType = messageType;
        }

        private bool Equals(User a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Id, Id) && Equals(a.Id, Id) && Equals(a.Name, Name) && Equals(a.MessageType, MessageType) 
                   && a.Categories.Cast<int>().SequenceEqual(Categories.Cast<int>());
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Book)) return false;
            return Equals((User)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Id.GetHashCode();
            res = res * num ^ Name.GetHashCode();
            res = res * num ^ MessageType.GetHashCode();
            res = res * Categories.Cast<int>().Aggregate((x, y) => x * num + y);

            return res;
        }
    }
}
