﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab0802
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Random rnd = new Random();


            for (int j = 0; j < 30; j++)
            {
                int x1 = rnd.Next(Width);
                int y1 = rnd.Next(Height);

                
                Pen p;
                int r = rnd.Next(50, 70);

                int c = rnd.Next(1, 4);

                switch (c)
                {
                    case 1:
                        p = new Pen(Color.FromArgb(rnd.Next(255), 0, 0));
                        break;
                    case 2:
                        p = new Pen(Color.FromArgb(0, rnd.Next(255), 0));
                        break;
                    default:
                        p = new Pen(Color.FromArgb(0, 0, rnd.Next(255)));
                        break;
                }


                for (int i = 0; i < 5000; i++)
                {
                    int x2 = rnd.Next(Width);
                    int y2 = rnd.Next(Height);
                    double d = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
                    if (d < r)
                    {
                        e.Graphics.DrawLine(p, x1, y1, x2, y2);
                    }
                }
            }

        }
    }
}
