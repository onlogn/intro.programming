﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1022
{
    class Program
    {
        static void Main(string[] args)
        {
            int numT = int.Parse(Console.ReadLine());

            for (int i = 0; i < numT; i++)
            {

                string str = Console.ReadLine();
                string[] s = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                int n1 = int.Parse(s[0]);
                int d1 = int.Parse(s[2]);
                int n2 = int.Parse(s[4]);
                int d2 = int.Parse(s[6]);

                string op = s[3];

                Tuple<int, int> t;

                switch (op)
                {
                    case "+":
                        t = SumOfFractions(n1, d1, n2, d2);
                        break;
                    case "-":
                        t = SubOfFractions(n1, d1, n2, d2);
                        break;
                    case "*":
                        t = MultipleOfFractions(n1, d1, n2, d2);
                        break;
                    default:
                        t = DivisionOfFractions(n1, d1, n2, d2);
                        break;
                }

                int gcd = Gcd(t.Item1, t.Item2);

                string sign = "";

                int a = t.Item1 / gcd;
                int b = t.Item2 / gcd;

                if ((a < 0 && b > 0) || (a > 0 && b < 0))
                {
                    sign = "-";
                }

                Console.WriteLine($"{t.Item1}/{t.Item2} = {sign}{Math.Abs(a)}/{Math.Abs(b)}");
            }
        }

        public static Tuple<int,int> SumOfFractions(int n1, int d1, int n2, int d2)
        {
            int numinator = n1 * d2 + n2 * d1;
            int denuminator = d1 * d2;

            return Tuple.Create(numinator, denuminator);
        }

        public static Tuple<int, int> SubOfFractions(int n1, int d1, int n2, int d2)
        {
            int numinator = n1 * d2 - n2 * d1;
            int denuminator = d1 * d2;

            return Tuple.Create(numinator, denuminator);
            
        }

        public static Tuple<int, int> MultipleOfFractions(int n1, int d1, int n2, int d2)
        {
            int numinator = n1 * n2;
            int denuminator = d1 * d2;

            return Tuple.Create(numinator, denuminator);
        }

        public static Tuple<int, int> DivisionOfFractions(int n1, int d1, int n2, int d2)
        {
            int numinator = n1 * d2;
            int denuminator = d1 * n2;

            return Tuple.Create(numinator, denuminator);
        }

        public static int Gcd(int a, int b)
        {
            while (b != 0)
            {
                a %= b;
                Swap(ref a, ref b);
            }

            return a;
        }

        public static void Swap(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }
    }
}
