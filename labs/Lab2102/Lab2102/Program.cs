﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Lab2102
{
    class Program
    {
        delegate int MathIntFunc(int x);
        delegate double MathFunc(double x);

        delegate string DoubleFormatter(double x);

        static void Main(string[] args)
        {
            MathIntFunc f = Sqr;
            PrintTable(f,"x","Sqr",1,10);

            PrintTable(Math.Abs,"x","Sqr",1,10);

            PrintTable(x=>x*x*x,"x","Sqr",1,10);

            PrintTable(Math.Sqrt, "x", "Sqr", 1, 3, 0.1, x=> $"{x:F4}");
            PrintTable(Math.Sqrt, "x", "Sqr", 1, 3, 0.1, x=> $"{x:E4}");

            Formatter fx = new Formatter(10);
            PrintTable(Math.Sqrt, "x", "Sqr", 1, 3, 0.1, fx.Compute);
        }

        static void PrintTable(MathIntFunc f, string titleArg, string titleFunc, int a, int b)
        {
            Console.WriteLine($"{titleArg, -10}{titleFunc,10}");
            Console.WriteLine(new string('-',20));
            for (int i = a; i <= b; i++)
            {
                Console.WriteLine($"{i, -10}{f(i),10}");
            }
            Console.WriteLine(new string('-', 20));
        }

        static void PrintTable(MathFunc f, string titleArg, string titleFunc, double a, double b, double dx, DoubleFormatter ft)
        {
            Console.WriteLine($"{titleArg,-10}{titleFunc,10}");
            Console.WriteLine(new string('-', 20));
            for (double i = a; i <= b; i+=dx)
            {
                Console.WriteLine($"{i,-10}{ft(f(i)),10}");
            }
            Console.WriteLine(new string('-', 20));
        }

        static int Sqr(int x)
        {
            return x * x;
        }
    }

    public class Formatter
    {
        public int Precision { get; }
        public Formatter(int w)
        {
            Precision = w;
        }

        public string Compute(double x)
        {
            return string.Format("{0:F" + Precision + "}",x);
        }
    }
}
