﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0204
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.Write("Inches = ");
            //decimal a = decimal.Parse(Console.ReadLine());
            //decimal res = a * 2.54m;
            //Console.WriteLine($"Centimeters = {res}");

            Console.Write("Inches = ");
            double a = double.Parse(Console.ReadLine());
            const double cmPerInch = 2.54;
            double res = a * cmPerInch;

            Console.WriteLine($"{a}'' = {res:F2}");

        }
    }
}
