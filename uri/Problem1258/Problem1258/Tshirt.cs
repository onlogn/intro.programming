﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1258
{
    class Tshirt
    {
        public string OwnerName { get; private set; }
        public string Color { get; private set; }
        public string Size { get; private set; }

        public Tshirt(string name, string color, string size)
        {
            OwnerName = name;
            Color = color;
            Size = size;
        }

        public Tshirt(Tshirt a):this(a.OwnerName,a.Color, a.Color)
        {
            
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Color);
            sb.Append(" ");
            sb.Append(Size);
            sb.Append(" ");
            sb.Append(OwnerName);

            return sb.ToString();
        }

        private bool Equals(Tshirt a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.OwnerName, OwnerName) && Equals(a.Color, Color) && Equals(a.Size, Size);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Tshirt)) return false;
            return Equals((Tshirt)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = OwnerName.GetHashCode();
            res = res * num + Color.GetHashCode();
            res = res * num + Size.GetHashCode();

            return res;
        }


    }
}
