﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2204
{
    public enum Align
    {
        Left,
        Right,
        Center
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            //AlignText("Hello", 20, '.', Align.Left);
            string s = "Hello";
            Console.WriteLine(s.AlignText(20,'.',Align.Left));
        }

        
    }

    public static class StringExtension
    {
        public static string AlignText(this string str, int strLen, char delimeter, Align a)
        {
            switch (a)
            {
                case Align.Left:
                    return str.PadLeft(strLen, delimeter);
                case Align.Right:
                    return str.PadRight(strLen, delimeter);
                case Align.Center:
                    int w1 = (strLen - str.Length) / 2;
                    int w2 = strLen - str.Length - w1;
                    return new string(delimeter, w1) + str + new string(delimeter,w2);

            }

            return "";
        }
    }
}
