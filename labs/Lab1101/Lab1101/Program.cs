﻿using System;
using System.Linq;

namespace Lab1101
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) arr[i] = int.Parse(Console.ReadLine());

            int[] copyArr = CopyArray(arr);

            ArrayReverse(copyArr);
        }

        private static void ArrayReverse<T>(T[] array)
        {
            for (int i = 0; i < array.Length / 2; i++)
            {
                T tmp = array[i];
                array[i] = array[array.Length - i - 1];
                array[array.Length - i - 1] = tmp;
            }
        }

        private static int[] CopyArray(int[] a)
        {
            int[] res = new int[a.Length];

            for (int i = 0; i < a.Length; i++) res[i] = a[i];

            return res;
        }

        private static bool IsEquals(int[] a, int[] b)
        {
            return (a == null && b == null) || (ReferenceEquals(a, b) || a.SequenceEqual(b));
        }
    }
}