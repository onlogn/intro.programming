﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0203
{
    class Program
    {
        static void Main(string[] args)
        {
            //int a = int.Parse(Console.ReadLine());
            //int sum = a % 10 + (a / 10 % 10) + (a / 100 % 10) + (a / 1000);
            //Console.WriteLine(sum);

            string str = Console.ReadLine().Trim();
            if (int.TryParse(str, out int res))
            {
                int n = str.Length;
                int sum = 0;
                for (int i = 0; i < n; i++)
                {
                    sum += res % 10;
                    res /= 10;
                }
                Console.WriteLine(sum);
            }
            


        }
    }
}
