﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1203wf
{
    class Maze
    {
        private int[,] _maze;
        private Point _currentPlayerPos;

        public Maze()
        {
            _maze = new[,]
            {
                {1,1,1,1,1,1,1,1,1},
                {1,0,0,1,0,0,0,0,1},
                {1,0,0,1,0,1,1,0,1},
                {1,0,0,1,0,1,-1,0,1},
                {1,0,0,1,0,1,0,0,1},
                {1,0,0,0,0,1,0,0,1},
                {1,1,1,1,1,1,1,1,1}

            };

            _currentPlayerPos = new Point(1,1);

        }

        public void Move(Keys key)
        {
            Point nextPosition = _currentPlayerPos + GetDirectionPoint(key);
            
            if (nextPosition.X < 0 || nextPosition.X > _maze.GetLength(0) || nextPosition.Y < 0 ||
                nextPosition.Y > _maze.GetLength(1))
            {

                return;
            }
            if (_maze[nextPosition.X, nextPosition.Y] == 0)
            {
                _currentPlayerPos = nextPosition;
            }
            else if (_maze[nextPosition.X, nextPosition.Y] == -1)
            {
                MessageBox.Show("Congratulations. You did it!");
                _currentPlayerPos = new Point(1,1);
            }
        }

        private Point GetDirectionPoint(Keys key)
        {
            Point res = new Point(0,0);

            switch (key)
            {
                case Keys.A: res = new Point(0,-1);
                    break;
                case Keys.D: res = new Point(0,1);
                    break;
                case Keys.W: res = new Point(-1,0);
                    break;
                case Keys.S: res = new Point(1,0);
                    break;
                default: res = new Point(0,0);
                    break;
            }

            return res;
        }

        public string ShowMaze()
        {
            StringBuilder sb = new StringBuilder();
                
            for (int i = 0; i < _maze.GetLength(0); i++)
            {
                for (int j = 0; j < _maze.GetLength(1); j++)
                {
                    if (i == _currentPlayerPos.X && j == _currentPlayerPos.Y)
                    {
                        sb.Append("+");
                    } else if (_maze[i, j] == 1)
                    {
                        sb.Append("#");
                    } else if (_maze[i, j] == -1)
                    {
                        sb.Append("F");
                    }
                    else
                    {
                        sb.Append(" ");
                    }
                }

                sb.AppendLine();
            }


            return sb.ToString();
        }

    }
}
