﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1500
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            string[] str = s.Split(new [] {' '}, StringSplitOptions.RemoveEmptyEntries);

            

            Console.WriteLine(str.Select(i => int.Parse(i)).Sum() % 2 == 0 ? "YES" : "NO");
        }
    }
}
