﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem296
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] s = new string[3];

            for (int i = 0; i < 3; i++)
                s[i] = Console.ReadLine();

            Dictionary<string, int> d = new Dictionary<string, int>();
            string c;
            for (int i = 0; i < s.Length; i++)
            {
                c = s[i];
                if (!d.ContainsKey(c))
                    d[c] = 1;
                else
                    d[c]++;

            }

            int max = d.Values.Max();
            
            Console.WriteLine(max == 1 ? 0 : max);


        }
    }
}
