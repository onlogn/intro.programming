﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem116
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine($"{MinDigit(n)} {MaxDigit(n)}");
        }

        private static int MinDigit(int n)
        {
            return n.ToString().Select(i => int.Parse(i.ToString())).Min();
        }
        private static int MaxDigit(int n)
        {
            return n.ToString().Select(i => int.Parse(i.ToString())).Max();
        }
    }
}
