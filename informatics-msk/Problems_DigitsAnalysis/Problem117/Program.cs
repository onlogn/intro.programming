﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem117
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            var bin = Convert.ToString(n, 2).Reverse().ToArray();

            Console.WriteLine(String.Join("",bin));
        }
    }
}
