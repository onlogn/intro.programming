﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2303
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadAndReverse();
            Console.WriteLine();
        }

        private static void ReadAndReverse()
        {
            string line = Console.ReadLine();
            if (line != null)
            {
                ReadAndReverse();
                Console.Write($"{line}");
            }
        }
    }
}
