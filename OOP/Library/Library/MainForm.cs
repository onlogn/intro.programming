﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.DataBaseModels;
using Library.DataBaseWork;

namespace Library
{
    public partial class MainForm : Form
    {
        private BindingList<Book> _booksList;
        private UsersNotifier _usersNotifier;
        public event EventHandler<AddBookEventArgs> BookAddedEvent;
        
        public MainForm()
        {
            InitializeComponent();
            dgvBooks.AutoGenerateColumns = false;
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            _booksList = new BindingList<Book>(await BookDataBaseService.GetBooksList());
            dgvBooks.DataSource = _booksList;
            _usersNotifier = new UsersNotifier();
            this.BookAddedEvent += _usersNotifier.OnBookAdded;
            _usersNotifier.BookForUserAdded += UsersNotifierOnBookForUserAdded;

        }

        private void UsersNotifierOnBookForUserAdded(object sender, UsersBooksEventArgs usersBooksEventArgs)
        {
            IMessageSender s = MessageSenderFactory.Create(usersBooksEventArgs.User.MessageType);
            s.SendMessage(usersBooksEventArgs.User,usersBooksEventArgs.Book);
        }

        private async void btnAddBook_Click(object sender, EventArgs e)
        {
            BookRedactionForm bf = new BookRedactionForm();
            
            if (bf.ShowDialog(this) == DialogResult.OK)
            {
                Book book = await BookDataBaseService.AddBook(bf.CurrentBook);
                _booksList.Add(book);

                AddBookEventArgs tmpBook = new AddBookEventArgs { Book = book };
                OnBookAddedEvent(tmpBook);

            }
        }

        private async void btnEditBook_Click(object sender, EventArgs e)
        {
            if (dgvBooks.SelectedRows.Count == 0)
                return;

            Book bookForEdit = (Book) dgvBooks.SelectedRows[0].DataBoundItem;
            BookRedactionForm bf = new BookRedactionForm(bookForEdit);

            if (bf.ShowDialog(this) == DialogResult.OK)
            {
                await BookDataBaseService.EditBook(bf.CurrentBook);
                _booksList[_booksList.IndexOf(bookForEdit)] = new Book(bf.CurrentBook.Id, bf.CurrentBook.Name, bf.CurrentBook.Category);
            }
        }

        private async void btnDeleteBook_Click(object sender, EventArgs e)
        {
            if (dgvBooks.SelectedRows.Count == 0)
                return;
            Book bookFordel = (Book)dgvBooks.SelectedRows[0].DataBoundItem;

            await BookDataBaseService.DeleteBook(bookFordel);
            _booksList.Remove(bookFordel);
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            UsersForm uf = new UsersForm();
            uf.Show();
        }

        protected virtual void OnBookAddedEvent(AddBookEventArgs e)
        {
            //if (BookAddedEvent != null)
            //{
            //    BookAddedEvent(this, e);
            //}
            BookAddedEvent?.Invoke(this, e);
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            LogsForm lf = new LogsForm();
            lf.Show();
        }
    }
}
