﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1203wf
{
    public partial class Form1 : Form
    {
        private Maze m;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnStartGame_Click(object sender, EventArgs e)
        {
            m = new Maze();
            lblLabirint.Text = m.ShowMaze();
        }
        
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            m.Move(e.KeyCode);
            lblLabirint.Text = m.ShowMaze();
        }
    }
}
