﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab19graphic
{
    public class Point
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point(Point a):this(a.X, a.Y)
        {
            
        }

        public static Point operator +(Point a,Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        } 

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        private bool Equals(Point a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.X, X) && Equals(a.Y, Y);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Point)) return false;
            return Equals((Point)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = X.GetHashCode();
            res = res * num ^ Y.GetHashCode();
            return res;
        }
    }
}
