﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0301
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Input b = ");
            int b = int.Parse(Console.ReadLine());

            bool isLess = a < b;
            Console.WriteLine($"{a} < {b} = {isLess}");

            bool isLessOrEqual = a <= b;
            Console.WriteLine($"{a} <= {b} = {isLessOrEqual}");

            bool isMore = a > b;
            Console.WriteLine($"{a} > {b} = {isMore}");

            bool isMoreOrEqual = a >= b;
            Console.WriteLine($"{a} >= {b} = {isMoreOrEqual}");

            bool isEqual = a == b;
            Console.WriteLine($"{a} == {b} = {isEqual}");

            bool isNotEquall = a != b;
            Console.WriteLine($"{a} != {b} = {isNotEquall}");
        }
    }
}
