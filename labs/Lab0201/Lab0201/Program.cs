﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0201
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Input b = ");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine($"Before: a = {a}, b = {b}");

            int tmp = a;
            a = b;
            b = tmp;

            Console.WriteLine($"After a = {a}, b = {b}");
        }
    }
}
