﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1204
{
    class Miner
    {
        public int NumOfRows { get; private set; }
        public int NumOfColumns { get; private set; }

        private int[,] _field;
        private Random _rnd = new Random();
        private HashSet<Point> _mines;

        public Miner(int numOfRows, int numOfColumns)
        {
            if (numOfRows < 1 || numOfColumns < 1)
            {
                throw new ArgumentException("Incorrect size");
            }
            NumOfRows = numOfRows;
            NumOfColumns = numOfColumns;
            _field = new int[numOfRows, numOfColumns];
        }

        public void PutMines(int num)
        {
            int bound = (NumOfRows + NumOfColumns) * 2;
            if (num < 1 || num > bound)
            {
                throw new ArgumentException($"Number of mines must be >= 1 and less than {bound}");
            }

            _mines = new HashSet<Point>();
            Point tmp;
            while (_mines.Count != num)
            {
                _mines.Add(new Point(_rnd.Next(0, NumOfRows), _rnd.Next(0, NumOfColumns)));
            }

            foreach (var v in _mines)
            {
                _field[v.X, v.Y] = -1;
            }
        }

        public string ShowField()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < NumOfRows; i++)
            {
                for (int j = 0; j < NumOfColumns; j++)
                {
                    sb.Append(_field[i, j] == -1 ? "*" : _field[i, j].ToString());
                    sb.Append(" ");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        public void AddValuesToField()
        {
            foreach (var m in _mines)
            {
                AddPointToField(m);
            }
        }

        private void AddPointToField(Point mine)
        {
            for (int i = mine.X - 1; i <= mine.X + 1; i++)
            {
                for (int j = mine.Y - 1; j <= mine.Y + 1; j++)
                {
                    if (i < 0 || i >= NumOfRows || j < 0 || j >= NumOfColumns || _field[i,j] == -1)
                        continue;
                    _field[i,j]++;
                }
            }
        } 
    }
}
