﻿namespace Game135
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.mnuDifficulty = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu4 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu5 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu6 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRestart = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.panelField = new System.Windows.Forms.Panel();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDifficulty});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(321, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // mnuDifficulty
            // 
            this.mnuDifficulty.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnu3,
            this.mnu4,
            this.mnu5,
            this.mnu6});
            this.mnuDifficulty.Name = "mnuDifficulty";
            this.mnuDifficulty.Size = new System.Drawing.Size(81, 20);
            this.mnuDifficulty.Text = "Сложность";
            // 
            // mnu3
            // 
            this.mnu3.Name = "mnu3";
            this.mnu3.Size = new System.Drawing.Size(180, 22);
            this.mnu3.Tag = "3";
            this.mnu3.Text = "3x3";
            this.mnu3.Click += new System.EventHandler(this.MenuItemClick);
            // 
            // mnu4
            // 
            this.mnu4.Checked = true;
            this.mnu4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mnu4.Name = "mnu4";
            this.mnu4.Size = new System.Drawing.Size(180, 22);
            this.mnu4.Tag = "4";
            this.mnu4.Text = "4x4";
            this.mnu4.Click += new System.EventHandler(this.MenuItemClick);
            // 
            // mnu5
            // 
            this.mnu5.Name = "mnu5";
            this.mnu5.Size = new System.Drawing.Size(180, 22);
            this.mnu5.Tag = "5";
            this.mnu5.Text = "5x5";
            this.mnu5.Click += new System.EventHandler(this.MenuItemClick);
            // 
            // mnu6
            // 
            this.mnu6.Name = "mnu6";
            this.mnu6.Size = new System.Drawing.Size(180, 22);
            this.mnu6.Tag = "6";
            this.mnu6.Text = "6x6";
            this.mnu6.Click += new System.EventHandler(this.MenuItemClick);
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(25, 338);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(90, 33);
            this.btnRestart.TabIndex = 1;
            this.btnRestart.Text = "Новая игра";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblInfo.Location = new System.Drawing.Point(125, 345);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(133, 20);
            this.lblInfo.TabIndex = 2;
            this.lblInfo.Text = "Шагов сделано: ";
            // 
            // panelField
            // 
            this.panelField.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelField.Location = new System.Drawing.Point(12, 27);
            this.panelField.Name = "panelField";
            this.panelField.Size = new System.Drawing.Size(300, 300);
            this.panelField.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 384);
            this.Controls.Add(this.panelField);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Game 15";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuDifficulty;
        private System.Windows.Forms.ToolStripMenuItem mnu3;
        private System.Windows.Forms.ToolStripMenuItem mnu4;
        private System.Windows.Forms.ToolStripMenuItem mnu5;
        private System.Windows.Forms.ToolStripMenuItem mnu6;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Panel panelField;
    }
}

