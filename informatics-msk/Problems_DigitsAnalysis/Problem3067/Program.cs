﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3067
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            List<string> str = new List<string>();
            while ((s = Console.ReadLine()) != "0")
            {
                str.Add(s);
            }

            Console.WriteLine(str.Select(int.Parse).Count(i => i%2 == 0));
        }
    }
}
