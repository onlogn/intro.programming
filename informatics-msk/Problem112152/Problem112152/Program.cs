﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem112152
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            string[] s = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            double a = double.Parse(s[0]);
            double b = double.Parse(s[1]);

            Console.WriteLine($"{a:f3} {(a + 0.001):f3} {(a + 0.002):f3} {(a + 0.003):f3} {(b-0.001):f3}");
        }
    }
}
