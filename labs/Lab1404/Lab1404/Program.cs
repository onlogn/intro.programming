﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1404
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "Familia Imya Otchestvo";

            string[] arrs = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            StringBuilder sb = new StringBuilder();

            sb.Append(arrs[0]);
            sb.Append(" ");
            sb.Append(arrs[1].Substring(0,1));
            sb.Append(". ");
            sb.Append(arrs[2].Substring(0, 1));
            sb.Append(".");

            Console.WriteLine(sb);
        }
    }
}
