﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1272
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {
                string str = Console.ReadLine();

                string[] words = str.Split(' ');

                foreach (string s in words)
                {
                    if (s.Length > 0)
                    {
                        Console.Write(s[0]);
                    }
                }
                Console.WriteLine();
            }

            

        }
    }
}
