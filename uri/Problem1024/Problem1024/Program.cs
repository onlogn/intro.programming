﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace Problem1024
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());

            for (int ind = 0; ind < num; ind++)
            {
                string s = Console.ReadLine();

                StringBuilder enc = new StringBuilder();

                foreach (char c in s)
                {
                    if (Char.IsLetter(c))
                    {
                        enc.Append((char)(c + 3));
                    }
                    else
                    {
                        enc.Append((char)(c));
                    }
                }

                int len = enc.Length;

                for (int i = 0; i < len / 2; i++)
                {
                    char tmp = enc[i];
                    enc[i] = enc[len - 1 - i];
                    enc[len - 1 - i] = tmp;
                }

                for (int i = len / 2; i < len; i++)
                {
                    enc[i] = (char)(enc[i] - 1);
                }

                Console.WriteLine(enc);
            }
   
        }
    }
}
