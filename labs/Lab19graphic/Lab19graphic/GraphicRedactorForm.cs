﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab19graphic
{
    public partial class GraphicRedactorForm : Form
    {
        private List<Figure> _figures = new List<Figure>();
        private Figure _selectedFigure;
        private Type _currentType;
        private Color _currentColor;
        private Point _coordForMove;

        public GraphicRedactorForm()
        {
            InitializeComponent();
        }

        private void GraphicRedactorForm_Load(object sender, EventArgs e)
        {
            _currentColor = Color.Black;
            btnColor.BackColor = _currentColor;
        }

        private void BtnClick(object sender, EventArgs e)
        {
            Button b = (Button) sender;
            switch (b.Name)
            {
                case nameof(btnCircle):
                    _currentType = typeof(Circle);
                    break;
                case nameof(btnRect):
                    _currentType = typeof(Rectangle);
                    break;
                case nameof(btnCross):
                    _currentType = typeof(Cross);
                    break;
                case nameof(btnLine):
                    _currentType = typeof(Line);
                    break;
                case nameof(btnSelect):
                    _currentType = null;
                    break;
                case nameof(btnClear):
                    _figures.Clear();
                    pictureBox.Refresh();
                    break;
            }
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            foreach (Figure f in _figures)
            {
                Graphics g = e.Graphics;
                f.Draw(g);
            }
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _currentType == null)
            {
                _selectedFigure = null;
                _coordForMove = null;
                foreach (var f in _figures)
                {
                    Point tmp = new Point(e.X, e.Y);
                    if (f.IsInFigureRange(tmp))
                    {
                        _selectedFigure = f;
                        _coordForMove = new Point(e.X - f.Position.X, e.Y - f.Position.Y);
                    }
                }
            }
            if (_currentType == null)
            {
                return;
            }
            _figures.Add((Figure)Activator.CreateInstance(_currentType, new Point(e.X, e.Y), _currentColor));
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _currentType != null)
            {
                Figure tmp = _figures.Last();
                tmp.SetSize(e.X - tmp.Position.X, e.Y - tmp.Position.Y);
                pictureBox.Refresh();
            }
            else if (_currentType == null && _selectedFigure != null && e.Button == MouseButtons.Left)
            {
                _selectedFigure.Move(new Point(e.X - _selectedFigure.Position.X - _coordForMove.X, e.Y - _selectedFigure.Position.Y - _coordForMove.Y));
                pictureBox.Refresh();
            }
            
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog(this) == DialogResult.OK)
            {
                _currentColor = colorDialog.Color;
                btnColor.BackColor = _currentColor;
            }
            

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                SaveLoad.Save(_figures, saveFileDialog.FileName);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                _selectedFigure = null;
                _figures = SaveLoad.Load(openFileDialog.SafeFileName);
                pictureBox.Refresh();
            }
        }

    }
}
