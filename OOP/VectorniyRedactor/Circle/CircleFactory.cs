﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Figures;
using Point = Figures.Point;

namespace Circle
{
    public class CircleFactory : IFigureFactory
    {
        public string FigureName => "Circle";

        public Type FigureType => typeof(Circle);

        public IFigure CreateFigure(Point p, int w, int h, Color c)
        {
            Circle tmp = new Circle(p, c);
            tmp.SetSize(w, h);
            return tmp;
        }

        public IFigure CreateFigure(Point p, Color c)
        {
            return new Circle(p,c);
        }

        public IFigure CreateFigure(FigureData f)
        {
            return new Circle(f);
        }
    }
}
