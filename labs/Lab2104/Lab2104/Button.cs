﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2104
{
    public delegate void ClickHandler(Button b);
    public class Button
    {
        public string Text { get; }
        public int X { get; }
        public int Y { get; }
        public int W { get; }
        public int H { get; }
        public event ClickHandler Click;

        public Button(string text, int x, int y, int w, int h)
        {
            Text = text;
            X = x;
            Y = y;
            W = w;
            H = h;
        }

        public bool Contains(int x, int y)
        {
            return X <= x && x < X + W && Y <= y && y < Y + H;
        }


        public virtual void OnClick(Button b)
        {
            Click?.Invoke(b);
        }
    }
}
