﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1258
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;

            bool flag = false;
            while ((s = Console.ReadLine()) != "0")
            {
                
                int num = int.Parse(s);
                List<Tshirt> shirts = new List<Tshirt>();
               // Tshirt[] arr = new Tshirt[num];
                for (int i = 0; i < num; i++)
                {
                    string s1 = Console.ReadLine().Trim();
                    string[] s2 = Console.ReadLine().Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    shirts.Add(new Tshirt(s1, s2[0], s2[1]));
                    //arr[i] = new Tshirt(s1, s2[0], s2[1]);
                }

                if (flag) Console.WriteLine();
                else flag = true;

                List<Tshirt> res = shirts.OrderBy(i => i.Color).
                ThenByDescending(i => i.Size).ThenBy(i => i.OwnerName).ToList();
                Console.WriteLine(String.Join(Environment.NewLine, res));
                
                //TshirtComparer t = new TshirtComparer();

                //Array.Sort(arr,t);
                //foreach (var i in arr)
                //{
                //    Console.WriteLine($"{i.Color} {i.Size} {i.OwnerName}");
                //}

            } 
        }
    }
}