﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1305
{
    class Program
    {
        static void Main(string[] args)
        {
            int key = 27;

            int c;
            char r;

            while ((c = Console.Read()) != -1)
            {
                if (Char.IsLetter((char) c))
                {
                    char fix;
                    if (Char.IsUpper((char) c))
                    {
                        fix = 'A';
                    }
                    else
                    {
                        fix = 'a';
                    }
                    r = (char) (((c - fix + key) % 26) + fix);
                }
                else r = (char) c;
                Console.Write(r);
            }
        }
    }
}
