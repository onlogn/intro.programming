﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Matrixes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes.Tests
{
    [TestClass()]
    public class MatrixTests
    {
        [TestMethod()]
        public void Constructor1Test()
        {
            Matrix a = new Matrix(new int[,] { { 1, 2 }, { 3, 4 }, {1, 1} });

            int ex_rows = 3;
            int ex_col = 2;

            Assert.AreEqual(a.Rows,ex_rows);
            Assert.AreEqual(a.Columns,ex_col);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException), "Number of rows and columns must be more than 0")]
        public void Constructor2Test()
        {
            Matrix a = new Matrix(0, 1);

        }


        [TestMethod()]
        public void DeterminantTest()
        {
            //arrange
            Matrix a = new Matrix(new int[,] { { 1, 1 }, { 1, 1 } });
            int expected = 0;
            //act
            int res = a.Determinant();
            //assret
            Assert.AreEqual(expected,res);
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception), "Matrix must be NxN")]
        public void DeterminantExceptionTest()
        {
            //arrange
            Matrix a = new Matrix(new int[,] { { 1, 1 }, { 1, 1 }, {1, 1}});
            
            //act
            a.Determinant();
            //assret

        }

        [TestMethod()]
        public void TransposeTest()
        {
            Matrix a = new Matrix(new int[,] { {1, 2}, {3, 4} });
            Matrix expected = new Matrix(new int[,] { { 1, 3 }, { 2, 4 } });

            Matrix res = a.Transpose();
            Assert.AreEqual(expected,res);
        }

        [TestMethod()]
        public void AdditionTest()
        {
            Matrix a = new Matrix(new int[,] {{1, 1}, {1, 1}});
            Matrix b = new Matrix(new int[,] { { 0, 1 }, { 0, 1 } });

            Matrix expected = new Matrix(new int[,] {{1, 2}, {1, 2}});

            Matrix res = a + b;

            Assert.AreEqual(expected, res);
        }

        [TestMethod()]
        public void SubstructTest()
        {
            Matrix a = new Matrix(new int[,] { { 1, 1 }, { 1, 1 } });
            Matrix b = new Matrix(new int[,] { { 0, 1 }, { 0, 1 } });

            Matrix expected = new Matrix(new int[,] { { 1, 0 }, { 1, 0 } });

            Matrix res = a - b;

            Assert.AreEqual(expected, res);
        }

        [TestMethod()]
        public void MultipleOnNumberTest()
        {
            Matrix a = new Matrix(new int[,] { { 1, 1 }, { 1, 1 } });
            int b = 2;

            Matrix expected = new Matrix(new int[,] { { 2, 2 }, { 2, 2 } });

            Matrix res = a*b;

            Assert.AreEqual(expected, res);
        }

        [TestMethod()]
        public void MultipleTest()
        {
            Matrix a = new Matrix(new int[,] { { 1, 2 }, { 3, 0 } });
            Matrix b = new Matrix(new int[,] { { 2, 3 }, { 4, 5 } });
            

            Matrix expected = new Matrix(new int[,] { { 10, 13 }, { 6, 9 } });

            Matrix res = a * b;

            Assert.AreEqual(expected, res);
        }

        [TestMethod()]
        public void EqualTest()
        {
            Matrix a = new Matrix(new int[,] { { 1, 2 }, { 3, 0 } });
            Matrix b = new Matrix(new int[,] {{1, 2}, {3, 4}});

            Matrix ex = new Matrix(new int[,] { { 1, 2 }, { 3, 0 } });

            Assert.AreEqual(a,ex);
            Assert.AreNotEqual(b,ex);

        }

    }
}