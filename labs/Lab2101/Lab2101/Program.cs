﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lab2101
{
    class Program
    {
        delegate double MathFunc(double x);

        static void Main(string[] args)
        {
            MathFunc f = Math.Sqrt;
            Console.WriteLine(f(4));

            f = Math.Abs;
            Console.WriteLine(f(-4));

            f = Sqr;
            Console.WriteLine(f(4));

            f = x => x * x * x;
            Console.WriteLine(f(4));
        }

        static double Sqr(double x)
        {
            return x * x;
        }
    }
}
