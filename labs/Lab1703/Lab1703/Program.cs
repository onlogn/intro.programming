﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1703
{
    class Program
    {
        static void Main(string[] args)
        {
            DynArray<int> d = new DynArray<int>();

            d.Add(1);
            d.Add(2);
            d.Add(3);

            d[2] = 4;

            Console.WriteLine(d[2]);

            d.Reverse();
            Console.WriteLine(d);
        }
    }
}
