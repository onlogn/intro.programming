﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game135
{
    public class Position
    {
        public int Row { get; private set; }
        public int Col { get; private set; }

        public Position(int row, int col)
        {
            Col = col;
            Row = row;
        }

        public Position(Position a) : this(a.Row, a.Col)
        {

        }

        public static Position operator +(Position a, Position b)
        {
            return new Position(a.Row + b.Row, a.Col + b.Col);
        }

        public override string ToString()
        {
            return $"({Row}, {Col})";
        }

        private bool Equals(Position a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Col, Col) && Equals(a.Row, Row);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Position)) return false;
            return Equals((Position)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Col.GetHashCode();
            res = res * num ^ Row.GetHashCode();
            return res;
        }
    }
}
