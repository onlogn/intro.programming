﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem227
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();
            string s = Console.ReadLine();
            string[] numbers = s.Split(' ');

            Console.WriteLine(numbers.Select(x => int.Parse(x)).Max());
        }
    }
}
