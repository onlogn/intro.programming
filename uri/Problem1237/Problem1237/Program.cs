﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1237
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1,s2;
            while ((s1 = Console.ReadLine()) != null)
            {
                s2 = Console.ReadLine();
                Console.WriteLine(LongestCommonSubstring(s1,s2));
            }
        }

        //алгоритм честно позаимствован отсюда: https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#C%23
        public static int LongestCommonSubstring(string str1, string str2)
        {
            if (String.IsNullOrEmpty(str1) || String.IsNullOrEmpty(str2))
                return 0;

            int[,] num = new int[str1.Length, str2.Length];
            int maxlen = 0;

            for (int i = 0; i < str1.Length; i++)
            {
                for (int j = 0; j < str2.Length; j++)
                {
                    if (str1[i] != str2[j])
                        num[i, j] = 0;
                    else
                    {
                        if ((i == 0) || (j == 0))
                            num[i, j] = 1;
                        else
                            num[i, j] = 1 + num[i - 1, j - 1];

                        if (num[i, j] > maxlen)
                        {
                            maxlen = num[i, j];
                        }
                    }
                }
            }
            return maxlen;
        }
    }
}
