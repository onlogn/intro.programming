﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2304
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Factorial(5));

            Console.WriteLine(Gcd(70,50));

            Console.WriteLine(Fib(5));
        }

        private static int Factorial(int n) => (n == 0) ? 1 : n * Factorial(n - 1);

        private static int Gcd(int a, int b)
        {
            if (b == 0)
                return a;
            if (b == 0)
                return b;
            return Gcd(b, a % b);
        }

        private static int Fib(int n)
        {
            if (n == 1 || n == 2) return 1;
            return Fib(n - 2) + Fib(n - 1);

        }
    }
}
