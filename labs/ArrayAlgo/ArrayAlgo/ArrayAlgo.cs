﻿using System;

namespace ArrayAlgo
{
    public class ArrayAlgo
    {
        public static void SelectSort(int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int indexOfMin = i;
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[j] < arr[indexOfMin])
                    {
                        indexOfMin = j;
                    }
                }

                int tmp = arr[i];
                arr[i] = arr[indexOfMin];
                arr[indexOfMin] = tmp;
            }
        }

        public static void SelectSort(Array arr, IMyComparer cmp)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int indexOfMin = i;
                for (int j = i + 1; j < arr.Length; j++)
                {
                    //if (arr[j] < arr[indexOfMin])
                    if(cmp.Compare(arr.GetValue(j), arr.GetValue(indexOfMin)) < 0)
                    {
                        indexOfMin = j;
                    }
                }

                object tmp = arr.GetValue(i);
                arr.SetValue(arr.GetValue(indexOfMin),i);
                arr.SetValue(tmp, indexOfMin);
            }
        }

        public static void SelectSort<T>(T[] arr, IMyComparer<T> cmp)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int indexOfMin = i;
                for (int j = i + 1; j < arr.Length; j++)
                {
                    //if (arr[j] < arr[indexOfMin])
                    if (cmp.Compare(arr[j], arr[indexOfMin]) < 0)
                    {
                        indexOfMin = j;
                    }
                }

                T tmp = arr[i];
                arr[i] = arr[indexOfMin];
                arr[indexOfMin] = tmp;
            }
        }
    }
}
