﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab1504
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedDictionary<char, int> d = new SortedDictionary<char, int>();

            using (StreamReader sr = new StreamReader("legion.txt",System.Text.Encoding.Default))
            {
                int c;
                string ruAlphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
                int cnt = 0;
                while((c = sr.Read()) != -1)
                {
                    char tmp = Char.ToLower((char)c);
                    if (!ruAlphabet.Contains(tmp)) continue;

                    if (!d.ContainsKey(tmp))
                    {
                        d[tmp] = 0;
                    }
                    d[tmp]++;
                    cnt++;
                }
                Console.WriteLine(d.DicToNumLetters(cnt));
            }
        }
    }

    public static class StringExtension
    {
        public static string DicToNumLetters(this SortedDictionary<char, int> d, int total)
        {
            StringBuilder sb = new StringBuilder();
            total = total == 0 ? 1 : total;
            foreach (var v in d)
            {
                sb.Append($"[{v.Key}] => {(double)v.Value / total :f2}\n");
            }

            return sb.ToString();
        }
    }
}
