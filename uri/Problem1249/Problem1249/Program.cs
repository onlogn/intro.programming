﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1249
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;

            while ((str = Console.ReadLine()) != null)
            {
                const int n = 13;

                var res = str.Select(i =>
                {
                    if (Char.IsLower(i))
                    {
                        return i + n > 'z' ? (char)('a' + n - 1 - ('z' - i)) : (char)(i + n);
                    }
                    else if (Char.IsUpper(i))
                    {
                        return i + n > 'Z' ? (char)('A' + n - 1 - ('Z' - i)) : (char)(i + n);
                    }

                    return i;
                });

                Console.Write(String.Join("", res));
                Console.WriteLine();
            }

            
        }
    }
}
