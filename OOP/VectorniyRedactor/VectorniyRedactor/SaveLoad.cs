﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Figures;

namespace VectorniyRedactor
{
    public static class SaveLoad
    {


        public static void Save(List<IFigure> list, string filename)
        {
            WriteFile(filename, list.Select(i => i.GetInfoForSerialization()).ToList());
        }

        public static List<IFigure> Load(string filename, List<IFigureFactory> list)
        {
            return ReadFile(filename).Select(i => FigureDataToFigure(i, list)).Where(i => i != null).ToList();
        }

        private static List<FigureData> ReadFile(string filename)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<FigureData>));

            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                return (List<FigureData>)jsonFormatter.ReadObject(fs);
            }

        }

        private static void WriteFile(string filename, List<FigureData> figures)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<FigureData>));

            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(fs, figures);
            }
        }

        private static IFigure FigureDataToFigure(FigureData fd, List<IFigureFactory> list)
        {
            //activator всегда object возвращает, тут кастуем в нужный нам тип
            //return (IFigure)Activator.CreateInstance(fd.FigureType, fd);

            return list.FirstOrDefault(i => i.FigureType.FullName == fd.TypeName)?.CreateFigure(fd);
        }

        public static Type LoadType(string fileName) 
        {
            Assembly asm = Assembly.LoadFrom(fileName);
            // дергаем самый первый класс, который "не падает" от IFigure (реализует его), т.е. кто в одной дллке напишет несколько - пролетит, 
            // получит только первую фигурку! 1 фигурка = 1 длл
            // GetInterfaces() - все интерефейсы, которые тип реализует
            // Any - хотя бы 1 от Ifigure чтобы был
            //return asm.GetTypes().FirstOrDefault(i => i.GetInterfaces().Any(j => j == typeof(IFigure)));

            return asm.GetTypes().FirstOrDefault(i => i.GetInterfaces().Any(j => j == typeof(IFigureFactory)));
        }
    }
}
