﻿using System.Data.Entity;
using Library.DataBaseModels;

namespace Library.DataBaseWork
{
    public class LibraryContext : DbContext
    {
        static LibraryContext()
        {
            Database.SetInitializer<LibraryContext>(new LibraryContextInitializer());
        }

        public LibraryContext(): base("DBConnection")
        {
            
        }

        public DbSet<BookData> Books { get; set; }
        public DbSet<UserData> Users { get; set; }
        public DbSet<CategoryData> Categories { get; set; }
        public DbSet<LogData> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(typeof(LibraryContext).Assembly);

            modelBuilder.Entity<UserData>()
                .HasMany<CategoryData>(s => s.Categories)
                .WithMany(c => c.Users)
                .Map(cs =>
                {
                    cs.MapLeftKey("UserId");
                    cs.MapRightKey("CategoryId");
                    cs.ToTable("Subscriptions");
                });

        }
    }
}
