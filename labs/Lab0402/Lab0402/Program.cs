﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0402
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Количество посещенных классов в курсе X? ");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine(a >= 20
                ? "Вы прошли курс X.\nРекомендуем взять курс Y как продолжение."
                : "К сожалению, Вы не прошли курс X.\nВы сможете взять его снова в следующем семестре");
        }
    }
}
