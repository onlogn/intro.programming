﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1505
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1 = Console.ReadLine();
            string str2 = Console.ReadLine();

            string res = str1 == str2 ? "==" : "!=";
            Console.WriteLine($"{str1} {res} {str2}");

            res = String.Equals(str1,str2,StringComparison.CurrentCultureIgnoreCase) ? "==" : "!=";
            Console.WriteLine($"{str1} {res} {str2}");

            int r = String.Compare(str1, str2, true);
            if (r < 0)
            {
                Console.WriteLine($"{str1} < {str2}");
            } else if (r > 0)
            {
                Console.WriteLine($"{str1} > {str2}");
            }
            else
            {
                Console.WriteLine($"{str1} = {str2}");
            }
        }
    }
}
