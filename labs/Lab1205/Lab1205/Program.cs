﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1205
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] a = new int[7][];
            for (int i = 0; i < 7; i++)
            {
                a[i] = new int[i + 1];
                a[i] = Enumerable.Range(1, i+1).ToArray();
            }

            foreach (var row in a)
            {
                foreach (var number in row)
                {
                    Console.Write($"{number} ");
                }
                Console.WriteLine();
            }
        }
    }
}
