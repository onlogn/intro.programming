﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1129
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            while ((str = Console.ReadLine()) != "0")
            {
                int n = int.Parse(str);
                for (int i = 0; i < n; i++)
                {
                    List<int> arr = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse).ToList();
                    var res = arr.Where(ix => ix <= 127).ToList();
                    if (res.Count == 1)
                    {
                        int idx = arr.IndexOf(res[0]);
                        string r;
                        switch (idx)
                        {
                            case 0: r = "A";
                                break;
                            case 1: r = "B";
                                break;
                            case 2: r = "C";
                                break;
                            case 3: r = "D";
                                break;
                            default: r = "E";
                                break;
                        }
                        Console.WriteLine(r);
                    }
                    else
                    {
                        Console.WriteLine("*");
                    }
                }
            }
        }
    }
}
