﻿namespace Library
{
    partial class BookRedactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBookName = new System.Windows.Forms.Label();
            this.txtBookName = new System.Windows.Forms.TextBox();
            this.lblBookCat = new System.Windows.Forms.Label();
            this.comboBoxBookCat = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblBookName
            // 
            this.lblBookName.AutoSize = true;
            this.lblBookName.Location = new System.Drawing.Point(12, 9);
            this.lblBookName.Name = "lblBookName";
            this.lblBookName.Size = new System.Drawing.Size(63, 13);
            this.lblBookName.TabIndex = 0;
            this.lblBookName.Text = "BookName:";
            // 
            // txtBookName
            // 
            this.txtBookName.Location = new System.Drawing.Point(94, 6);
            this.txtBookName.Name = "txtBookName";
            this.txtBookName.Size = new System.Drawing.Size(318, 20);
            this.txtBookName.TabIndex = 1;
            // 
            // lblBookCat
            // 
            this.lblBookCat.AutoSize = true;
            this.lblBookCat.Location = new System.Drawing.Point(12, 51);
            this.lblBookCat.Name = "lblBookCat";
            this.lblBookCat.Size = new System.Drawing.Size(74, 13);
            this.lblBookCat.TabIndex = 0;
            this.lblBookCat.Text = "BookCategory";
            // 
            // comboBoxBookCat
            // 
            this.comboBoxBookCat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBookCat.FormattingEnabled = true;
            this.comboBoxBookCat.Location = new System.Drawing.Point(94, 48);
            this.comboBoxBookCat.Name = "comboBoxBookCat";
            this.comboBoxBookCat.Size = new System.Drawing.Size(318, 21);
            this.comboBoxBookCat.TabIndex = 2;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(229, 78);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(334, 78);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // BookRedactionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 107);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.comboBoxBookCat);
            this.Controls.Add(this.txtBookName);
            this.Controls.Add(this.lblBookCat);
            this.Controls.Add(this.lblBookName);
            this.Name = "BookRedactionForm";
            this.Text = "BookRedactionForm";
            this.Load += new System.EventHandler(this.BookRedactionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBookName;
        private System.Windows.Forms.TextBox txtBookName;
        private System.Windows.Forms.Label lblBookCat;
        private System.Windows.Forms.ComboBox comboBoxBookCat;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}