﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_2940
{
    class Program
    {
        static void Main(string[] args)
        {
            int v = int.Parse(Console.ReadLine());
            int t = int.Parse(Console.ReadLine());

            if(v>0)
                Console.WriteLine(v * t % 109);
            else
                Console.WriteLine((109 + v * t % 109)%109);
            
        }
    }
}
