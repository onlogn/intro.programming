﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0202
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Input b = ");
            int b = int.Parse(Console.ReadLine());

            //a = a ^ b;
            //b = b ^ a;
            //a = a ^ b;

            //a = a + b;
            //b = a - b;
            //a = a - b;

            a += b;
            b = a - b;
            a -= b;

            Console.WriteLine($"Now a = {a}, b = {b}");
        }
    }
}
