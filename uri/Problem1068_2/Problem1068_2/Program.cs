﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1068_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;

            while ((str = Console.ReadLine()) != null)
            {
                int cnt = 0;

                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == '(')
                    {
                        cnt++;
                    }

                    if (str[i] == ')')
                    {
                        cnt--;
                    }

                    if (cnt < 0)
                    {
                        break;
                    }
                }

                Console.WriteLine(cnt == 0 ? "correct" : "incorrect");
            }
        }
    }
}
