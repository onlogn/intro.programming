﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0601
{
    class Program
    {
        static void Main(string[] args)
        {
            int grade;
            int sum = 0;
            int cnt = 0;

            while ((grade = int.Parse(Console.ReadLine()))!=0)
            {
                sum += grade;
                cnt++;
            }

            double res = (double) sum / cnt;

            Console.WriteLine(cnt != 0 ? res.ToString("f1") : "No data");

        }
    }
}
