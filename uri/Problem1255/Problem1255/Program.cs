﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1255
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTests = int.Parse(Console.ReadLine());

            for (int cntTest = 0; cntTest < numOfTests; cntTest++)
            {
                string str = Console.ReadLine();

                str = str.ToLower();

                Dictionary<char, int> ch = new Dictionary<char, int>();

                foreach (char c in str)
                {
                    if (Char.IsLetter(c))
                    {
                        if (!ch.ContainsKey(c))
                        {
                            ch[c] = 0;
                        }

                        ch[c] += 1;
                    }
                }


                var res = ch.OrderByDescending(i => i.Value).ToArray();

                int max = ch.Max(i => i.Value);


                string outputStr = String.Empty;
                foreach (KeyValuePair<char, int> c in res)
                {
                    if (c.Value == max)
                    {
                        outputStr += c.Key;
                    }
                       
                }

                Console.WriteLine(String.Join("",outputStr.OrderBy(i=>i)));
            }

            
        }
    }
}
