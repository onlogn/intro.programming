﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1204
{
    class Program
    {
        static void Main(string[] args)
        {
            Miner m = new Miner(9,14);
            m.PutMines(15);
            Console.WriteLine(m.ShowField());
            m.AddValuesToField();
            Console.WriteLine(m.ShowField());
        }
    }
}
