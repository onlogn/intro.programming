﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3069
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            List<int> nums = new List<int>();
            while ((s = Console.ReadLine()) != "0")
            {
                nums.Add(int.Parse(s));
            }

            int cnt = 0;
            for (int i = 1; i < nums.Count; i++)
            {
                if (nums[i] > nums[i - 1])
                {
                    cnt++;
                }
            }

            Console.WriteLine(cnt);
           
        }
    }
}
