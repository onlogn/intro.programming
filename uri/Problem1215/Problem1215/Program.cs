﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Problem1215
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            SortedDictionary<string,int> sd = new SortedDictionary<string, int>();
            StringBuilder sb = new StringBuilder();
            while ((s = Console.ReadLine()) != null)
            {
                sb.Clear();
                s += ".";
                for (int i = 0; i < s.Length; i++)
                {
                    if (Char.IsLetter(s[i]))
                    {
                        sb.Append(s[i]);
                    }
                    else
                    {
                        if (sb.Length != 0)
                        {
                            sd[sb.ToString().ToLower()] = 0;
                        }

                        sb.Clear();
                    }
                }
            }
            Console.WriteLine(String.Join(Environment.NewLine,sd.Keys));
            Console.WriteLine();
            
        }
    }
}
