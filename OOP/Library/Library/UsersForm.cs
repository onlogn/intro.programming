﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.DataBaseWork;

namespace Library
{
    public partial class UsersForm : Form
    {
        private BindingList<User> _users;

        public UsersForm()
        {
            InitializeComponent();
            dgvUsers.AutoGenerateColumns = false;
        }

        private async void UsersForm_Load(object sender, EventArgs e)
        {
            _users = new BindingList<User>(await UserDataBaseService.GetUsersList());
            dgvUsers.DataSource = _users;
        }

        private async void btnAddUser_Click(object sender, EventArgs e)
        {
            UserRedactionDialog uf = new UserRedactionDialog();

            if (uf.ShowDialog(this) == DialogResult.OK)
            {
                int idx = await UserDataBaseService.AddUser(uf.CurrentUser);
                _users.Add(new User(idx, uf.CurrentUser.Name, uf.CurrentUser.Categories, uf.CurrentUser.MessageType));
            }
        }

        private async void btnEditUser_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count == 0)
                return;

            User userForEdit = (User) dgvUsers.SelectedRows[0].DataBoundItem;
            UserRedactionDialog uf = new UserRedactionDialog(userForEdit);

            if (uf.ShowDialog(this) == DialogResult.OK)
            {
                await UserDataBaseService.EditUser(userForEdit);
                _users[_users.IndexOf(userForEdit)] = new User(uf.CurrentUser.Id,uf.CurrentUser.Name,uf.CurrentUser.Categories,uf.CurrentUser.MessageType);
            }
        }

        private async void btnDeleteUser_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count == 0) return;

            User userForDelete = (User) dgvUsers.SelectedRows[0].DataBoundItem;
            await UserDataBaseService.DeleteUser(userForDelete);
            _users.Remove(userForDelete);
        }
    }
}
