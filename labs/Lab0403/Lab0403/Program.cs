﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0403
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Определение знака числа\nВведите произвольное целое число: ");
            int a = int.Parse(Console.ReadLine());

            if (a > 0)
            {
                Console.WriteLine($"Число {a} положительное");
            }
            else if (a < 0)
            {
                Console.WriteLine($"Число {a} отрицательное");
            }
            else
            {
                Console.WriteLine("Число равно 0");
            }

        }
    }
}
