﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem112145
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            string[] s = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            var r = s.Select(x => int.Parse(x)).ToArray();

            Console.Write($"{r[0]}+{r[1]}+{r[2]}=");
            Console.WriteLine(r.Sum());

            Console.Write($"{r[0]}*{r[1]}*{r[2]}=");
            Console.WriteLine(r.Aggregate((x,y)=>x*y));

            Console.Write($"({r[0]}+{r[1]}+{r[2]})/3=");
            Console.WriteLine($"{(r.Sum() / 3.0):f3}");
        }
    }
}
