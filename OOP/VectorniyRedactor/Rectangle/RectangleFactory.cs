﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Figures;
using Point = Figures.Point;

namespace Rectangle
{
    public class RectangleFactory :IFigureFactory
    {
        public string FigureName => "Rectangle";

        public Type FigureType => typeof(Rectangle);

        public IFigure CreateFigure(Point p, int w, int h, Color c)
        {
            Rectangle tmp =  new Rectangle(p, c);
            tmp.SetSize(w,h);
            return tmp;
        }

        public IFigure CreateFigure(Point p, Color c)
        {
            return new Rectangle(p, c);
        }

        public IFigure CreateFigure(FigureData f)
        {
            return new Rectangle(f);
        }
    }
}
