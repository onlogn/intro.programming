﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1519
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;

            while ((str = Console.ReadLine()) != ".")
            {
                string[] s = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                Dictionary<string, int> words = new Dictionary<string, int>();
                Dictionary<char, string> abbr = new Dictionary<char, string>();

                for (int i = 0; i < s.Length; i++)
                {
                    if (s[i].Length > 2)
                    {

                        if (!words.ContainsKey(s[i]))
                        {
                            words[s[i]] = 0;
                        }

                        words[s[i]]++;
                    }
                }

                for (int i = 0; i < s.Length; i++)
                {
                    char firstLetter = s[i][0];
                    if (s[i].Length <= 2) continue;
                    if (!abbr.ContainsKey(firstLetter))
                    {
                        abbr[firstLetter] = s[i];
                    }
                    else
                    {
                        int countCutSymbols = words[s[i]] * (s[i].Length - 2);
                        int abbrWordLen = words[abbr[firstLetter]] * (abbr[firstLetter].Length - 2);
                        if (countCutSymbols > abbrWordLen)
                        {
                            abbr[firstLetter] = s[i];
                        }
                    }
                }

                HashSet<string> wordsForAbbr = new HashSet<string>();

                foreach (var x in abbr)
                {
                    wordsForAbbr.Add(x.Value);
                }

                var sortedAbbr = abbr.OrderBy(x => x.Key);

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < s.Length; i++)
                {
                    if (wordsForAbbr.Contains(s[i]))
                    {
                        sb.Append(s[i][0]);
                        sb.Append(".");
                    }
                    else
                    {
                        sb.Append(s[i]);
                    }

                    sb.Append(" ");
                }

                Console.WriteLine(sb.ToString().Trim());

                Console.WriteLine($"{abbr.Count}");

                foreach (var x in sortedAbbr)
                {
                    Console.WriteLine($"{x.Key}. = {x.Value}");
                }

            }
        }
    }
}
