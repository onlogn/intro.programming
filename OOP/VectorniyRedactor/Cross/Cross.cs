﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Figures;
using Point = Figures.Point;

namespace Cross
{
    public class Cross : Figure
    {
        private Rectangle.Rectangle _r1;
        private Rectangle.Rectangle _r2;

        public Cross(Point p, Color c) : base(p, c)
        {
            _r1 = new Rectangle.Rectangle(p, c);
            _r2 = new Rectangle.Rectangle(new Point(Position.X - Height / 2 + Width / 2, Position.Y + Height / 2 - Width / 2), c);
        }

        public Cross(FigureData f) : this(new Point(f.X, f.Y), f.Color)
        {
            // делаем класс Cross sealed, потому-что вызов виртуального метода в конструкторе
            this.SetSize(f.Width, f.Height);
        }

        public override void SetSize(int w, int h)
        {
            Height = h;
            Width = w;
            _r1.SetSize(w, h);
            _r2.SetSize(h, w);
            int x = Position.X - Height / 2 + Width / 2;
            int y = Position.Y + Height / 2 - Width / 2;
            _r2.Move(new Point(x - _r2.Position.X, y - _r2.Position.Y));
        }

        public override void Draw(Graphics g)
        {
            _r1.Draw(g);
            _r2.Draw(g);
        }

        public override bool IsInFigureRange(Point p)
        {
            return _r1.IsInFigureRange(p) || _r2.IsInFigureRange(p);
        }

        public override void Move(Point p)
        {
            base.Move(p);
            _r1.Move(p);
            _r2.Move(p);
        }

        public override FigureData GetInfoForSerialization()
        {
            return new FigureData()
            {
                FigureType = typeof(Cross),
                Color = Color,
                Height = Height,
                Width = Width,
                X = Position.X,
                Y = Position.Y
            };
        }
    }
}
