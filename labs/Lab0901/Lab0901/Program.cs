﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0901
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.Write("Input X = ");
            int x = int.Parse(Console.ReadLine());

            Console.Write("Input X1 = ");
            int x1 = int.Parse(Console.ReadLine());

            Console.Write("Input X2 = ");
            int x2 = int.Parse(Console.ReadLine());

            int d1 = x1 - x;
            int d2 = x2 - x;
            if (d1 < d2)
            {
                Console.WriteLine($"First Number {x1} near {x}. Dist = {d1}");
            }
            else
            {
                Console.WriteLine($"Second Number {x2} near {x}. Dist = {d2}");
            }
        }

        public static int Abs(int x)
        {
            return x < 0 ? -x : x;
        }
    }
}
