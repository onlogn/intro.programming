﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1252
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            while ((s = Console.ReadLine()) != "0 0")
            {
                Console.WriteLine(s);
                int[] t = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

                int numOfSequence = t[0];
                int mod = t[1];

                int[] arr = new int[numOfSequence];

                for (int i = 0; i < numOfSequence; i++)
                {
                    arr[i] = int.Parse(Console.ReadLine());
                }
                // ThenBy(i => i % 2 == 0 ? i : i*-1)
                var res = arr.OrderBy(i => i % mod).ThenByDescending(i => i % 2 != 0).ThenBy(i => i % 2 == 0 ? i : 0)
                    .ThenByDescending(i => i % 2 != 0 ? i : 0).ToArray();
                Console.WriteLine(String.Join("\n", res));
            }
            Console.WriteLine(s);
        }
    }
}
