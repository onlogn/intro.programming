﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Figures;

namespace VectorniyRedactor
{
    public class History
    {
        public Operation Operation { get; private set; }
        public IFigure Figure { get; private set; }
        public Point PreviousCoord { get; private set; }
        public int Index { get; private set; }
        public Point NewCoord { get; private set; }

        //add
        public History(Operation operation, IFigure figure)
        {
            Operation = operation;
            Figure = figure;
        }

        //move
        public History(Operation operation, IFigure figure, Point previousCoord, Point newCoord)
        {
            Operation = operation;
            Figure = figure;
            PreviousCoord = previousCoord;
            NewCoord = newCoord;
        }

        //remove
        public History(Operation operation, IFigure figure, int index)
        {
            Operation = operation;
            Figure = figure;
            Index = index;
        }

    }
}
