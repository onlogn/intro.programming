﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab19graphic
{
    public sealed class Circle: Figure
    {
        private Point _center;

        public Circle(Point p, Color c):base(p,c)
        {
            
        }

        public Circle(FigureData f) : this(new Point(f.X, f.Y), f.Color)
        {
            this.SetSize(f.Width,f.Height);
        }

        public override void SetSize(int w, int h)
        {
            Width = w;
            Height = h;
            _center = new Point(Position.X + Width / 2, Position.Y + Height / 2);
        }

        public override void Draw(Graphics g)
        {
            Brush b = new SolidBrush(Color);
            g.FillEllipse(b, Position.X, Position.Y, Width, Height);
        }

        public override bool IsInFigureRange(Point p)
        {
            // (x-x0)^2/a^2 + (y-y0)^2/b^2 <= 1 (x0;y0) - center
            return Math.Pow(p.X - _center.X, 2) / Math.Pow(Width * 0.5, 2) +
                   Math.Pow(p.Y - _center.Y, 2) / Math.Pow(Height * 0.5, 2) <= 1;
        }

        public override string ToString()
        {
            return $"Ellipse: ({Position.X}, {Position.Y}) Width = {Width}, Height = {Height}";
        }

        public override void Move(Point p)
        {
            base.Move(p);
            _center = new Point(Position.X + Width / 2, Position.Y + Height / 2);
        }

        public override FigureData GetInfoForSerialization()
        {
            return new FigureData()
            {
                FigureType = typeof(Circle),
                Color = Color,
                Height = Height,
                Width = Width,
                X = Position.X,
                Y = Position.Y
            };
        }
    }
}
