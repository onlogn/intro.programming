﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2104
{
    class Program
    {
        private static int cnt;

        static void Main(string[] args)
        {
            List<Button> bs = new List<Button>();

            bs.Add(new Button("Hello World", 0, 0, 300, 100));
            bs.Add(new Button("Button 1", 0, 200, 50,50));
            bs.Add(new Button("Button 2", 100, 200, 50,50));

            bs[0].Click += ClickHandler1;
            bs[0].Click += allBtnClick;

            bs[1].Click += ClickHandler2;
            bs[1].Click += allBtnClick;

            bs[2].Click += ClickHandler2;
            bs[2].Click += allBtnClick;

            int[] input = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

            int x = input[0];
            int y = input[1];

            foreach (Button b in bs)
            {
                if (b.Contains(x, y))
                {
                    b.OnClick(b);
                }
            }

            Console.WriteLine(cnt);

        }

        public static void ClickHandler1(Button b)
        {
            Console.WriteLine("Hello world");
        }

        public static void ClickHandler2(Button b)
        {
            Console.WriteLine($"Button clicked {b.Text}");
        }

        public static void allBtnClick(Button b)
        {
            cnt++;
        }
    }
}
