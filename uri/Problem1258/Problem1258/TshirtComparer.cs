﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1258
{
    class TshirtComparer : IComparer<Tshirt>
    {
        public int Compare(Tshirt x, Tshirt y)
        {
            int tmp = String.Compare(x.Color, y.Color);
            if (tmp > 0)
            {
                return 1;
            }
            else if(tmp < 0)
            {
                return -1;
            }
            else
            {
                int tx = GetSize(x.Size);
                int ty = GetSize(y.Size);

                if (tx > ty)
                {
                    return 1;
                } else if (tx < ty)
                {
                    return -1;
                }
                else
                {
                    int st = String.Compare(x.OwnerName, y.OwnerName);
                    if (st > 0)
                    {
                        return 1;
                    }
                    else if (st < 0)
                    {
                        return -1;
                    } else
                    {
                        return 0;
                    }
                }
            }
        }

        private int GetSize(string s)
        {
            int res;
            switch (s)
            {
                case "P":
                    res = 1;
                    break;
                case "M":
                    res = 2;
                    break;
                default:
                    res = 3;
                    break;
            }

            return res;
        }
    }
}
