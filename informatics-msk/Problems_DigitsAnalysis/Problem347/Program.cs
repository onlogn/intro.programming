﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem347
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            bool noZero = true;

            for (int i = 0; i < n; i++)
            {
                noZero = noZero && Console.ReadLine() != "0";
            }

            Console.WriteLine(noZero ? "NO" : "YES");
        }
    }
}
