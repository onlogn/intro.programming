﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class MessageSenderFactory
    {
        public static IMessageSender Create(PreferredMessageType messageType)
        {
            switch (messageType)
            {
                case PreferredMessageType.SMS:
                    return new SmsSender();
                case PreferredMessageType.Call:
                    return new CallSender();
                case PreferredMessageType.Email:
                    return new EmailSender();
                case PreferredMessageType.Telegram:
                    return new TelegramSender();
                case PreferredMessageType.WhatsApp:
                    return new WhatsAppSender();
                default:
                    throw new Exception("Unknown Type Message");
            }
        }
    }
}
