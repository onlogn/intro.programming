﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1523
{
    class Program
    {
        static void Main(string[] args)
        {
            bool asc = true;
            bool ascw = true;
            bool desc = true;
            bool descw = true;
            bool constant = true;

            int cur;
            int prev = int.Parse(Console.ReadLine());

            while ((cur = int.Parse(Console.ReadLine())) != -2000000000)
            {
                if (prev >= cur) asc = false;
                if (prev > cur) ascw = false;
                if (prev <= cur) desc = false;
                if (prev < cur) descw = false;
                if (prev != cur) constant = false;
                prev = cur;
            }

            if (asc) Console.WriteLine("ASCENDING");
            else if (desc) Console.WriteLine("DESCENDING");
            else if (constant) Console.WriteLine("CONSTANT");
            else if (ascw) Console.WriteLine("WEAKLY ASCENDING");
            else if (descw) Console.WriteLine("WEAKLY DESCENDING");
            else Console.WriteLine("RANDOM");

            //bool asc = true;
            //bool ascw = true;
            //bool desc = true;
            //bool descw = true;
            //bool constant = true;

            //int cur;
            //int prev = int.Parse(Console.ReadLine());

            //while ((cur = int.Parse(Console.ReadLine())) != -2000000000)
            //{
            //    asc = asc && prev < cur;
            //    ascw = ascw && prev <= cur;
            //    desc = desc && prev > cur;
            //    descw = descw && prev >= cur;
            //    constant = constant && prev != cur;
            //    prev = cur;
            //}

            //if (asc) Console.WriteLine("ASCENDING");
            //else if (desc) Console.WriteLine("DESCENDING");
            //else if (constant) Console.WriteLine("CONSTANT");
            //else if (ascw) Console.WriteLine("WEAKLY ASCENDING");
            //else if (descw) Console.WriteLine("WEAKLY DESCENDING");
            //else Console.WriteLine("RANDOM");

        }


    }
}
