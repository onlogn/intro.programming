﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3065
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            int sum = 0;
            while ((s = Console.ReadLine()) != "0")
            {
                sum += int.Parse(s);
            }
            Console.WriteLine(sum);
        }
    }
}
