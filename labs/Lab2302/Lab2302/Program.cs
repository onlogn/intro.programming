﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2302
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //if (args.Length != 2)
                //    return;
                
                //string outputFile = args[0];
                string outputFile = "res.txt";
                string dir = @"C:\Programming\intro.programming\labs";

                //if (!Directory.Exists(args[1]))
                //{
                //    Console.WriteLine($"Folder {args[1]} doesn't exist");
                //    return;
                //}
                if (!Directory.Exists(dir))
                {
                    Console.WriteLine($"Folder {dir} doesn't exist");
                    return;
                }

                using (StreamWriter sw = new StreamWriter(outputFile))
                {
                    //ProcessDirectory(sw, args[1]);
                    ProcessDirectory(sw, dir);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void ProcessDirectory(StreamWriter sw, string dir)
        {
            string[] directoties = Directory.GetDirectories(dir);
            foreach (string d in directoties)
            {
                ProcessDirectory(sw,d);
            }
            string[] files = Directory.GetFiles(dir);
            for (int i = 1; i < files.Length; i++)
            {
                if (files[i].EndsWith("cs") && !files[i].Contains("Assembly") && !files[i].Contains("Designer") && !files[i].Contains("Temporaty"))
                {
                    sw.WriteLine(files[i]);
                    CopyFile(sw, files[i]);
                }
            }

        }

        private static void CopyFile(StreamWriter sw, string fileName)
        {
            using (StreamReader sr = new StreamReader(fileName))
            {
                int numLines = 1;
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    sw.WriteLine($"{numLines++,4} {line}");
                }
            }
        }
    }
}
