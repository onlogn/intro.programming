﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0604
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTests = int.Parse(Console.ReadLine());
            List<int> numbers = new List<int>();
            for (int i = 0; i < numOfTests; i++)
            {
                numbers.Add(int.Parse(Console.ReadLine()));
            }
            Console.WriteLine($"Max = {numbers.Max()}, Min = {numbers.Min()}");
        }
    }
}
