﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0502
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Y: ");
            int y = int.Parse(Console.ReadLine());
            Console.Write("M: ");
            int m = int.Parse(Console.ReadLine());

            //1 way
            Console.WriteLine(DateTime.DaysInMonth(y,m));

            // 2 way
            DateTime d = new DateTime(y,m,1);
            Console.WriteLine((d.AddMonths(1) - d).Days);


            //3way
            if (m > 12 || m < 1)
            {
                throw new Exception("Incorrect month");
            }
            
            if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)
            {
                Console.WriteLine(31);
            }
            else if (m == 4 || m == 6 || m == 9 || m == 11)
            {
                Console.WriteLine(30);
            }
            else if (m == 2 && (y % 400==0 || (y%4==0 && y%100!=0)))
            {
                Console.WriteLine(29);
            }
            else
            {
                Console.WriteLine(28);
            }

        }
    }
}
