﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1602_03_FractionsCalculator
{
    public class Rational
    {
        public int Nomerator { get; private set; }
        public int Denomerator { get; private set; }

        public Rational(int aNum, int aDen)
        {
            if (aDen == 0) throw new ArgumentException("Знаменатель не должен равняться 0");
            if (aDen < 0)
            {
                aNum = -aNum;
                aDen = Math.Abs(aDen);
            }

            int gcd = Gcd(Math.Abs(aNum), Math.Abs(aDen));
            Nomerator = aNum / gcd;
            Denomerator = aDen / gcd;
        }

        public Rational(int a)
        {
            Nomerator = a;
            Denomerator = 1;
        }

        public Rational(Rational a) : this(a.Nomerator, a.Denomerator)
        {

        }

        public Rational Sum(Rational a)
        {
            return new Rational(Nomerator * a.Denomerator + Denomerator * a.Nomerator, Denomerator * a.Denomerator);
        }

        public Rational Sub(Rational a)
        {
            return new Rational(Nomerator * a.Denomerator - Denomerator * a.Nomerator, Denomerator * a.Denomerator);
        }

        public Rational Mult(Rational a)
        {
            return new Rational(Nomerator * a.Nomerator, Denomerator * a.Denomerator);
        }

        public Rational Div(Rational a)
        {
            return new Rational(Nomerator * a.Denomerator, Denomerator * a.Nomerator);
        }

        public static Rational operator +(Rational a, Rational b)
        {
            return a.Sum(b);
        }

        public static Rational operator -(Rational a, Rational b)
        {
            return a.Sub(b);
        }

        public static Rational operator *(Rational a, Rational b)
        {
            return a.Mult(b);
        }

        public static Rational operator /(Rational a, Rational b)
        {
            return a.Div(b);
        }

        private int Gcd(int a, int b)
        {
            while (b > 0)
            {
                a %= b;
                int tmp = a;
                a = b;
                b = tmp;
            }

            return a;
        }

        //public static Rational Parse(string expr)
        //{
        //    Regex r = new Regex(@"(\d+)+/(\d+)+");
        //    if (r.IsMatch(expr))
        //    {
        //        var tmp = r.Match(expr);
        //        return new Rational(int.Parse(tmp.Groups[1].ToString()), int.Parse(tmp.Groups[2].ToString()));
        //    }
        //    if (int.TryParse(expr,out int a))
        //    {
        //        return new Rational(a);
        //    }
        //    throw new FormatException("Необходимо вводить дробь");
        //}

        public static Rational Parse(string expr)
        {
            List<int> res = expr.Split(new[] {'/', ' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse)
                .ToList();
            switch (res.Count)
            {
                case 1:
                    return new Rational(res[0], 1);
                case 2:
                    return new Rational(res[0], res[1]);
                default:
                    throw new Exception("Incorrect Fraction");
            }
        }

        public override string ToString()
        {
            return $"{Nomerator}/{Denomerator}";
        }

        private bool Equals(Rational a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Nomerator, Nomerator) && Equals(a.Denomerator, Denomerator);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Rational)) return false;
            return Equals((Rational)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Nomerator.GetHashCode();
            res = res * num + Denomerator.GetHashCode();
            return res;
        }
    }

}