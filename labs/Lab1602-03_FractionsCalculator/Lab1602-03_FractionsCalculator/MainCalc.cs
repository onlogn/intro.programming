﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1602_03_FractionsCalculator
{
    public partial class MainCalc : Form
    {
        private Rational _fracResult;
        private Operations? _operation;
        private bool _isSuccess = false;

        private enum Operations 
        {
            Add,
            Subtract,
            Multiply,
            Divide
        }
        
        public MainCalc()
        {
            InitializeComponent();
        }
        
        private void MainCalc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Oemplus)
            {
                btnPlus.PerformClick();
            }
            else if (e.KeyCode == Keys.Subtract || e.KeyCode == Keys.OemMinus)
            {
                btnMinus.PerformClick();
            }
            else if (e.KeyCode == Keys.Multiply || (e.KeyCode == Keys.D8 && e.Shift))
            {
                btnMult.PerformClick();
            }
            else if (e.KeyCode == (Keys)186) 
            {
                btnDiv.PerformClick();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                btnRes.PerformClick();
            }
            else if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.NumPad0)
            {
                btn0.PerformClick();
            }
            else if (e.KeyCode == Keys.D1 || e.KeyCode == Keys.NumPad1)
            {
                btn1.PerformClick();
            }
            else if (e.KeyCode == Keys.D2 || e.KeyCode == Keys.NumPad2)
            {
                btn2.PerformClick();
            }
            else if (e.KeyCode == Keys.D3 || e.KeyCode == Keys.NumPad3)
            {
                btn3.PerformClick();
            }
            else if (e.KeyCode == Keys.D4 || e.KeyCode == Keys.NumPad4)
            {
                btn4.PerformClick();
            }
            else if (e.KeyCode == Keys.D5 || e.KeyCode == Keys.NumPad5)
            {
                btn5.PerformClick();
            }
            else if (e.KeyCode == Keys.D6 || e.KeyCode == Keys.NumPad6)
            {
                btn6.PerformClick();
            }
            else if (e.KeyCode == Keys.D7 || e.KeyCode == Keys.NumPad7)
            {
                btn7.PerformClick();
            }
            else if (e.KeyCode == Keys.D8 || e.KeyCode == Keys.NumPad8)
            {
                btn8.PerformClick();
            }
            else if (e.KeyCode == Keys.D9 || e.KeyCode == Keys.NumPad9)
            {
                btn9.PerformClick();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                btnClearAll.PerformClick();
            }
            else if (e.KeyCode == Keys.Divide || e.KeyCode == Keys.OemQuestion)
            {
                btnFracSeparator.PerformClick(); 
            }
            else if (e.KeyCode == Keys.Back)
            {
                btnBack.PerformClick();
            }
        }

        public void ButtonClick(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button) sender;
                if (_isSuccess) ClearData();
                switch (b.Name)
                {
                    case nameof(btnMinus):
                        GetAnswer(Operations.Subtract);
                        break;
                    case nameof(btnMult):
                        GetAnswer(Operations.Multiply);
                        break;
                    case nameof(btnDiv):
                        GetAnswer(Operations.Divide);
                        break;
                    case nameof(btnPlus):
                        GetAnswer(Operations.Add);
                        break;
                    case nameof(btnClearAll):
                        ClearData();
                        break;
                    case nameof(btnBack):
                        txtFraction.Text = txtFraction.Text.Length == 0
                            ? ""
                            : txtFraction.Text.Substring(0, txtFraction.Text.Length - 1);
                        break;
                    case nameof(btnRes):
                        //if(_operation != null)
                        if(_operation.HasValue)
                            GetAnswer(_operation.Value);
                        break;
                    default:
                        txtFraction.Text += b.Text;
                        break;
                }

                lblCurrentData.Focus();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private Rational Solve(Rational a, Rational b, Operations operation)
        {
            switch (operation)
            {
                case Operations.Add:
                    return a + b;
                case Operations.Subtract:
                    return a - b;
                case Operations.Multiply:
                    return a * b;
                case Operations.Divide:
                    return a / b;
                default:
                    throw new Exception("Invalid operation");
            }
            
        }

        private void AddText(string text)
        {
            lblCurrentData.Text += text;
        }

        private void ClearData()
        {
            _fracResult = null;
            lblCurrentData.Text = String.Empty;
            txtFraction.Text = String.Empty;
            _isSuccess = false;
            _operation = null;
        }

        private string OperationToString(Operations op)
        {
            switch (op)
            {
                case Operations.Add:
                    return "+";
                case Operations.Divide:
                    return ":";
                case Operations.Multiply:
                    return "*";
                case Operations.Subtract:
                    return "-";
                default: throw new Exception("Unknown Operation");
            }
        }

        private void GetAnswer(Operations operation)
        {
            Rational tmpFrac = Rational.Parse(txtFraction.Text);
            if (_fracResult == null)
            {
                _fracResult = tmpFrac;
                _operation = operation;

                AddText(tmpFrac + OperationToString(_operation.Value));
                txtFraction.Clear();
            }

            else if (!_operation.HasValue)
            {
                throw new Exception("Input op");
            }
            else
            {

                Rational ans = Solve(_fracResult, tmpFrac, _operation.Value);
                _fracResult = ans;

                AddText(tmpFrac + "=" + ans);
                txtFraction.Clear();
                _fracResult = null;
                _isSuccess = true;
            }
            
            
        }
 
    }
}
