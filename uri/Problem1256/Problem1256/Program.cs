﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1256
{
    class Program
    {
        static void Main(string[] args)
        {
            int numTests = int.Parse(Console.ReadLine());
            for (int nTest = 0; nTest < numTests; nTest++)
            {
                string str1 = Console.ReadLine();
                string str2 = Console.ReadLine();

                string[] s1 = str1.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                int k = int.Parse(s1[0]);

                string[] s2 = str2.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                int[] v = s2.Select(i => int.Parse(i)).ToArray();


                if (nTest != 0)
                {
                    Console.WriteLine();
                }

                Dictionary<int, List<int>> d = new Dictionary<int, List<int>>();

                for (int i = 0; i < k; i++)
                {
                    d[i] = new List<int>();
                }

                for (int i = 0; i < v.Length; i++)
                {
                    d[v[i] % k].Add(v[i]);
                }

                foreach (var i in d)
                {
                    Console.WriteLine($"{i.Key} -> {i.Value.ListToStr()}");
                }

                
            }
        }
    }

    public static class StringExtension
    {
        public static string ListToStr(this List<int> n)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var i in n)
            {
                sb.Append(i);
                sb.Append(" -> ");
            }

            sb.Append(@"\");

            return sb.ToString();
        }
    }
}
