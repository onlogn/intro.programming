﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListArr
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new[] {1,2,3,4,5,6,7,8,9};
            int[] b = new[] {10,11,12};

            ListArray<int> x = new ListArray<int>(a);
            //Console.WriteLine(x);

            //x.Add(2);
            //Console.WriteLine(x);

            x.AddRange(b);
            Console.WriteLine(x);

            int ind = 6;
            Console.WriteLine(x.IndexOf(ind));

            x.Insert(1,10);
            Console.WriteLine(x);

            x.Remove(10);
            Console.WriteLine(x);

            Console.WriteLine();

            foreach (var i in x)
            {
                Console.Write($"{i} ");
            }
            List<int> z = new List<int>();

            ListArray<int> v = new ListArray<int>(50);

            Console.WriteLine(v.Length);
        }

    }
}
