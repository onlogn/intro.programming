﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1049
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string,string> d = new Dictionary<string, string>()
            {
                {"vertebrado ave carnivoro " ,"aguia"},
                {"vertebrado ave onivoro " ,"pomba"},
                {"vertebrado mamifero onivoro " ,"homem"},
                {"vertebrado mamifero herbivoro " ,"vaca"},
                {"invertebrado inseto hematofago " ,"pulga"},
                {"invertebrado inseto herbivoro " ,"lagarta"},
                {"invertebrado anelideo hematofago " ,"sanguessuga"},
                {"invertebrado anelideo onivoro " ,"minhoca"}
            };

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 3; i++)
            {
                sb.Append(Console.ReadLine().Trim());
                sb.Append(" ");
            }

            Console.WriteLine(d[sb.ToString()]);
        }
    }
}
