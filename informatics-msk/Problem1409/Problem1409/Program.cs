﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1409
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();
            string s = Console.ReadLine();

            string[] n = s.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            var res = n.Select(x => int.Parse(x)).OrderBy(i => i).ToArray();

            var s1 = res.First();
            var s2 = res.Skip(1).Take(1);
            Console.WriteLine($"{s1} {s2.First()}");
            //Console.WriteLine($"{res[0]} {res[1]}");
           
        }
    }
}
