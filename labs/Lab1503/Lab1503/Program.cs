﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1503
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] str = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            StringBuilder sb = new StringBuilder();
            List<string> sl = new List<string>();

            foreach (var v in str)
            {
                sb.Append(char.ToUpper(v[0]));
                sb.Append(v.Substring(1));

                sl.Add(sb.ToString());
                sb.Clear();
            }

            Console.WriteLine(sl.ListToStr());
        }
    }

    public static class StringExtension
    {
        public static string ListToStr(this List<string> sl)
        {
            return String.Join(" ", sl);
        }
    }
}
