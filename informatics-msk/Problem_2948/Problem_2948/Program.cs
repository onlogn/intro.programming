﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_2948
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int ss = n % 60;
            int mm = n / 60;
            int hh = mm / 60 % 24;
            mm = mm % 60;
          
            Console.WriteLine($"{hh}:{mm.ToString("D2")}:{ss.ToString("D2")}");
        }
    }
}
