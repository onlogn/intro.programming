﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1068
{
    class Program
    {
        static void Main(string[] args)
        {
           string str;

            while ((str = Console.ReadLine()) != null)
            {

                Stack<char> res = new Stack<char>();

                bool flag = true;

                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == '(')
                    {
                        res.Push(str[i]);
                    }

                    if (str[i] == ')')
                    {
                        if (res.Count != 0)
                        {
                            res.Pop();
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }

                Console.WriteLine(flag && res.Count == 0 ? "correct" : "incorrect");
            }
        }
    }
}
