﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1803
{
    class Program
    {
        static void Main(string[] args)
        {
            const int n = 10000000;

           
            Stopwatch sw = Stopwatch.StartNew();
            RefType[] a = new RefType[n];
            for (int i = 0; i < n; i++)
            {
                a[i] = new RefType(i, i+1);
            }
            sw.Stop();
            Console.WriteLine($"Time = {sw.ElapsedMilliseconds}");
            
            Stopwatch sw2 = Stopwatch.StartNew();
            ValType[] b = new ValType[n];
            for (int i = 0; i < n; i++)
            {
                b[i] = new ValType(i, i + 1);
            }
            sw2.Stop();
            Console.WriteLine($"Time = {sw2.ElapsedMilliseconds}");

        }
    }

    public class RefType
    {
        public int Field1;
        public int Field2;

        public RefType(int f1, int f2)
        {
            Field1 = f1;
            Field2 = f2;
        }

        public long Compute()
        {
            return (long)Field1 + Field2;
        }
    }

    public struct ValType
    {
        public int Field1;
        public int Field2;

        public ValType(int f1, int f2)
        {
            Field1 = f1;
            Field2 = f2;
        }

        public long Compute()
        {
            return (long)Field1 + Field2;
        }
    }
}
