﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DataBaseModels
{
    [Table("Logs")]
    public class LogData
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string BookName { get; set; }
        public DateTime Date { get; set; }
        public string TypeOfMessage { get; set; }
    }
}
