﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1248
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTests = int.Parse(Console.ReadLine());
            for (int ix = 0; ix < numOfTests; ix++)
            {
                string d = Console.ReadLine();
                string s1 = Console.ReadLine();
                string s2 = Console.ReadLine();

                string s = s1 + s2;

                var need = d.Except(s).OrderBy(i => i).ToArray();

                if (s.Except(d).Any())
                {
                    Console.WriteLine("CHEATER");
                }
                else
                {
                    foreach (var i in need)
                    {
                        Console.Write(i);
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
