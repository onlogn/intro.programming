﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1202
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = new int[,]
            {
                { 11,12,13},
                { 21,22,23}
            };

            Matrix a = new Matrix(arr);

            Console.WriteLine(a);
            Console.WriteLine(a.RotateToLeft());
            Console.WriteLine(a.RotateToRight());
        }
    }
}
