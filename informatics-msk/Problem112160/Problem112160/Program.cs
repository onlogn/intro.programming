﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem112160
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();

            string[] s = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            Dictionary<string, int> d = new Dictionary<string, int>();
            string c;
            for (int i = 0; i < s.Length; i++)
            {
                c = s[i];
                if (!d.ContainsKey(c))
                    d[c] = 1;
                else
                    d[c]++;
                
            }

            int max = d.Values.Max();
            Console.WriteLine(max == 1 ? 0 : max);


        }
    }
}
