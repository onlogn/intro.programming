﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Figures;
using Point = Figures.Point;

namespace VectorniyRedactor
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private const string ExtFolder = "ext";

        private List<IFigure> _figures = new List<IFigure>();
        private IFigure _selectedFigure;
        private IFigureFactory _currentFactory;
        private Color _currentColor;
        private Point _coordForMove;
        private List<Type> _extTypes = new List<Type>();
        private Dictionary<Button,IFigureFactory> _d = new Dictionary<Button, IFigureFactory>();

        private List<History> _undo = new List<History>();
        private List<History> _redo = new List<History>();
        private Point _prevCoord;

        private void GraphicRedactorForm_Load(object sender, EventArgs e)
        {
            _currentColor = Color.Black;
            btnColor.BackColor = _currentColor;

            DirectoryInfo dirInfo = new DirectoryInfo(ExtFolder);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }

            string[] dlls = dirInfo.GetFiles().Where(i => i.Extension == ".dll").Select(i => i.FullName).ToArray();

            foreach (string d in dlls)
            {
                CreateInfoPanel(d);
            }
        }

        
        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            foreach (IFigure f in _figures)
            {
                Graphics g = e.Graphics;
                f.Draw(g);
            }
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _currentFactory == null)
            {
                _selectedFigure = null;
                _coordForMove = null;
                foreach (var f in _figures)
                {
                    Point tmp = new Point(e.X, e.Y);
                    if (f.IsInFigureRange(tmp))
                    {
                        _selectedFigure = f;
                        _coordForMove = new Point(e.X - f.Position.X, e.Y - f.Position.Y);
                        _prevCoord = new Point(f.Position);
                    }
                }
            }
            if (_currentFactory == null)
            {
                return;
            }
            _figures.Add(_currentFactory.CreateFigure(new Point(e.X, e.Y), _currentColor));
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _currentFactory != null)
            {
                IFigure tmp = _figures.Last();
                tmp.SetSize(e.X - tmp.Position.X, e.Y - tmp.Position.Y);
                pictureBox.Refresh();
            }
            else if (_currentFactory == null && _selectedFigure != null && e.Button == MouseButtons.Left)
            {
                _selectedFigure.Move(new Point(e.X - _selectedFigure.Position.X - _coordForMove.X, e.Y - _selectedFigure.Position.Y - _coordForMove.Y));
                pictureBox.Refresh();
            }

        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (_selectedFigure == null && _currentFactory == null)
            {
                return;
            }
            Operation op;
            if (_currentFactory == null)
            {
                op = Operation.Move;
                _undo.Add(new History(op, _selectedFigure,_prevCoord,_selectedFigure.Position));
            }
            else
            {
                op = Operation.Add;
                _undo.Add(new History(op, _figures.Last()));
            }
            btnUndo.Enabled = true;
            _redo.Clear();
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog(this) == DialogResult.OK)
            {
                _currentColor = colorDialog.Color;
                btnColor.BackColor = _currentColor;
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                SaveLoad.Save(_figures, saveFileDialog.FileName);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                _selectedFigure = null;
                _figures = SaveLoad.Load(openFileDialog.FileName, _d.Values.ToList());
                pictureBox.Refresh();
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            _currentFactory = null;
        }

        private void btnClr_Click(object sender, EventArgs e)
        {
            _figures.Clear();
            pictureBox.Refresh();
        }

        private void btnExt_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog.ShowDialog(this) == DialogResult.OK)
                {
                    string path = openFileDialog.FileName;
                    string newPath = $"{ExtFolder}\\{Path.GetFileName(path)}";
                    FileInfo fileInf = new FileInfo(path);
                    if (fileInf.Exists)
                    {
                        fileInf.CopyTo(newPath, true);
                    }
                    CreateInfoPanel(newPath);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error");
            }
            
        }

        private void CreateInfoPanel(string dllfileName)
        {
            Type tmpType = SaveLoad.LoadType(dllfileName);
            _extTypes.Add(tmpType);

            Button btn = new Button();
            btn.Width = 116;
            btn.Height = 30;
            btn.Click += btnFigure_Click;


            IFigureFactory tmpF = (IFigureFactory) Activator.CreateInstance(tmpType);
            _d.Add(btn,tmpF);
            btn.Text = tmpF.FigureName;


            flpFigures.Controls.Add(btn);
        }

        private void btnFigure_Click(object sender, EventArgs e)
        {
            _currentFactory = _d[(Button) sender];
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (_selectedFigure != null && e.KeyCode == Keys.Delete)
            {
                _undo.Add(new History(Operation.Remove,_selectedFigure,_figures.IndexOf(_selectedFigure)));
                _redo.Clear();
                btnUndo.Enabled = true;

                _figures.Remove(_selectedFigure);
                _selectedFigure = null;
                pictureBox.Refresh();
            }
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            History last = _undo.Last();
            _undo.Remove(last);
            _redo.Add(last);

            btnRedo.Enabled = true;

            if (last.Operation == Operation.Add)
            {
                _figures.Remove(last.Figure);
            }
            else if (last.Operation == Operation.Move)
            {
                last.Figure.Move(last.PreviousCoord - last.Figure.Position);
            }
            else if (last.Operation == Operation.Remove)
            {
                _figures.Insert(last.Index,last.Figure);
            }

            pictureBox.Refresh();

            if (!_undo.Any())
            {
                btnUndo.Enabled = false;
            }
        }

        private void btnRedo_Click(object sender, EventArgs e)
        {
            History last = _redo.Last();
            _redo.Remove(last);
            _undo.Add(last);

            btnUndo.Enabled = true;

            if (last.Operation == Operation.Add)
            {
                _figures.Add(last.Figure);
            }
            else if (last.Operation == Operation.Move)
            {
                last.Figure.Move(last.NewCoord - last.Figure.Position);
            }
            else if (last.Operation == Operation.Remove)
            {
                _figures.Remove(last.Figure);
            }

            pictureBox.Refresh();

            if (!_redo.Any())
            {
                btnRedo.Enabled = false;
            }
        }
    }

}
