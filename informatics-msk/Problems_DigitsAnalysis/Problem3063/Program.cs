﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3063
{
    class Program
    {
        static void Main(string[] args)
        {
            double x = double.Parse(Console.ReadLine());
            double p = double.Parse(Console.ReadLine());
            double y = double.Parse(Console.ReadLine());

            //Console.WriteLine(Math.Ceiling(Math.Log(y / x, 1 + p / 100)));

            int cnt = 0;
            while (x < y)
            {
                cnt++;
                x = Math.Floor(x * (1 + p / 100) * 100) / 100;
            }
            Console.WriteLine(cnt);
        }
    }
}
