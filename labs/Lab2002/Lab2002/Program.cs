﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArrayAlgo;
using Lab2001;
using ArrayAlgo = ArrayAlgo.ArrayAlgo;

namespace Lab2002
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = {5, 1, 6, 9, 40, 11};
            Console.WriteLine(a.ArrToStr());
            global::ArrayAlgo.ArrayAlgo.SelectSort(a);
            Console.WriteLine(a.ArrToStr());

            Student[] s = new[]
            {
                new Student("Zam", 1998, 3.5),
                new Student("Alesha", 1999, 1.5),
                new Student("Vasya", 1997, 5.5),
            };

            Console.WriteLine(s.ArrToStr());
            global::ArrayAlgo.ArrayAlgo.SelectSort(s, new CmpStudentsByName());

        }
    }

    public class CmpStudentsByName : IMyComparer
    {
        public int Compare(object o1, object o2)
        {
            throw new NotImplementedException();
        }
    }

    public static class StringExtension
    {
        public static string ArrToStr<T>(this T[] arr)
        {
            return String.Join(" ", arr);
        }
    }
}
