﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1121
{
    class Program
    {
        static void Main(string[] args)
        {
            string s1;

            while ((s1 = Console.ReadLine()) != "0 0 0")
            {
                string[] d = s1.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                int[] data = d.Select(i => int.Parse(i)).ToArray();

                List<string> area = new List<string>();
                for (int i = 0; i < data[0]; i++)
                {
                    area.Add(Console.ReadLine());
                }

                int cnt = 0;

                string actions = Console.ReadLine();

                Point currentPos = Robot.FindRobot(area, out char tmp_direction);

                List<Point> movesList = Robot.GetMovesList(actions, tmp_direction);

                HashSet<Point> visited = new HashSet<Point>();

                for (int i = 0; i < movesList.Count; i++)
                {
                    Point next_position = currentPos + movesList[i];

                    if (next_position.Y >= area.Count || next_position.X >= area[0].Length || next_position.Y < 0 ||
                        next_position.X < 0)
                    {
                        continue;
                    }
                    else if (area[next_position.Y][next_position.X] == '#')
                    {
                        continue;
                    }
                    else if (area[next_position.Y][next_position.X] == '*'
                             && !visited.Contains(next_position))
                    {
                        cnt++;
                        visited.Add(next_position);
                        currentPos = next_position;
                    }
                    else
                    {
                        currentPos = next_position;
                    }





                }

                Console.WriteLine(cnt);
            }
        }
    }
}
