﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0305
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine(((x % 4 == 0 && x % 100 != 0) || x % 400 == 0) ? "YES":"NO");
        }
    }
}
