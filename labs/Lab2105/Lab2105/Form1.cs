﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2105
{
    public partial class Form1 : Form
    {
        private int _cnt;
        public Form1()
        {
            InitializeComponent();
            btnSayHello.Click += AllButtonsClick;
            button2.Click += AllButtonsClick;
            button3.Click += AllButtonsClick;

        }

        private void btnSayHello_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello World");
        }

        private void BtnClick(object sender, EventArgs e)
        {
            MessageBox.Show($"{(sender as Button).Text}");
        }

        public void AllButtonsClick(object sender, EventArgs e)
        {
            _cnt++;
        }
    }
}
