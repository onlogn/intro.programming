﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Problem1445
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1 = Console.ReadLine();
            string[] s1 = str1.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            string str2 = Console.ReadLine();
            string[] s2 = str2.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            int m = int.Parse(s1[0]), n = int.Parse(s1[1]), x = int.Parse(s2[0]), y  = int.Parse(s2[1]);

            if (x > 1)
                Console.WriteLine($"{x - 1} {y}");
            if (x < m)
                Console.WriteLine($"{x + 1} {y}");
            if (y > 1)
                Console.WriteLine($"{x} {y-1}");
            if (y < n)
                Console.WriteLine($"{x} {y+1}");


        }
    }
}
