﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Lab2502
{
    class Program
    {
        static void Main(string[] args)
        {
            HashSet<string> users = new HashSet<string>();
            
            string input;
            while ((input = Console.ReadLine()) != null)
            {
                var tmp = input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                string operation = tmp[0];
                string userName = tmp[1];

                switch (operation)
                {
                    case "#":
                        users.Print();
                        break;
                    case "+":
                        Console.WriteLine(users.Add(userName) ? "Success" : "Input another name");
                        break;
                    case "-":
                        users.Remove(userName);
                        Console.WriteLine("OK");
                        break;
                    case "?":
                        Console.WriteLine(users.Contains(userName) ? "Online" : "Offline");
                        break;
                    default:
                        throw new Exception();
                }
            }
        }
    }

    public static class Utils
    {
        public static void Print<T>(this IEnumerable<T> content)
        {
            int cnt = 1;
            foreach (T e in content.OrderBy(s=>s))
            {
                Console.WriteLine($"{cnt++}: {e}");
            }
        }
    }
}
