﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1005
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            string[] strArr = new string[n];
            for (int i = 0; i < n; i++)
            {
                strArr[i] = Console.ReadLine();
            }

            Console.WriteLine(String.Join(" ",strArr));

            ArrayReverse(strArr);

            Console.WriteLine(String.Join(" ", strArr));

        }

        static void ArrayReverse<T>(T[] array)
        {
            for (int i = 0; i < array.Length / 2; i++)
            {
                var tmp = array[i];
                array[i] = array[array.Length - i - 1];
                array[array.Length - i - 1] = tmp;
            }
        }
    }

}
