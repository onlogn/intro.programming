﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1424
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            while ((s = Console.ReadLine()) != null)
            {
                int[] input = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

                int size = input[0];
                int q = input[1];

                Dictionary<int, List<int>> d = new Dictionary<int, List<int>>();
                int[] arr = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse).ToArray();
                for (int i = 0; i < arr.Length; i++)
                {
                    if (!d.ContainsKey(arr[i]))
                    {
                        d[arr[i]] = new List<int>();
                    }

                    d[arr[i]].Add(i);
                }

                for (int i = 0; i < q; i++)
                {
                    int[] tmp = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse).ToArray();
                    int key = tmp[1];
                    int idx = tmp[0] - 1;

                    if (d.ContainsKey(key) && (idx >= 0 && idx < d[key].Count))
                    {
                        Console.WriteLine(d[key][idx]+1);
                    }
                    else
                    {
                        Console.WriteLine("0");
                    }

                }
            }
        }
    }
}
