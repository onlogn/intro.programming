﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2401
{
    public class MarksList<T> : IEnumerable<T>
    {
        public int LBound { get; }
        public int RBound { get; }
        private T[] _data;

        public T this[int ix]
        {
            get
            {
                CheckRange(ix);
                return _data[ix - LBound];
            }
            set
            {
                CheckRange(ix);
                _data[ix - LBound] = value;
            }
        }

        public MarksList(int lBound, int rBound)
        {
            if (lBound > rBound)
            {
                throw new ArgumentException("Incorrect arguments. LowerBound > UpperBound");
            }
            LBound = lBound;
            RBound = rBound;
            _data = new T[RBound - LBound + 1];
        }


        private void CheckRange(int ix)
        {
            if (ix < LBound || ix > RBound)
            {
                throw new Exception("Incorrect index");
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            //return new MarksListIterator<T>(this);
            foreach (T e in _data)
            {
                yield return e;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            //return new MarksListIterator<T>(this);
            return _data.GetEnumerator();
        }
    }

    //class MarksListIterator<T> : IEnumerator<T>
    //{
    //    private MarksList<T> _list;
    //    private int curIx = -1;

    //    public MarksListIterator(MarksList<T> m)
    //    {
    //        _list = m;
    //    }

    //    public void Dispose()
    //    {
            
    //    }

    //    public bool MoveNext()
    //    {
    //        ++curIx;
    //        if(curIx <= _list.RBound)
    //        {
    //            return true;
    //        }

    //        return false;
    //    }

    //    public void Reset()
    //    {
    //        curIx = -1;
    //    }

    //    public T Current => _list[curIx];

    //    object IEnumerator.Current => Current;
    //}
}
