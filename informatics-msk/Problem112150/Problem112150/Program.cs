﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem112150
{
    class Program
    {
        static void Main(string[] args)
        {
            string r = Console.ReadLine();
            string[] s = r.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            double x = double.Parse(s[0]);
            double y = double.Parse(s[1]);

            Console.WriteLine($"{Math.Pow(x, y):f3}");
        }
    }
}
