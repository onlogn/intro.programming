﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2504
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, List<string>> d= new Dictionary<string, List<string>>();

            d.Add("set",new List<string>(){"Множество", "Закат", "Установить"});

            d["hey"] = new List<string>(){"Privetstvie", "heeeee"};

            d.Print();
        }
    }

    public static class Utils
    {
        public static void Print<T>(this Dictionary<T, List<T>> d)
        {
            foreach (KeyValuePair<T, List<T>> keyValuePair in d)
            {
                Console.Write($"{keyValuePair.Key} => ");
                foreach (var e in keyValuePair.Value)
                {
                    Console.Write($"{e} ");
                }
                Console.WriteLine();
            }
        }
    }
}
