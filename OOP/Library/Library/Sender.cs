﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DataBaseWork;

namespace Library
{
    public class SmsSender : IMessageSender
    {
        public async Task SendMessage(User user, Book book)
        {
            await LogService.WriteLog(book, user);
        }
    }

    public class WhatsAppSender : IMessageSender
    {
        public async Task SendMessage(User user, Book book)
        {
            await LogService.WriteLog(book, user);
        }
    }

    public class TelegramSender : IMessageSender
    {
        public async Task SendMessage(User user, Book book)
        {
            await LogService.WriteLog(book, user);
        }
    }

    public class EmailSender : IMessageSender
    {
        public async Task SendMessage(User user, Book book)
        {
            await LogService.WriteLog(book, user);
        }
    }

    public class CallSender : IMessageSender
    {
        public async Task SendMessage(User user, Book book)
        {
            await LogService.WriteLog(book, user);
        }
    }
}
