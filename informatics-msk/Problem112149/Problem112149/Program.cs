﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem112149
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string s = Console.ReadLine();
            string[] r = s.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            Tuple<double, double> p1 = new Tuple<double, double>(double.Parse(r[0]),double.Parse(r[1]));
            s = Console.ReadLine();
            r = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Tuple<double, double> p2 = new Tuple<double, double>(double.Parse(r[0]), double.Parse(r[1]));

            Console.WriteLine($"{Math.Sqrt((p2.Item1 - p1.Item1)*(p2.Item1 - p1.Item1) + (p2.Item2 - p1.Item2)* (p2.Item2 - p1.Item2)):f3}");
        }
    }
}
