﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1105
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next();
            }

            int[] find = new int[n*2];
            for (int i = 0; i < arr.Length * 2; i++)
            {
                find[i] = rnd.Next();
            }

            Stopwatch s1 = new Stopwatch();
            s1.Start();
            
            for (int i = 0; i < find.Length; i++)
            {
                LinearSearch(arr, find[i]);
            }
            Console.WriteLine($"LinearDearch: {s1.ElapsedMilliseconds} ms");
            s1.Stop();
            s1.Restart();
            Array.Sort(arr);
            
            
            for (int i = 0; i < find.Length; i++)
            {
                Array.BinarySearch(arr, find[i]);
            }
            Console.WriteLine($"BinarySearch: {s1.ElapsedMilliseconds} ms");
        }

        public static int LinearSearch(int[] arr, int item)
        {
            int found = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == item)
                    found++;
            }

            return found;
        }

    }
}
