﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1004
{
    public partial class Form1 : Form
    {
        List<Ball> balls = new List<Ball>();

        List<Ball> delete = new List<Ball>();
        
        Random rnd = new Random();

       public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;

            foreach (var i in balls)
            {
                Brush b1 = new SolidBrush(i.BallColor);
                g.FillEllipse(b1, (int)i.X, (int)i.Y, i.Diameter, i.Diameter);
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            balls.Add(Ball.GenerateRandomBall(rnd.Next(0,Width - 30)));

            foreach (var i in balls)
            {
                if (!i.MoveBall(Height))
                {
                    delete.Add(i);
                }
            }

            balls = balls.Except(delete).ToList();
            delete.Clear();

            Refresh();
        }
    }
}
