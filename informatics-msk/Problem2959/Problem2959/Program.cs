﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem2959
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());

            if (a > 0)
            {
                Console.WriteLine("1");
            }
            else if (a < 0)
            {
                Console.WriteLine("-1");
            }
            else
            {
                Console.WriteLine("0");
            }
        }
    }
}
