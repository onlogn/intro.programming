﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1002
{
    class Program
    {
        static void Main(string[] args)
        {
            double r = double.Parse(Console.ReadLine());

            double pi = 3.14159;
            double res = pi * r * r;
            Console.WriteLine($"A={res.ToString("F4")}");
        }
    }
}
