﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1206
{
    class Point
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point(char x, char y)
        {
           X = int.Parse((x - 'a' + 1).ToString());
           Y = int.Parse(y.ToString());
        }

        public Point(Point a):this(a.X,a.Y)
        {

        }

        public bool IsInRange()
        {
            return !(X < 1 || X > 8 || Y < 1 || Y > 8);
        }

        public void MovePoint(Point direction)
        {
            X += direction.X;
            Y += direction.Y;
        }

        private bool Equals(Point a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.X, X) && Equals(a.Y, Y);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Point)) return false;
            return Equals((Point)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = X.GetHashCode();
            res = res * num + Y.GetHashCode();
           return res;
        }

        //public override string ToString()
        //{
        //    return $"{X} {Y}";
        //}
    }
}
