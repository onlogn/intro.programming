﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem224
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();
            string str = Console.ReadLine();
            string a = Console.ReadLine();

            string[] strArray = str.Split(' ');

            var res = strArray.Any(s => s == a);

            Console.WriteLine(res?"YES":"NO");
        }
    }
}
