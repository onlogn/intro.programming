﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0605
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            if (n < 1)
            {
                throw new Exception("Incorrect number of tests");
            }

            Random rnd = new Random();
            int cntRight = 0;
            int a, b;
            for (int i = 0; i < n; i++)
            {
                a = rnd.Next(-100, 101);
                b = rnd.Next(-100, 101);

                Console.Write($"{a} + {b} = ");
                int.TryParse(Console.ReadLine(),out int userRes);
                if (a + b == userRes) cntRight++;

            }
            Console.WriteLine($"Right = {cntRight}\nWrong = {n - cntRight}");
        }
    }
}
