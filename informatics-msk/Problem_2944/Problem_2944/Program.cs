﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_2944
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());
            int tmp = a;
            int res = 0;

            for (int i = 0; i < 3; i++)
            {
                res += tmp % 10;
                tmp = tmp / 10;

            }
            
            Console.WriteLine(res);
        }
    }
}
