﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1303
{
    class Program
    {
        static void Main(string[] args)
        {
            long cntLetter = 0, cntSpace = 0, cntDigits = 0, cntPunct = 0, cntControl = 0, cntOthers = 0;

            Console.WriteLine("Statistics");
            Console.WriteLine(new string('-', 40));
            int code;
            while ((code = Console.Read()) != -1)
            {
                if (Char.IsLetter((char)code)) cntLetter++;
                else if (Char.IsWhiteSpace((char)code)) cntSpace++;
                else if (Char.IsDigit((char)code)) cntDigits++;
                else if (Char.IsControl((char)code)) cntControl++;
                else if (Char.IsPunctuation((char)code)) cntPunct++;
                else cntOthers++;
            }

            Console.WriteLine($"Letters: {cntLetter}, Spaces: {cntSpace}, Digits: {cntDigits}," +
                              $"Controls: {cntControl}, Punct: {cntPunct}, Others: {cntOthers}");


        }
    }
}
