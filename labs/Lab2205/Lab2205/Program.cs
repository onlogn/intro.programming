﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auca;

namespace Lab2205
{
    class Program
    {
        static void Main(string[] args)
        {
            Rational r = new Rational(3,2);
            Console.WriteLine(r.Reciprocal());
            Console.WriteLine(r.Sqr());

        }
    }

    public static class HelperExtension{
        public static Rational Reciprocal(this Rational r)
        {
            return new Rational(r.Denomerator, r.Nomerator);
        }

        public static Rational Sqr(this Rational r)
        {
            return new Rational(r.Nomerator * r.Nomerator, r.Denomerator * r.Denomerator);
        }
    }
}
