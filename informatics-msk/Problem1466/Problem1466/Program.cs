﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1466
{
    class Program
    {
        static void Main(string[] args)
        {

            long n = long.Parse(Console.ReadLine());
            long res;
            if (n > 0)
            {
                if (n % 2 != 0)
                {
                    res = (1 + n) / 2 * n;
                }
                else
                {
                    res = n / 2 * (1 + n);
                }
            }
            else
            {
                if (n % 2 != 0)
                {
                    res = (1 + n) / 2 * (abs(n) + 2);
                }
                else
                {
                    res = (abs(n) + 2) / 2 * (1 + n);
                }
            }
            Console.WriteLine(res);
        }

        static long abs(long x)
        {
            return (x < 0) ? -x : x;
        }
    }
}
