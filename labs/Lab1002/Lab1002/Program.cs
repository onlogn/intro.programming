﻿using System;
using System.Text;

namespace Lab1002
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            Random rnd = new Random();
            const int cntNumber = 11;

            int[] counters = new int[cntNumber];

            for (int i = 0; i < n; i++)
            {
                int dice1 = rnd.Next(1, 7);
                int dice2 = rnd.Next(1, 7);

                counters[dice1 + dice2 - 2]++;
            }

            Console.WriteLine(counters.ArrToResult(n));
        }
    }

    public static class StringExtension
    {
        public static string ArrToStr(this int[] arr)
        {
            return String.Join(" ",arr);
        }

        public static string ArrToResult(this int[] arr, int n)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 11; i++)
            {
                sb.Append(i + 2);
                sb.Append(": ");
                sb.Append($"{(double)arr[i] / n * 100:f2}");
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
