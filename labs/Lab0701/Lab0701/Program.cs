﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0701
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            int cnt = 0;

            do
            {
                number /= 10;
                cnt++;
            } while (number != 0);

            Console.WriteLine(cnt);
        }
    }
}
