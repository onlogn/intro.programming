﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library.DataBaseWork;

namespace Library
{
    public partial class UserRedactionDialog : Form
    {
        public User CurrentUser { get; private set; }
        public UserRedactionDialog()
        {
            InitializeComponent();
        }

        public UserRedactionDialog(User u):this()
        {
            CurrentUser = new User(u.Id, u.Name, u.Categories, u.MessageType);
            txtUserName.Text = CurrentUser.Name;
        }

        private async void UserRedactionForm_Load(object sender, EventArgs e)
        {
            comboBoxPrefType.DataSource = Enum.GetValues(typeof(PreferredMessageType));
            SetComboboxValue();

            List<Category> categ = await CategoryDataBaseService.GetCategoryList();
            checkedListBoxCat.DataSource = categ;
            checkedListBoxCat.DisplayMember = nameof(Category.Name);
            SetCheckboxItems();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string name = txtUserName.Text.Trim();
            if (string.IsNullOrEmpty(name) || comboBoxPrefType.SelectedItem == null)
            {
                MessageBox.Show("Input name, Select Preffered Type...");
                return;
            }
            int idx = CurrentUser?.Id ?? -1;
            List<Category> checkedList = checkedListBoxCat.CheckedItems.Cast<Category>().ToList();
            CurrentUser = new User(idx,name, checkedList, (PreferredMessageType)comboBoxPrefType.SelectedItem);
            
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void SetComboboxValue()
        {
            if (CurrentUser != null)
            {
                comboBoxPrefType.SelectedItem = CurrentUser.MessageType;
            }
        }

        private void SetCheckboxItems()
        {
            if (CurrentUser != null)
            {
                List<Category> list = CurrentUser.Categories;
                for (int i = 0; i < checkedListBoxCat.Items.Count; i++)
                {
                    Category x = (Category) checkedListBoxCat.Items[i];
                    if (list.Contains(x))
                    {
                        checkedListBoxCat.SetItemChecked(i, true);
                    }
                } 
            }
        }
    }
}
