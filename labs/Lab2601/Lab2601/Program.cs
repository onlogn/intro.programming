﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab2601
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>()
            {
                new Student() {Id = 1, Name = "Alesha Popovich", Gpa = 2.5, Year = 2000},
                new Student() {Id = 2, Name = "Zmei Gorinich", Gpa = 3, Year = 1998},
                new Student() {Id = 3, Name = "Dobrynya Nikitich", Gpa = 2.2, Year = 2001},
                new Student() {Id = 4, Name = "Ilya Muromets", Gpa = 2.2, Year = 2001},
                new Student() {Id = 5, Name = "Uli", Gpa = 5, Year = 2000},
                new Student() {Id = 6, Name = "Lubava", Gpa = 2.1, Year = 2001},
                new Student() {Id = 7, Name = "Tihon", Gpa = 4.0, Year = 1990}
            };

            //students.ForEach(s => Console.WriteLine($"{s.Id}, {s.Name}, {s.Gpa}"));

            //students
            //    .Select(s => new {s.Id, s.Gpa})
            //    .ForEach(si => Console.WriteLine($"{si.Id}, {si.Gpa}"));

            //Action<int> d = x => Console.WriteLine(x % 2 == 0 ? "Even" : "Odd");
            //d(5);

            Console.WriteLine($"AVG GPA = {students.Average(s => s.Gpa):F2}");

            Console.WriteLine($"Students 2001 = {students.Count(s => s.Year == 2001)}");

            Console.WriteLine($"Max GPA = {students.Max(s => s.Gpa)}");

            double max = students.Max(s => s.Gpa);
            Console.WriteLine($"Best Student = {students.FirstOrDefault(s => s.Gpa == max)}");

            var res = students.Aggregate("all names: ", (s, n) => s + " " + n.Name);
            Console.WriteLine(res);

            var avgStudents = students.Where(s => s.Gpa > 2 && s.Gpa < 3);
            avgStudents.ForEach(e => Console.WriteLine(e));

            Console.WriteLine("Ends With A");
            var studentsEndsW = students.Where(s => s.Name.EndsWith("h"));
            studentsEndsW.ForEach(e => Console.WriteLine(e));

            Console.WriteLine("Sorted Students");
            var ordered = students.OrderBy(s => s.Year).ThenByDescending(s => s.Gpa);
            ordered.ForEach(e => Console.WriteLine(e));

            Console.WriteLine("Group");
            var group = students.GroupBy(s => s.Year).Select(g => new {Year = g.Key, Count = g.Count()});
            group.ForEach(e => Console.WriteLine(e));

            Console.WriteLine("Group 2");
            var group2 = students.GroupBy(s => s.Year).Select(g => new { Year = g.Key, Count = g.Count(), Stud = g.Select(s => s) });
            foreach (var v in group2)
            {
                Console.WriteLine($"{v.Year}: {v.Count}");
                foreach (Student s in v.Stud)
                {
                    Console.WriteLine(s);
                }
            }

            Console.WriteLine("Any");

            var any = students.Any(s => Math.Abs(s.Gpa - 4.0) < 0.00001);
            Console.WriteLine(any);

            //var all = students.All(s => s.Gpa > 3);
            //Console.WriteLine(all);

            var first = students.FirstOrDefault(s => s.Name == "kk");
            Console.WriteLine(first?.Name?? "lol");
            Console.WriteLine($"sfsdf {first?.Name ?? "lol"}");
        }

        
    }

    public static class Utils
    {
        public static void ForEach<T>(this IEnumerable<T> list, Action<T> act)
        {
            foreach (T e in list)
            {
                act(e);
            }
        }
    }

    class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Gpa { get; set; }
        public int Year { get; set; }

        public override string ToString()
        {
            return $"{Id}. {Name} - {Gpa} / {Year}";
        }
    }
}
