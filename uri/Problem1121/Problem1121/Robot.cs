﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1121
{
    static class Robot
    {
        private static Dictionary<char, Point> _directions = new Dictionary<char, Point>(4)
        {
            {'N', new Point(0, -1)},
            {'S', new Point(0, 1)},
            {'L', new Point(1, 0)},
            {'O', new Point(-1, 0)}
        };

        private static Dictionary<string, char> _turns = new Dictionary<string, char>()
        {
            {"ND", 'L'},
            {"NE", 'O'},
            {"SD", 'O'},
            {"SE", 'L'},
            {"LD", 'S'},
            {"LE", 'N'},
            {"OD", 'N'},
            {"OE", 'S'}
        };

        public static Point GetDirection(char direction)
        {
            return _directions[direction];
        }

        public static char GetNewDirection(char previous_direction, char current_direction)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(previous_direction);
            sb.Append(current_direction);

            return _turns[sb.ToString()];
        }

        public static List<Point> GetMovesList(string actions, char direction)
        {
            List<Point> res = new List<Point>();

            char currentDirection = direction;
            for (int i = 0; i < actions.Length; i++)
            {
                if (actions[i] == 'F')
                {
                    res.Add(GetDirection(currentDirection));
                }
                else if (actions[i] == 'D' || actions[i] == 'E')
                {
                    currentDirection = GetNewDirection(currentDirection, actions[i]);
                }
            }

            return res;
        }

        public static Point FindRobot(List<string> arr, out char directionChar)
        {
            for (int i = 0; i < arr.Count; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    if (arr[i][j] == 'N' || arr[i][j] == 'S' || arr[i][j] == 'L' || arr[i][j] == 'O')
                    {
                        directionChar = arr[i][j];
                        return new Point(j, i);
                    }
                }
            }

            // это по идее никогда не произойдет
            directionChar = '0';
            return new Point(-1,-1);
        }

    }
}
