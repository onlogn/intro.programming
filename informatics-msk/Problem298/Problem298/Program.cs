﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem298
{
    class Program
    {
        static void Main(string[] args)
        {
            int x1 = int.Parse(Console.ReadLine());
            int y1 = int.Parse(Console.ReadLine());
            int x2 = int.Parse(Console.ReadLine());
            int y2 = int.Parse(Console.ReadLine());

            Console.WriteLine(
                (Math.Abs(x2 - x1) == 1 && Math.Abs(y2 - y1) == 1) || (Math.Abs(x2 - x1) == 1 && (y2 == y1)) ||
                (Math.Abs(y2 - y1) == 1 && (x2 == x1))
                    ? "YES":"NO");
        }
    }
}
