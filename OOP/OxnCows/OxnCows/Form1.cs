﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OxnCows
{
    public partial class Form1 : Form
    {
        OxnCowsGame game;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtGameRules.Text = "Правила игры\r\nКомпьютер задумывает четыре различные цифры из 0,1,2,...9." +
                                "Игрок делает ходы, чтобы узнать эти цифры и их порядок. Каждый ход состоит из четырёх цифр, " +
                                "0 может стоять на первом месте. В ответ компьютер показывает число отгаданных цифр, " +
                                "стоящих на своих местах(число быков) и число отгаданных цифр, стоящих не на своих " +
                                "местах(число коров).\r\nПример\r\nКомпьютер задумал 0834. Игрок сделал ход 8134. Компьютер " +
                                "ответил:\r\n 2 ox(-es) быка(цифры 3 и 4) и 1 cow(-s) корова(цифра 8).\r\nКомпьютер уже что - то задумал." +
                                "\r\nИграем! Узнай, что задумано компьютером!";

            txtUserNumber.Visible = btnSendNumber.Visible = false;
            txtUserNumber.Enabled = btnSendNumber.Enabled = false;

        }

        private void btnStartNewGame_Click(object sender, EventArgs e)
        {
            game = new OxnCowsGame();
            //lblResults.Text += game.GetNumber();
            txtUserNumber.Visible = btnSendNumber.Visible = true;
            txtUserNumber.Enabled = btnSendNumber.Enabled = true;
            btnStartNewGame.Visible = false;
            btnStartNewGame.Enabled = false;
        }

        private void btnSendNumber_Click(object sender, EventArgs e)
        {
            string userAnswer = txtUserNumber.Text;
            if (!game.IsCorrectData(userAnswer))
            {
                MessageBox.Show(this,"Input correct data! You must input one number with 4 different digits!");
            }
            else
            {
                List<int> userNumber = userAnswer.Select(i => int.Parse(i.ToString())).ToList();
                Tuple<int, int> res = game.CheckNumber(userNumber);
                if (res.Item1 == 4)
                {
                    MessageBox.Show(this, $"You did it! Your number of tries is {game.NumberOfTries}!");
                    AddText($"Nice. Your number of tries is {game.NumberOfTries}!");
                    btnStartNewGame.Visible = true;
                    btnStartNewGame.Enabled = true;
                    txtUserNumber.Visible = btnSendNumber.Visible = false;
                    txtUserNumber.Enabled = btnSendNumber.Enabled = false;
                }
                else
                {
                    AddText($"{userAnswer}: {res.Item1} ox(-es), {res.Item2} cow(-s)!");
                }
            }

            txtUserNumber.Focus();
            txtUserNumber.Clear();
        }

        private void AddText(string txt)
        {
            txtUserRes.AppendText(txt + Environment.NewLine);
        }
    }
}
