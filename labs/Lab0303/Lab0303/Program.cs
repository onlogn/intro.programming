﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0303
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Logical AND:");
            Console.WriteLine($"{false} && {false} = {false && false}");
            Console.WriteLine($"{false} && {true} = {false && true}");
            Console.WriteLine($"{true} && {false} = {true && false}");
            Console.WriteLine($"{true} && {true} = {true && true}");

            Console.WriteLine("Logical OR:");
            Console.WriteLine($"{false} || {false} = {false || false}");
            Console.WriteLine($"{false} || {true} = {true || true}");
            Console.WriteLine($"{true} || {false} = {true || false}");
            Console.WriteLine($"{true} || {true} = {true || true}");
        }
    }
}
