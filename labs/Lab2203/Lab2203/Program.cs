﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2203
{
    class Program
    {
        private enum ArrayOrder
        {
            Increasing,
            Decreasing,
            Random
        }

        static void Main(string[] args)
        {
            const int N = 1000000;
            TestSort("Incr", GenerateArray(N, ArrayOrder.Increasing));
            TestSort("Incr", GenerateArray(N, ArrayOrder.Decreasing));
            TestSort("Incr", GenerateArray(N, ArrayOrder.Random));
        }

        private static int[] GenerateArray(int n, ArrayOrder r)
        {
            Random _rnd = new Random();
            int[] res = new int[n];
            for (int i = 0; i < res.Length; i++)
            {
                switch (r)
                {
                    case ArrayOrder.Increasing:
                        res[i] = i;
                        break;
                    case ArrayOrder.Decreasing:
                        res[i] = n - i;
                        break;
                    case ArrayOrder.Random:
                        res[i] = _rnd.Next(1,n);
                        break;

                }

            }

            return res;
        }


        private static void TestSort(string incr, int[] ints)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Array.Sort(ints);
            sw.Stop();
            
            Console.WriteLine($"{incr} - {sw.ElapsedMilliseconds}");
        }
    }
}
