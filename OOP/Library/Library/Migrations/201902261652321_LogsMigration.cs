namespace Library.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LogsMigration : DbMigration
    {
        public override void Up()
        {
           
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        BookName = c.String(),
                        Date = c.DateTime(nullable: false),
                        TypeOfMessage = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            
        }
        
        public override void Down()
        {
           
            DropTable("dbo.Logs");
           
        }
    }
}
