﻿namespace Library
{
    partial class UserRedactionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUsername = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblPrefType = new System.Windows.Forms.Label();
            this.comboBoxPrefType = new System.Windows.Forms.ComboBox();
            this.groupBoxUserCat = new System.Windows.Forms.GroupBox();
            this.checkedListBoxCat = new System.Windows.Forms.CheckedListBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBoxUserCat.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(12, 18);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(60, 13);
            this.lblUsername.TabIndex = 0;
            this.lblUsername.Text = "UserName:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(78, 16);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(238, 20);
            this.txtUserName.TabIndex = 1;
            // 
            // lblPrefType
            // 
            this.lblPrefType.AutoSize = true;
            this.lblPrefType.Location = new System.Drawing.Point(12, 55);
            this.lblPrefType.Name = "lblPrefType";
            this.lblPrefType.Size = new System.Drawing.Size(53, 13);
            this.lblPrefType.TabIndex = 0;
            this.lblPrefType.Text = "PrefType:";
            // 
            // comboBoxPrefType
            // 
            this.comboBoxPrefType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPrefType.FormattingEnabled = true;
            this.comboBoxPrefType.Location = new System.Drawing.Point(78, 52);
            this.comboBoxPrefType.Name = "comboBoxPrefType";
            this.comboBoxPrefType.Size = new System.Drawing.Size(238, 21);
            this.comboBoxPrefType.TabIndex = 2;
            // 
            // groupBoxUserCat
            // 
            this.groupBoxUserCat.Controls.Add(this.checkedListBoxCat);
            this.groupBoxUserCat.Location = new System.Drawing.Point(12, 93);
            this.groupBoxUserCat.Name = "groupBoxUserCat";
            this.groupBoxUserCat.Size = new System.Drawing.Size(304, 309);
            this.groupBoxUserCat.TabIndex = 3;
            this.groupBoxUserCat.TabStop = false;
            this.groupBoxUserCat.Text = "Категории";
            // 
            // checkedListBoxCat
            // 
            this.checkedListBoxCat.CheckOnClick = true;
            this.checkedListBoxCat.FormattingEnabled = true;
            this.checkedListBoxCat.Location = new System.Drawing.Point(6, 19);
            this.checkedListBoxCat.Name = "checkedListBoxCat";
            this.checkedListBoxCat.Size = new System.Drawing.Size(292, 274);
            this.checkedListBoxCat.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(18, 408);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(142, 30);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(174, 408);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(142, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // UserRedactionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 450);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBoxUserCat);
            this.Controls.Add(this.comboBoxPrefType);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblPrefType);
            this.Controls.Add(this.lblUsername);
            this.Name = "UserRedactionDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UserRedactionForm";
            this.Load += new System.EventHandler(this.UserRedactionForm_Load);
            this.groupBoxUserCat.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblPrefType;
        private System.Windows.Forms.ComboBox comboBoxPrefType;
        private System.Windows.Forms.GroupBox groupBoxUserCat;
        private System.Windows.Forms.CheckedListBox checkedListBoxCat;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}