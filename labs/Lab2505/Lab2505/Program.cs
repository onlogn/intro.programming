﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2505
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string,int> dict = new Dictionary<string, int>();

            string file = "file.txt";
            HashSet<string> articles = new HashSet<string>()
            {
                "at", "the", "a", "in", "of", "and", "to", "be", "are"
            };

            using (StreamReader sr = new StreamReader(file))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] words = line.ToLower().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string word in words)
                    {
                        if (articles.Contains(word)) continue;
                        dict[word] = dict.TryGetValue(word, out int v) ? v + 1 : 1;
                    }
                }
            }

            foreach (KeyValuePair<string, int> keyValuePair in dict.OrderByDescending(t => t.Value))
            {
                Console.WriteLine($"{keyValuePair.Key} => {keyValuePair.Value}");
            }
        }
    }
}
