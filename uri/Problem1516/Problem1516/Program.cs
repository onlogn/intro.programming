﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1516
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            while((str = Console.ReadLine())!="0 0")
            {
                string[] tmpstr = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                int n = int.Parse(tmpstr[0]);
                int m = int.Parse(tmpstr[1]);

                string[] lines = new string[n];
                for (int i = 0; i < n; i++)
                {
                    lines[i] = Console.ReadLine();
                }

                string resize = Console.ReadLine();
                string[] newVals = resize.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                int a = int.Parse(newVals[0]);
                int b = int.Parse(newVals[1]);

                int repeatColumns = b / m;
                int repeatRows = a / n;

                for (int k = 0; k < m; k++)
                {
                    StringBuilder res = new StringBuilder();
                    for (int j = 0; j < lines[k].Length; j++)
                    {
                        for (int i = 0; i < repeatColumns; i++)
                        {
                            res.Append(lines[k][j]);
                        }
                    }

                    for (int r = 0; r < repeatRows; r++)
                    {
                        Console.WriteLine(res);
                    }
                }
                Console.WriteLine();

                
            }
        }
    }
}
