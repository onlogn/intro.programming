﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1601
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Run();
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect Format");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Divide on Zero");
            }
            catch (OverflowException)
            {
                Console.WriteLine("Overflow");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void Run()
        {
            int a = int.Parse(Console.ReadLine());
            string operation = Console.ReadLine();
            int b = int.Parse(Console.ReadLine());

            switch (operation)
            {
                case "+":
                    Console.WriteLine($"{a} {operation} {b} = {checked(a + b)}");
                    break;
                case "-":
                    Console.WriteLine($"{a} {operation} {b} = {checked(a - b)}");
                    break;
                case "*":
                    Console.WriteLine($"{a} {operation} {b} = {checked(a * b)}");
                    break;
                case "/":
                    Console.WriteLine($"{a} {operation} {b} = {a / b}");
                    break;
                case "%":
                    Console.WriteLine($"{a} {operation} {b} = {a % b}");
                    break;
                default:
                    throw new Exception("Incorrect Operation");
            }
        }
    }

}
