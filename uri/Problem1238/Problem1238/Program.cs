﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1238
{
    class Program
    {
        static void Main(string[] args)
        {

            int n = int.Parse(Console.ReadLine());
            for (int k = 0; k < n; k++)
            {
                string str = Console.ReadLine();

                string[] words = str.Split(new char[] { ' ' });
                int m = Math.Min(words[0].Length, words[1].Length);

                for (int i = 0; i < m; i++)
                {
                    Console.Write(words[0][i]);
                    Console.Write(words[1][i]);
                }

                if (words[0].Length > words[1].Length)
                {
                    Console.Write(words[0].Substring(m));
                }
                else
                {
                    Console.Write(words[1].Substring(m));
                }

                Console.WriteLine();
            }
            
        }
    }
}
