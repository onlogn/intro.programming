﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1271
{
    class Program
    {
        static void Main(string[] args)
        {
            int genomCounter = 1;
            string str;

            while ((str = Console.ReadLine()) != "0")
            {
                Console.WriteLine($"Genome {genomCounter}");
                int n = int.Parse(str);
                List<int> arr = Enumerable.Range(1, n).ToList();
                int r = int.Parse(Console.ReadLine());

                for (int i = 0; i < r; i++)
                {
                    int[] idx = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse).ToArray();
                   arr.Reverse(idx[0] - 1, idx[1] - idx[0] + 1);
                }

                int q = int.Parse(Console.ReadLine());
                for (int i = 0; i < q; i++)
                {
                    Console.WriteLine(arr.IndexOf(int.Parse(Console.ReadLine())) + 1);
                }

                genomCounter++;
            }

        }
    }
}
