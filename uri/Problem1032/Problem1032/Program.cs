﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1032
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;

            while ((n = int.Parse(Console.ReadLine())) != 0)
            {
                List<int> prime = PrimeNumbers(40000);

                Console.WriteLine(LastNum(n, prime)+1);

                //Queue<int> q = new Queue<int>();
                //for (int i = 1; i <= n; i++)
                //{
                //    q.Enqueue(i);
                //}

                //int res = 0;
                //foreach (var p in prime)
                //{
                //    if (q.Count == 0) break;

                //    for (int i = 0; i < p - 1; i++)
                //    {
                //        q.Enqueue(q.Dequeue());
                //    }

                //    res = q.Dequeue();
                //}

                //Console.WriteLine(res);
            }
        }

        public static List<int> PrimeNumbers(int n)
        {
            List<int> res = new List<int>();

            bool[] boolArr = Enumerable.Repeat(true, n+1).ToArray();
            boolArr[0] = boolArr[1] = false;

            for (int i = 2; i <= n; i++)
            {
                if (boolArr[i])
                {
                    for (int j = i * 2; j <= n; j+=i)
                    {
                        boolArr[j] = false;
                    }
                }
            }

            for (int i = 0; i < n; i++)
            {
                if(boolArr[i])
                    res.Add(i);
            }
            return res;
        }

        public static int LastNum(int n, List<int> prime)
        {
            int res = 0;
            for (int i = 1; i <= n; i++)
            {
                res = (res + prime[n - i]) % i;
            }

            return res;
        }
    }
}
