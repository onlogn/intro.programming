﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0404
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input your's points (from 0 to 100): ");

            int a = int.Parse(Console.ReadLine());

            if (a < 0 || a > 100)
            {
                Console.WriteLine($"{a} = incorrect number of points");
            }
            else if (a < 20)
            {
                Console.WriteLine($"{a} = F");
            }
            else if (a < 40)
            {
                Console.WriteLine($"{a} = D");
            }
            else if (a < 60)
            {
                Console.WriteLine($"{a} = C");
            }
            else if (a < 80)
            {
                Console.WriteLine($"{a} = B");
            }
            else if (a <= 100)
            {
                Console.WriteLine($"{a} = A");
            }
        }
    }
}
