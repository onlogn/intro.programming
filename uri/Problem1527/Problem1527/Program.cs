﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1527
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            while ((s = Console.ReadLine()) != "0 0")
            {
                int[] str = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
                int nUsers = str[0];
                int nActions = str[1];

                Guild[] d = new Guild[nUsers];

                int[] sLvls = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse).ToArray();

                for (int i = 0; i < sLvls.Length; i++)
                {
                    d[i] = new Guild(i + 1,sLvls[i]);
                }

                const int idRaphael = 1;
                int cntWin = 0;

                for (int i = 0; i < nActions; i++)
                {
                    int[] act = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse).ToArray();
                    int actionType = act[0];
                    int uFirst = act[1] - 1;
                    int uSecond = act[2] - 1;

                    if (actionType == 1)
                    {
                        Guild tmp = d[uFirst].AddGuild(d[uSecond]);
                        List<int> usersList = tmp.Users;  
                        for (int j = 0; j < usersList.Count; j++)  
                        {
                            d[usersList[j] - 1] = tmp;
                        }
                    } else if (actionType == 2)
                    {
                        if ((d[uFirst].IsRalph && d[uFirst].GuildPower > d[uSecond].GuildPower) 
                            || (d[uSecond].IsRalph && d[uSecond].GuildPower > d[uFirst].GuildPower))
                        {
                            cntWin++;
                        }
                    }
                }

                Console.WriteLine(cntWin);
            }
        }
    }
}
