﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2507
{
    public class GameCell
    {
        public int Row { get; set; }
        public int Col { get; set; }

        public override string ToString()
        {
            return $"({Row}; {Col})";
        }

        public override bool Equals(object obj)
        {
            var cell = obj as GameCell;
            return cell != null &&
                   Row == cell.Row &&
                   Col == cell.Col;
        }

        public override int GetHashCode()
        {
            var hashCode = 240067226;
            hashCode = hashCode * -1521134295 + Row.GetHashCode();
            hashCode = hashCode * -1521134295 + Col.GetHashCode();
            return hashCode;
        }
    }
}
