﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab2001;

namespace Lab2103
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] s = new Student[]
            {
                new Student("Z", 1990, 4.5), 
                new Student("B", 1997, 4.0), 
                new Student("B", 1995, 4.1), 
            };

            Array.Sort(s, (s1,s2)=>s1.Name.CompareTo(s2.Name));

            Console.WriteLine(s.ArrToStr());
        }
    }

    public static class StringExtension
    {
        public static string ArrToStr<T>(this T[] arr)
        {
            return String.Join("\n", arr);
        }
    }
}
