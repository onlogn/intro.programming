﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Auca
{
    public class Rational
    {
        public BigInteger Nomerator { get; private set; }
        public BigInteger Denomerator { get; private set; }

        public Rational(BigInteger aNum, BigInteger aDen)
        {
            if (aDen == 0) throw new ArgumentException("Знаменатель не должен равняться 0");
            if (aDen < 0)
            {
                aNum = -aNum;
                aDen = Abs(aDen);
            }

            BigInteger gcd = Gcd(Abs(aNum), Abs(aDen));
            Nomerator = aNum / gcd;
            Denomerator = aDen / gcd;
        }


        public Rational(int a)
        {
            Nomerator = a;
            Denomerator = 1;
        }

        public Rational(Rational a) : this(a.Nomerator, a.Denomerator)
        {

        }

        private BigInteger Abs(BigInteger a)
        {
            return a >= 0 ? a : -a;
        }

        public Rational Sum(Rational a)
        {
            return new Rational(Nomerator * a.Denomerator + Denomerator * a.Nomerator, Denomerator * a.Denomerator);
        }

        public Rational Sub(Rational a)
        {
            return new Rational(Nomerator * a.Denomerator - Denomerator * a.Nomerator, Denomerator * a.Denomerator);
        }

        public Rational Mult(Rational a)
        {
            return new Rational(Nomerator * a.Nomerator, Denomerator * a.Denomerator);
        }

        public Rational Div(Rational a)
        {
            return new Rational(Nomerator * a.Denomerator, Denomerator * a.Nomerator);
        }

        public static Rational operator +(Rational a, Rational b)
        {
            return a.Sum(b);
        }

        public static Rational operator -(Rational a, Rational b)
        {
            return a.Sub(b);
        }

        public static Rational operator *(Rational a, Rational b)
        {
            return a.Mult(b);
        }

        public static Rational operator /(Rational a, Rational b)
        {
            return a.Div(b);
        }

        private BigInteger Gcd(BigInteger a, BigInteger b)
        {
            while (b > 0)
            {
                a %= b;
                BigInteger tmp = a;
                a = b;
                b = tmp;
            }

            return a;
        }

        public static Rational Parse(string expr)
        {
            List<int> res = expr.Split(new[] { '/', ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse)
                .ToList();
            switch (res.Count)
            {
                case 1:
                    return new Rational(res[0], 1);
                case 2:
                    return new Rational(res[0], res[1]);
                default:
                    throw new Exception("Incorrect Fraction");
            }
        }

        public override string ToString()
        {
            return $"{Nomerator}/{Denomerator}";
        }

        private bool Equals(Rational a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Nomerator, Nomerator) && Equals(a.Denomerator, Denomerator);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Rational)) return false;
            return Equals((Rational)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Nomerator.GetHashCode();
            res = res * num + Denomerator.GetHashCode();
            return res;
        }
    }
}
