﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_1168
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            for (int j = 0; j < n; j++)
            {
                string str = Console.ReadLine();
                int sum = 0;
                foreach (char c in str)
                {
                    sum += Leds(c);
                }
                Console.WriteLine($"{sum} leds");
            }
        }
        static int Leds(char ch)
        {
            switch (ch)
            {
                case '1':
                    return 2;
                case '2':
                case '3':
                    return 5;
                case '4':
                    return 4;
                case '5':
                    return 5;
                case '6':
                    return 6;
                case '7':
                    return 3;
                case '8':
                    return 7;
                case '9':
                    return 6;
                case '0':
                    return 6;
                default:
                    return 0;

            }
        }
    }
}
