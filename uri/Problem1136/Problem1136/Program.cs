﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1136
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;

            while ((str = Console.ReadLine()) != "0 0")
            {

                int[] ns = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse).ToArray();
                int[] arr = Console.ReadLine().Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse).ToArray();

                HashSet<int> res = new HashSet<int>();
                int tmp;
                for (int i = 0; i < arr.Length; i++)
                {
                    for (int j = i; j < arr.Length; j++)
                    {
                        tmp = Math.Abs(arr[j] - arr[i]);
                        if (!res.Contains(tmp))
                            res.Add(tmp);
                    }
                }

                int[] all = Enumerable.Range(1, ns[0]).ToArray();

                if (all.Except(res).Count() == 0)
                {
                    Console.WriteLine("Y");
                }
                else
                {
                    Console.WriteLine("N");
                }
            }
        }
    }
}
