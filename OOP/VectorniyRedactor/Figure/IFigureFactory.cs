﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Figures;
using Point = Figures.Point;

namespace Figures
{
    // СТРАДАЙТЕ ЮЗВЕРИ! ТАК ВАМ! ТАКИЕ КОНСТРУКТОРЫ ТОЛЬКО МОЖНО ЛЯПАТЬ!
    public interface IFigureFactory
    {
        string FigureName { get; }
        Type FigureType { get; }
        IFigure CreateFigure(Point p, int w, int h, Color c);
        IFigure CreateFigure(Point p, Color c);
        IFigure CreateFigure(FigureData f);
    }
}
