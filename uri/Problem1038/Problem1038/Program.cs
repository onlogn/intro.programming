﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1038
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            string[] s = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            Dictionary<int,decimal> d = new Dictionary<int, decimal>()
            {
                {1, 4}, {2, 4.5m}, {3, 5}, {4, 2}, {5, 1.5m}
            };

            Console.WriteLine($"Total: R$ {d[int.Parse(s[0])] * int.Parse(s[1]):f2}");
        }
    }
}
