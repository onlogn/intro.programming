﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem115
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine(NumOfZeroes(n));
        }

        private static int NumOfZeroes(int n)
        {
            int res = 0;

            while (n != 0)
            {
                if (n % 10 == 0)
                {
                    res++;
                }

                n /= 10;
            }

            return res;
        }
    }
}
