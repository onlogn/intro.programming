﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1702
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lst = new List<int>();

            string s;

            while ((s = Console.ReadLine()) != null)
            {
                lst.Add(int.Parse(s));
                Console.Write($"Len = {lst.Count}, Capacity = {lst.Capacity}\n");
            }

            lst.Reverse();

            Console.WriteLine(String.Join(" ", lst));
        }
    }
}
