﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1030
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTests = int.Parse(Console.ReadLine());
            for (int nt = 1; nt <= numOfTests; nt++)
            {
                string str = Console.ReadLine();
                string[] s = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                int n = int.Parse(s[0]);
                int k = int.Parse(s[1]);


                Queue<int> q = new Queue<int>();

                for (int i = 1; i <= n; i++)
                {
                    q.Enqueue(i);
                }


                int cnt = 0;
                while (q.Count > 1)
                {
                    if (cnt == k - 1)
                    {
                        q.Dequeue();
                        cnt = 0;
                    }
                    else
                    {
                        q.Enqueue(q.Dequeue());
                        cnt++;
                    }
                }

                Console.WriteLine($"Case {nt}: {q.Peek()}");
            }
        }
    }
}
