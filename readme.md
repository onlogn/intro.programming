#Практика по программированию

##Задачки Informatics-Msk

####Ввод-вывод, оператор присваивания, арифметические операции

[Задача №2936. Гипотенуза](https://informatics.msk.ru/mod/statements/view.php?id=2296) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2936/Problem_2936/Program.cs)

[Задача №2937. Следующее и предыдущее](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2937) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2937/Problem_2937/Program.cs)

[Задача №2938. Дележ яблок - 1](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2938) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2938/Problem_2938/Program.cs)

[Задача №2939. Дележ яблок - 2](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2939) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2939/Problem_2939/Program.cs)

[Задача №2940. МКАД](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2940) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2940/Problem_2940/Program.cs)

[Задача №2941. Последняя цифра](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2941) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2941/Problem_2941/Program.cs)

[Задача №2942. Число десятков двузначного числа](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2942) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2942/Problem_2942/Program.cs)

[Задача №2943. Число десятков](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2943) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2943/Problem_2943/Program.cs)

[Задача №2944. Сумма цифр](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2944) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2944/Problem_2944/Program.cs)

[Задача №2945. Следующее четное](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2945) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2945/Problem_2945/Program.cs)

[Задача №2947. Электронные часы - 1](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2947) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2947/Problem_2947/Program.cs)

[Задача №2948. Электронные часы - 2](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2948) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2948/Problem_2948/Program.cs)

[Задача №2949. Обмен значений](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2949) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2949/Problem_2949/Program.cs)

[Задача №2950. Конец уроков](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2950) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2950/Problem_2950/Program.cs)

[Задача №2951. Стоимость покупки](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2951) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2951/Problem_2951/Program.cs)

[Задача №2952. Разность времен](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2952) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2952/Problem_2952/Program.cs)

[Задача №2953. Автопробег](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2953) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem_2953/Problem_2953/Program.cs)

####Программирование (Е. В. Андреева)

[Задача №112145. Три числа](https://informatics.msk.ru/mod/statements/view.php?id=11098) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112145/Problem112145/Program.cs)

[Задача №1478. Урок в неделе](https://informatics.msk.ru/mod/statements/view3.php?id=1135&chapterid=1478)#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1478/Problem1478/Program.cs)

[Задача №1500.](https://informatics.msk.ru/mod/statements/view3.php?id=1140&chapterid=1500#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1500/Problem1500/Program.cs)

####Поляков-Еремин 

[Задача №112145. Три числа](https://informatics.msk.ru/mod/statements/view.php?id=11098#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112145/Problem112145/Program.cs)

[Задача №112146. Круг](https://informatics.msk.ru/mod/statements/view3.php?id=11098&chapterid=112146#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112146/Problem112146/Program.cs)

[Задача №112149. Расстояние](https://informatics.msk.ru/mod/statements/view3.php?id=11098&chapterid=112149#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112149/Problem112149/Program.cs)

[Задача №112150. Степень](https://informatics.msk.ru/mod/statements/view3.php?id=11098&chapterid=112150#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112150/Problem112150/Program.cs)

[Задача №112151. Случайные целые](https://informatics.msk.ru/mod/statements/view3.php?id=11098&chapterid=112151#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112151/Problem112151/Program.cs)

[Задача №112152. Случайные вещественные](https://informatics.msk.ru/mod/statements/view3.php?id=11098&chapterid=112152#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112152/Problem112152/Program.cs)

[Задача №112159. Трёхзначное число](https://informatics.msk.ru/mod/statements/view3.php?id=11144&chapterid=112159#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112159/Problem112159/Program.cs)

[Задача №112160. Равные числа](https://informatics.msk.ru/mod/statements/view3.php?id=11144&chapterid=112160#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem112160/Problem112160/Program.cs)

####Типы данных

[Задача №1466. Сумма от 1 до N](https://informatics.msk.ru/mod/statements/view3.php?id=1120&chapterid=1466) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1466/Problem1466/Program.cs) 

#### Условный оператор

[Задача №292. Максимум из двух чисел](https://informatics.msk.ru/mod/statements/view.php?id=276#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem292/Problem292/Program.cs)

[Задача №2956. Симметричное число](https://informatics.msk.ru/mod/statements/view3.php?id=2296&chapterid=2956#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem2956/Problem2956/Program.cs)

[Задача №2960. Тестирующая система](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=2960#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem2960/Problem2960/Program.cs)

[Задача №2959. Знак числа](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=2959#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem2959/Problem2959/Program.cs)

[Задача №1451. Четные и нечетные числа](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=1451#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1451/Problem1451/Program.cs)

[Задача №1448. Апельсины бочками](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=1448#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1448/Problem1448/Program.cs)

[Задача №296. Количество равных из трех](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=296#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem296/Problem296/Program.cs)

[Задача №1445. Координаты соседей](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=1445#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1445/Problem1445/Program.cs)

[Задача №253. Високосный год](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=253#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/labs/Lab0305/Lab0305/Program.cs)

[Задача №257. Конь](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=257#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem257/Problem257/Program.cs)

[Задача №254. Ладья](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=254#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem254/Problem254/Program.cs)

[Задача №255. Слон](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=255#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem255/Problem255/Program.cs)

[Задача №256. Ферзь](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=256#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem256/Problem256/Program.cs)

[Задача №298. Король](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=298#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem298/Problem298/Program.cs)

[Задача №2961. Упорядочить три числа](https://informatics.msk.ru/mod/statements/view3.php?id=276&chapterid=2961#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem2961/Problem2961/Program.cs)


#### Поиск и сортировка

[Задача №223. Линейный поиск - 1](https://informatics.msk.ru/mod/statements/view.php?id=270#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem223/Problem223/Program.cs)

[Задача №224. Линейный поиск - 2](https://informatics.msk.ru/mod/statements/view3.php?id=270&chapterid=224#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem224/Problem224/Program.cs)

[Задача №225. Ближайшее число](https://informatics.msk.ru/mod/statements/view3.php?id=270&chapterid=225#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem225/Problem225/Program.cs)

[Задача №227. Максимальный элемент массива](https://informatics.msk.ru/mod/statements/view3.php?id=270&chapterid=227#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem227/Problem227/Program.cs)

[Задача №1440. Серебряная медаль](https://informatics.msk.ru/mod/statements/view3.php?id=270&chapterid=1440#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1440/Problem1440/Program.cs)

[Задача №1409. Ревизия](https://informatics.msk.ru/mod/statements/view3.php?id=270&chapterid=1409#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1409/Problem1409/Program.cs)

[Задача №1447. Контроперация](https://informatics.msk.ru/mod/statements/view3.php?id=270&chapterid=1447#1) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problem1447/Problem1447/Program.cs)

#### Задачи на циклы

[Задача №3064. Длина последовательности](https://informatics.msk.ru/mod/statements/view.php?id=2585) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3064/Program.cs)

[Задача №3065. Сумма последовательности](https://informatics.msk.ru/mod/statements/view3.php?id=2585&chapterid=3065) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3065/Program.cs)

[Задача №3066. Среднее значение последовательности](https://informatics.msk.ru/mod/statements/view3.php?id=2585&chapterid=3066) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3066/Program.cs)

[Задача №3067. Количество четных элементов последовательности](https://informatics.msk.ru/mod/statements/view3.php?id=2585&chapterid=3067) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3067/Program.cs)

[Задача №3068. Максимум последовательности](https://informatics.msk.ru/mod/statements/view3.php?id=2585&chapterid=3068) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3068/Program.cs)

[Задача №3069. Количество элементов, которые больше предыдущего](https://informatics.msk.ru/mod/statements/view3.php?id=2585&chapterid=3069) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3069/Program.cs)

[Задача №3070. Второй максимум](https://informatics.msk.ru/mod/statements/view3.php?id=2585&chapterid=3070) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3070/Program.cs)

[Задача №114. Сумма цифр числа](https://informatics.msk.ru/mod/statements/view3.php?id=2587&chapterid=114) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problems_DigitsAnalysis/Program.cs)

[Задача №115. Количество нулей](https://informatics.msk.ru/mod/statements/view3.php?id=2587&chapterid=115) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem115/Program.cs)

[Задача №116. Минимальная и максимальная цифры](https://informatics.msk.ru/mod/statements/view3.php?id=2587&chapterid=116) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem116/Program.cs)

[Задача №117. Двоичная запись](https://informatics.msk.ru/mod/statements/view3.php?id=2587&chapterid=117) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem117/Program.cs)

[Задача №118. Обращение числа](https://informatics.msk.ru/mod/statements/view3.php?id=2587&chapterid=118) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem118/Program.cs)

[Задача №119. Количество палиндромов](https://informatics.msk.ru/mod/statements/view3.php?id=2587&chapterid=119) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem119/Program.cs)

[Задача №3062. Утренняя пробежка](https://informatics.msk.ru/mod/statements/view3.php?id=249&chapterid=3062) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3062/Program.cs)

[Задача №3063. Банковские проценты](https://informatics.msk.ru/mod/statements/view3.php?id=249&chapterid=3063) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem3063/Program.cs)

[Задача №343. Сумма чисел](https://informatics.msk.ru/mod/statements/view3.php?id=280&chapterid=343) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem343/Program.cs)

[Задача №333. Четные числа](https://informatics.msk.ru/mod/statements/view3.php?id=280&chapterid=333) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem333/Program.cs)

[Задача №346. Подсчет чисел](https://informatics.msk.ru/mod/statements/view3.php?id=280&chapterid=346) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem346/Program.cs)

[Задача №347. Ноль или не ноль](https://informatics.msk.ru/mod/statements/view3.php?id=280&chapterid=347) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem347/Program.cs)

[Задача №351. Факториал](https://informatics.msk.ru/mod/statements/view3.php?id=278&chapterid=351) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem351/Program.cs)

[Задача №1528. Оценки судей](https://informatics.msk.ru/mod/statements/view3.php?id=1143&chapterid=1528) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/informatics-msk/Problems_DigitsAnalysis/Problem1528/Program.cs)

## Задачки URI Online Judge

### Beginner

[1002		Area of a Circle](https://www.urionlinejudge.com.br/judge/en/problems/view/1002) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1002/Problem1002/Program.cs)

[1003		Simple Sum](https://www.urionlinejudge.com.br/judge/en/problems/view/1003) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1003/Problem1003/Program.cs)

[1052	Month](https://www.urionlinejudge.com.br/judge/en/problems/view/1052) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1052/Problem1052/Program.cs)

[1050	DDD](https://www.urionlinejudge.com.br/judge/en/problems/view/1050) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1050/Problem1050/Program.cs)

[1038	Snack](https://www.urionlinejudge.com.br/judge/en/problems/view/1038) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1038/Problem1038/Program.cs)

[1049	Animal](https://www.urionlinejudge.com.br/judge/en/problems/view/1049) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1049/Problem1049/Program.cs)

### Strings

[1024		Encryption](https://www.urionlinejudge.com.br/judge/en/problems/view/1024) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1024/Problem1024/Program.cs)

[1120		Contract Revision](https://www.urionlinejudge.com.br/judge/en/problems/view/1120) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1120/Problem1120/Program.cs)

[1168		LED](https://www.urionlinejudge.com.br/judge/en/problems/view/1168) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem_1168/Problem_1168/Program.cs)

[1234		Dancing Sentence](https://www.urionlinejudge.com.br/judge/en/problems/view/1234) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1234/Problem1234/Program.cs)

[1235		Inside Out](https://www.urionlinejudge.com.br/judge/en/problems/view/1235) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1235/Problem1235/Program.cs)

[1237		Compare Substring](https://www.urionlinejudge.com.br/judge/en/problems/view/1237) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1237/Problem1237/Program.cs)

[1238		Combiner](https://www.urionlinejudge.com.br/judge/en/problems/view/1238) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1238/Problem1238/Program.cs)

[1239		Bloggo Shortcuts](https://www.urionlinejudge.com.br/judge/en/problems/view/1239) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1239/Problem1239/Program.cs)

[1241		Fit or Dont Fit II](https://www.urionlinejudge.com.br/judge/en/problems/view/1241) - [[**Решение 1**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1241/Problem1241/Program.cs) [[**Решение 2**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1241_v2/Problem1241_v2/Program.cs)

[1243		How Easy](https://www.urionlinejudge.com.br/judge/en/problems/view/1243) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1243/Problem1243/Program.cs)

[1248		Diet Plan](https://www.urionlinejudge.com.br/judge/en/problems/view/1248) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1248/Problem1248/Program.cs)

[1249		Rot13](https://www.urionlinejudge.com.br/judge/en/problems/view/1249) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1249/Problem1249/Program.cs)

[1253		Caesar Cipher](https://www.urionlinejudge.com.br/judge/en/problems/view/1253) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1253/Problem1253/Program.cs)

[1255		Letter Frequency](https://www.urionlinejudge.com.br/judge/en/problems/view/1255) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1255/Problem1255/Program.cs)

[1257		Array Hash](https://www.urionlinejudge.com.br/judge/en/problems/view/1257) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1257/Problem1257/Program.cs)

[1272		Hidden Message](https://www.urionlinejudge.com.br/judge/en/problems/view/1272) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1272/Problem1272/Program.cs)

[1273		Justifier](https://www.urionlinejudge.com.br/judge/en/problems/view/1273) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1273/Problem1273/Program.cs)

[1277		Short Attendance](https://www.urionlinejudge.com.br/judge/en/problems/view/1277) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1277/Problem1277/Program.cs)

[1287		Friendly Int Parser](https://www.urionlinejudge.com.br/judge/en/problems/view/1287) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1287/Problem1287/Program.cs)

[1367		Help!](https://www.urionlinejudge.com.br/judge/en/problems/view/1367) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1367/Problem1367/Program.cs)

[1516	Image](https://www.urionlinejudge.com.br/judge/en/problems/view/1516) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1516/Problem1516/Program.cs)

[2714	My Temporary Password](https://www.urionlinejudge.com.br/judge/en/problems/view/2714) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem2714/Problem2714/Program.cs)

[2025	Joulupukki](https://www.urionlinejudge.com.br/judge/en/problems/view/2025) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem2025/Problem2025/Program.cs)

[1519	Abbreviations](https://www.urionlinejudge.com.br/judge/en/runs/code/12111253) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1519/Problem1519/Program.cs)

###DATA STRUCTURES AND LIBRARIES

[1022	TDA Rational](https://www.urionlinejudge.com.br/judge/en/problems/view/1022) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1022/Problem1022/Program.cs)

[1110	Throwing Cards Away](https://www.urionlinejudge.com.br/judge/en/problems/view/1110) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1110/Problem1110/Program.cs)

[1069	Diamonds and Sand](https://www.urionlinejudge.com.br/judge/en/runs/code/12042968) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1069/Problem1069/Program.cs)

[1068	Parenthesis Balance I](https://www.urionlinejudge.com.br/judge/en/problems/view/1068) - [[**Решение 1 - stack**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1068/Problem1068/Program.cs) | [[**Решение 2**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1068_2/Problem1068_2/Program.cs) 

[1519	Abbreviations](https://www.urionlinejudge.com.br/judge/en/problems/view/1519) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1519/Problem1519/Program.cs)

[1256	Hash Tables](https://www.urionlinejudge.com.br/judge/en/problems/view/1256) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1256/Problem1256/Program.cs)

###AD-HOC

[ООП - ШАХМАТЫ 	1206	Challenge of St. Petersburg](https://www.urionlinejudge.com.br/judge/en/problems/view/1206) - [[**Решение - ООП реализация**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1206/Problem1206/Program.cs)

[1030	Flavious Josephus Legend](https://www.urionlinejudge.com.br/judge/en/problems/view/1030) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1030/Problem1030/Program.cs)

[1032	Joseph’s Cousin](https://www.urionlinejudge.com.br/judge/en/problems/view/1032) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1032/Problem1032/Program.cs)

[1285	Different Digits](https://www.urionlinejudge.com.br/judge/en/problems/view/1285) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1285/Problem1285/Program.cs)

[1136	Bingo!](https://www.urionlinejudge.com.br/judge/en/problems/view/1136) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1136/Problem1136/Program.cs)

[1104	Exchanging Cards](https://www.urionlinejudge.com.br/judge/en/problems/view/1104) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1104/Problem1104/Program.cs)

[1089	Musical Loop](https://www.urionlinejudge.com.br/judge/en/problems/view/1089) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1089/Problem1089/Program.cs)

[1271	Where Are My Genes](https://www.urionlinejudge.com.br/judge/en/problems/view/1271) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1271/Problem1271/Program.cs)

[1129	Optical Reader](https://www.urionlinejudge.com.br/judge/en/problems/view/1129) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1129/Problem1129/Program.cs)

[1121	Sticker Collector Robot](https://www.urionlinejudge.com.br/judge/en/problems/view/1121) - [[**Решение**]](https://bitbucket.org/onlogn/intro.programming/src/master/uri/Problem1121/Problem1121/Program.cs)