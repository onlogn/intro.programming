﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0702
{
    class Program
    {
        static void Main(string[] args)
        {

            //show menu
            while(true)
            {
                Console.Write("1. Inch -> Cm\n2. Cm - Inch\n3. Stop\n");
                Console.Write("Your choise: ");
                int action = int.Parse(Console.ReadLine());

                const double CmPerInch = 2.54;


                if (action == 3)
                {
                    Console.WriteLine("Bye bye");
                    break;
                }

                switch (action)
                {
                    case 1:
                        Console.Write($"Inch: ");
                        double inch = double.Parse(Console.ReadLine());
                        Console.WriteLine($"Cm = {CmPerInch*inch:f4}"); 
                        break;
                    case 2:
                        Console.Write($"Cm: ");
                        double cm = double.Parse(Console.ReadLine());
                        Console.WriteLine($"Inch = {cm/CmPerInch:f4}");
                        break;
                    default:
                        Console.WriteLine("Incorrect choise");
                        break;
                } 
            }
        }
    }
}
