﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1405
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileInput = "input.txt";
            string fileOutput = "output.txt";

            using (StreamReader sr = new StreamReader(fileInput))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] res = line.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
                    string name = res[0];

                    int sum = res.Where(i => int.TryParse(i, out int t)).Select(int.Parse).Sum();
                    
                    Console.WriteLine($"{name,20} {sum}");
                }
            }
        }
    }
}
