﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrixes
{
    public class Matrix
    {
        #region properties
        public int Rows { get; private set; }
        public int Columns { get; private set; }
        private int[,] _elements;
        #endregion

        public int this[int i, int j]
        {
            get { return _elements[i,j]; }
            set { _elements[i, j] = value; }
        }

        /// <summary>
        /// Array with data
        /// </summary>
        /// <param name="elements">int[,]</param>
        public Matrix(int[,] elements)
        {
            if (elements == null || elements.GetLength(0) < 1 || elements.GetLength(1) < 1)
            {
                throw new ArgumentException("Data must be correct");
            } 
            _elements = elements.Clone() as int[,]; 
            Rows = _elements.GetLength(0);
            Columns = _elements.GetLength(1);
        }

        public Matrix(int rows, int columns)
        {
            if (rows < 1 || columns < 1)
            {
                throw new ArgumentException("Number of rows and columns must be more than 0");
            }
            Rows = rows;
            Columns = columns;
            _elements = new int[rows,columns];
        }

        public Matrix(Matrix a):this(a._elements)
        {

        }

        public int Determinant()
        {
            if (Rows != Columns)
            {
                throw new Exception("Matrix must be NxN");
            }

            if (Rows > 2)
            {
                throw new NotImplementedException("Потому что потом!");
            }

            if (Rows == 2)
            {
                return _elements[0, 0] * _elements[1, 1] - _elements[0, 1] * _elements[1, 0];
            }

            return 0;
        }

        public Matrix Transpose()
        {
            Matrix a = new Matrix(Columns,Rows);
            for (int i = 0; i < Columns; i++)
            {
                for (int j = 0; j < Rows; j++)
                {
                    a[i, j] = this[j,i];

                }
            }

            return a;
        }

        public static Matrix operator + (Matrix a, Matrix b)
        {
            if (a.Rows != b.Rows || a.Columns != b.Columns)
            {
                throw new Exception("Matrix must have the same dimensions!");
            }

            Matrix c = new Matrix(a.Rows,a.Columns);

            for (int i = 0; i < a.Rows; i++)
            {
                for (int j = 0; j < a.Columns; j++)
                {
                    c[i, j] = a[i, j] + b[i, j];
                }
            }

            return c;
        }

        public static Matrix operator - (Matrix a, Matrix b)
        {
            if (a.Rows != b.Rows || a.Columns != b.Columns)
            {
                throw new Exception("Matrix must have the same dimensions!");
            }

            Matrix c = new Matrix(a.Rows, a.Columns);

            for (int i = 0; i < a.Rows; i++)
            {
                for (int j = 0; j < a.Columns; j++)
                {
                    c[i, j] = a[i, j] - b[i, j];
                }
            }

            return c;
        }

        public static Matrix operator * (int n, Matrix a)
        {
            Matrix c = new Matrix(a.Rows, a.Columns);

            for (int i = 0; i < a.Rows; i++)
            {
                for (int j = 0; j < a.Columns; j++)
                {
                    c[i, j] = a[i, j] * n;
                }
            }

            return c;
        }

        public static Matrix operator *(Matrix a, int n)
        {
            return n * a;
        }

        public static Matrix operator *(Matrix a, Matrix b)
        {
            if (a.Columns != b.Rows)
            {
                throw new Exception("It's unreal");
            }

            Matrix c = new Matrix(a.Rows,b.Columns);
 
            for (int i = 0; i < a.Rows; i++)
            {
                for (int j = 0; j < b.Columns; j++)
                {
                    for (int k = 0; k < b.Rows; k++)
                    {
                        c[i, j] += a[i, k] * b[k, j];
                    }
                }
            }

            return c;
        }
    

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    str.Append(_elements[i, j]);
                    str.Append(" ");
                }

                str.Append(Environment.NewLine);
            }
            return str.ToString();
        }

        //этот метод скопипастила 
        private bool Equals(Matrix a)
        {
            if (ReferenceEquals(null, a)) return false;
            if (ReferenceEquals(this, a)) return true;
            return Equals(a.Rows, Rows) && Equals(a.Columns, Columns) && (a._elements.Cast<int>().SequenceEqual(_elements.Cast<int>()));
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Matrix)) return false;
            return Equals((Matrix)obj);
        }

        public override int GetHashCode()
        {
            int num = 13;

            var res = Rows.GetHashCode();
            res = res * num + Columns.GetHashCode();
            res = res * num + _elements.Cast<int>().Aggregate((x, y) => x * num + y);
            
            
            return res;
        }
    }
}
