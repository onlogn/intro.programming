﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1537
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int cnt = 0;

            for (int i = 0; i <= n / 10; i++)
            {
                for (int j = 0; j <= (n - i * 10) / 5; j++)
                {
                    for (int k = 0; k <= (n - i * 10 - j * 5) / 2; k++)
                    {
                        cnt++;
                    }
                }
            }

            Console.WriteLine(cnt);
        }
    }
}
