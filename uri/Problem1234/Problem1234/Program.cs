﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1234
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            while ((str = Console.ReadLine())!=null)
            {
                bool ix = true;
                foreach (char c in str)
                {
                    
                    if (Char.IsLetter(c))
                    {
                        if (ix)
                        {
                            Console.Write(Char.ToUpper(c));
                            ix = false;
                        }
                        else
                        {
                            Console.Write(Char.ToLower(c));
                            ix = true;
                        }
                    }
                    else
                    {
                        Console.Write(c);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
