﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1273
{
    class Program
    {
        static void Main(string[] args)
        {
            int numOfTests;
            bool first = true;
            while ((numOfTests = int.Parse(Console.ReadLine())) != 0)
            {
                if(first) {
                    first = false;
                }
                else
                {
                    Console.WriteLine();
                }

                string[] str = new string[numOfTests];
                int max = 0;
                for (int tests = 0; tests < numOfTests; tests++)
                {
                    str[tests] = Console.ReadLine();
                    if (str[tests].Length > max)
                        max = str[tests].Length;
                }

                foreach (string s in str)
                {
                    Console.WriteLine(AddSpaces(max - s.Length) + s);
                }
                
            }
        }

        static string AddSpaces(int numOfSpaces)
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < numOfSpaces; i++)
            {
                str.Append(" ");
            }
            return str.ToString();
        }
    }
}
