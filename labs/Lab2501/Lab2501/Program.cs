﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2501
{
    class Program
    {
        static void Main(string[] args)
        {
            HashSet<int> set1 = new HashSet<int>()
            {
                1,2,3,5,9,4
            };

            set1.ForEach(Console.WriteLine);

            HashSet<Rational> set2 =new HashSet<Rational>()
            {
                new Rational(1,2),
                new Rational(1,2),
                new Rational(1,2),
                new Rational(3,2),
                new Rational(2,5),
            };

            set2.ForEach(Console.WriteLine);

            Hz hz = new Hz();
            int[] a = hz.Data as int[];
            a[0] = 1000;

            Console.WriteLine(String.Join(" ",hz.Data));
        }
    }

    public class Hz
    {
        //public int[] Data { get; }
        public IReadOnlyList<int> Data { get; }

        public Hz()
        {
            Data = new int[] {1, 2, 3};
        }
    }

    public static class Utils
    {
        public static void ForEach<T>(this IEnumerable<T> container, Action<T> action)
        {
            foreach (T e in container)
            {
                action(e);
            }
        } 
    }
}
