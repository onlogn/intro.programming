﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game15
{
    public partial class MainForm : Form
    {
        private Game15 _game;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            NewGame();
        }

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            int col = e.X / Game15.ItemSize;
            int row = e.Y / Game15.ItemSize;

            _game.Move(new Position(row,col));
            ShowField();
            if (_game.IsGameOver)
            {
                MessageBox.Show($"Поздравляю! Твое количество ходов: {_game.Counter}");
                NewGame();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            NewGame();
        }

        private void ShowField()
        {
            pictureBox.Image = _game.GetImage();
            pictureBox.Refresh();
            ShowSteps();
        }

        private void ShowSteps()
        {
            lblInfo.Text = $"Сделано шагов: {_game.Counter}";
        }

        private void NewGame()
        {
            _game = new Game15();
            _game.Shuffle(1000);
            ShowField();
        }
    }
}
