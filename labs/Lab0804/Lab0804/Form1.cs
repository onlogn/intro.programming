﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab0804
{
    public partial class Form1 : Form
    {
        private int c = 0;
        private int dc = 1;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            int selfSize = 500;

            int x1 = Width / 2 - selfSize - 50;
            int x2 = Width / 2 + 50;
            int y = Height / 2 - selfSize / 2;

           
            Brush b1 = new SolidBrush(Color.FromArgb(c, 0, 0));
            Brush b2 = new SolidBrush(Color.FromArgb(255 - c, 0, 0));

            g.FillRectangle(b1, x1, y, 500, 500);
            g.FillRectangle(b2, x2, y, 500, 500);


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (c + dc == 256 || c + dc == -1) 
            {
                dc = -dc;
            }

            c += dc;

            Refresh();
        }
    }
}
