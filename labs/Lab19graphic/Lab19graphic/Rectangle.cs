﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab19graphic
{
    public sealed class Rectangle : Figure
    {
        private Point _topLeft;
        public Rectangle(Point p, Color c): base(p, c)
        {
            
        }

        public Rectangle(FigureData f):this(new Point(f.X, f.Y),f.Color)
        {
            this.SetSize(f.Width,f.Height);
        }

        public override void SetSize(int w, int h)
        {
            Width = w;
            Height = h;
            TopLeftCalc();   
        }

        public override void Draw(Graphics g)
        {
            Brush b = new SolidBrush(Color);
            g.FillRectangle(b, _topLeft.X, _topLeft.Y, Math.Abs(Width), Math.Abs(Height));
        }

        public override bool IsInFigureRange(Point p)
        {
            return p.X >= _topLeft.X && p.X <= _topLeft.X + Math.Abs(Width) && p.Y >= _topLeft.Y && p.Y <= _topLeft.Y + Math.Abs(Height);
        }

        public override string ToString()
        {
            return $"Rectangle: ({Position.X}, {Position.Y}) Width = {Width}, Height = {Height}";
        }

        public override void Move(Point p)
        {
            base.Move(p);
            TopLeftCalc();
        }

        public override FigureData GetInfoForSerialization()
        {
            return new FigureData()
            {
                FigureType = typeof(Rectangle),
                Color = Color,
                Height = Height,
                Width = Width,
                X = Position.X,
                Y = Position.Y
            };
        }

        private void TopLeftCalc()
        {
            int ix = Position.X;
            int iy = Position.Y;
            if (Width < 0)
            {
                ix = Position.X - Math.Abs(Width);
            }

            if (Height < 0)
            {
                iy = Position.Y - Math.Abs(Height);
            }

            _topLeft = new Point(ix, iy);
        }
    }
}
