﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem118
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine(ReverseNumbers(n));
        }

        private static int ReverseNumbers(int n)
        {
            return int.Parse(string.Join("",n.ToString().Reverse()));
        }
    }
}
