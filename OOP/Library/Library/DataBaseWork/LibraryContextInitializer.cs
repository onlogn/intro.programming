﻿using System.Data.Entity;
using Library.DataBaseModels;

namespace Library.DataBaseWork
{
    public class LibraryContextInitializer: CreateDatabaseIfNotExists<LibraryContext>
    {
        protected override void Seed(LibraryContext db)
        {
            CategoryData c1 = new CategoryData() { Name = "Художественная"};
            CategoryData c2 = new CategoryData() { Name = "Журналы"};
            CategoryData c3 = new CategoryData() { Name = "Программирование"};
            CategoryData c4 = new CategoryData() { Name = "Учебники"};
            CategoryData c5 = new CategoryData() { Name = "Газеты"};
            

            db.Categories.Add(c1);
            db.Categories.Add(c2);
            db.Categories.Add(c3);
            db.Categories.Add(c4);
            db.Categories.Add(c5);
            
            db.SaveChanges();
        }
    }
}
