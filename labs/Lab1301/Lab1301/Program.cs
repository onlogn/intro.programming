﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1301
{
    class Program
    {
        static void Main(string[] args)
        {
            char c = 'A';
            Console.WriteLine($"English letter: {c} (code: {(int)c})");

            c = 'Ф';
            Console.WriteLine($"Russian letter: {c} (code: {(int)c})");

            c = '\u03C0';
            Console.WriteLine($"Russian letter: {c} (code: {(int)c})");

            ShowChars('a','z');

            ShowChars('А', 'Я');

            c = 'Ё';
            Console.WriteLine($"Russian letter: {c} (code: {(int)c})");
        }

        private static void ShowChars(int from, int to)
        {
            int cnt = 0;
            for (int i = from; i <= to; i++)
            {
                if (cnt == 8)
                {
                    Console.WriteLine();
                    cnt = 0;
                }
                Console.Write($"{(char)i} ");
                cnt++;
            }
            Console.WriteLine();
        }
    }
}
