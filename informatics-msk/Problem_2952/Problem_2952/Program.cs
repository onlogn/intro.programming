﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_2952
{
    class Program
    {
        static void Main(string[] args)
        {
            int hh1 = int.Parse(Console.ReadLine());
            int mm1 = int.Parse(Console.ReadLine());
            int ss1 = int.Parse(Console.ReadLine());
            int hh2 = int.Parse(Console.ReadLine());
            int mm2 = int.Parse(Console.ReadLine());
            int ss2 = int.Parse(Console.ReadLine());

            int first = ss1 + mm1 * 60 + hh1 * 60 * 60;
            int last = ss2 + mm2 * 60 + hh2 * 60 * 60;

            int res = last - first;

            Console.WriteLine(res);
        }
    }
}
