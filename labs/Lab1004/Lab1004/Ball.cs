﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1004
{
    class Ball
    {
        public double X { get; private set; }
        public double Y { get; private set; }
        public int Diameter { get; private set; }
        public double Dx { get; private set; }
        public double Dy { get; private set; }
        public Color BallColor { get; private set; }

        public Ball(double x, double y, int d, double dx, double dy, Color cl)
        {
            X = x;
            Y = y;
            Diameter = d;
            Dx = dx;
            Dy = dy;
            BallColor = cl;
        }


        public bool MoveBall(int height)
        {
            Y += Dy;
            if (Y <= height)
                return true;
            return false;
        }

        public static Ball GenerateRandomBall(int posX)
        {
            Random rnd = new Random();

            double resy = rnd.Next(30, 50)*0.10;

            int d = rnd.Next(10, 41);

            return new Ball(posX, 0, d, 0, resy,
                Color.FromArgb(rnd.Next(1, 256), rnd.Next(1, 256), rnd.Next(1, 256)));
        }

    }
}
