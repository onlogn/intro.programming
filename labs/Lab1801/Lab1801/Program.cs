﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1801
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("reftype");

            SomeRefType r1 = new SomeRefType(3);
            SomeRefType r2 = new SomeRefType(33);

            Console.WriteLine($"r1 = {r1}, r2 = {r2}");
            r1 = r2;
            Console.WriteLine($"r1 = {r1}, r2 = {r2}");
            r2._data = 42;
            Console.WriteLine($"r1 = {r1}, r2 = {r2}");

            Console.WriteLine("valtype");
            var v1 = new SomeValType(3);
            var v2 = new SomeValType(33);

            Console.WriteLine($"v1 = {v1}, v2 = {v2}");
            v1 = v2;
            Console.WriteLine($"v1 = {v1}, v2 = {v2}");
            v2._data = 42;
            Console.WriteLine($"v1 = {v1}, v2 = {v2}");
        }
    }

    class SomeRefType
    {
        public int _data;

        public SomeRefType(int data)
        {
            _data = data;
        }

        public override string ToString()
        {
            return _data.ToString();
        }
    }

    struct SomeValType
    {
        public int _data;

        public SomeValType(int data)
        {
            _data = data;
        }

        public override string ToString()
        {
            return _data.ToString();
        }
    }
}
