﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DataBaseModels;

namespace Library.DataBaseWork
{
    public static class CategoryDataBaseService
    {
        public static async Task<List<Category>> GetCategoryList()
        {
            using (LibraryContext db = new LibraryContext())
            {
               return (await db.Categories.OrderBy(i=>i.Name).ToListAsync()).Select(i => new Category(i.Id, i.Name)).ToList();
            }

        }
    }
}
