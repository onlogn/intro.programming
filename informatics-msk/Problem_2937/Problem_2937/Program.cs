﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_2937
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine($"The next number for the number {a} is {a+1}.");
            Console.WriteLine($"The previous number for the number {a} is {a-1}.");
        }
    }
}
