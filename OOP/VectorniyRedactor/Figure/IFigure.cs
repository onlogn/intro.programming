﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures
{
    public interface IFigure
    {
        Point Position { get; }
        int Width { get; }
        int Height { get; }
        Color Color { get; }

        void SetSize(int w, int h);

        void Draw(Graphics g);

        bool IsInFigureRange(Point p);

        void Move(Point p);

        FigureData GetInfoForSerialization();
    }
}
