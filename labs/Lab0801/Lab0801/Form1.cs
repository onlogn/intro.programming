﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab0801
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
            Random rnd = new Random();
            double r = 350;
            for (int i = 0; i < 5000; i++)
            {
                // Create points that define line.
                Pen blackPen = new Pen(Color.FromArgb(rnd.Next(255),rnd.Next(255),rnd.Next(255)));
                int x1 = Width / 2;
                int y1 = Height / 2;
                int x2 = rnd.Next(Width);
                int y2 = rnd.Next(Width);
                double d = Math.Sqrt(Math.Pow(x2 - x1,2) + Math.Pow(y2 - y1,2));
                if(d < r) e.Graphics.DrawLine(blackPen, x1, y1, x2, y2);
            }
        }
    }
}
