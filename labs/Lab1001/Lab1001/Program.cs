﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1001
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = int.Parse(Console.ReadLine());
            int[] arr = new int[size];

            if (size != 0)
            {
                arr[0] = 1;
                arr[arr.Length - 1] = 1;

                arr[arr.Length / 2] = 1;

                if (arr.Length % 2 == 0)
                {
                    arr[arr.Length / 2 - 1] = 1;
                }

                Console.WriteLine(arr.ArrToStr());
            }
        }
    }

    public static class StringExtension
    {
        public static string ArrToStr(this int[] arr)
        {
            return String.Join(" ", arr);
        }
    }
}
