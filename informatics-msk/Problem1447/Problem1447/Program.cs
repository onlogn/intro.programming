﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1447
{
    class Program
    {
        static void Main(string[] args)
        {
            var inp = Console.ReadLine().Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Skip(1).Select(n => int.Parse(n)).ToArray();

            var minVal = inp.Min();
            var maxVal = inp.Max();

            var res = inp.Select(i => i == maxVal ? minVal : i);

            Console.WriteLine(String.Join(" ",res));
            
        }
    }
}
