﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Problem1206
{
    abstract class ChessFigure
    {
        public Point Location { get; private set; }

        public ChessFigure(Point p)
        {
            Location = p;
        }
    
        public List<Point> ListOfAllPositions(List<Point> useless)
        {
            List<Point> res = new List<Point>();

            for (int i = 1; i <= 8; i++)
            {
                for (int j = 1; j <= 8; j++)
                {
                    if (CanGetToDestination(new Point(i, j), useless))
                    {
                        res.Add(new Point(i,j));
                    }
                }
            }

            return res;
        }

        public abstract bool CanGetToDestination(Point p, List<Point> restricted);

    }

    abstract class FigureWithSpecificMoves : ChessFigure
    {
        protected abstract List<Point> MovementList { get; set; }

        public FigureWithSpecificMoves(Point p): base(p)
        {
            
        }

        public List<Point> GetAvailableAttackPoints(List<Point> restricted)
        {
            List<Point> res = new List<Point>();

            foreach (Point p in MovementList)
            {
                res.AddRange(GetAvailableMovements(p, restricted));
            }

            return res;
        }

        private List<Point> GetAvailableMovements(Point currentDirection, List<Point> restricted)
        {
            List<Point> res = new List<Point>();
            Point i = new Point(Location);
            while (i.IsInRange())
            {
                i.MovePoint(currentDirection);
                res.Add(new Point(i));
                if (restricted.Any(k => k.Equals(i)))
                    break;
            }
            return res;
        }

        public override bool CanGetToDestination(Point p, List<Point> restricted)
        {
            return GetAvailableAttackPoints(restricted).Any(i => i.Equals(p));
        }
    }

    //ладья
    class Rook : FigureWithSpecificMoves
    {
        protected override List<Point> MovementList { get; set; }
             
        public Rook(Point p) : base(p)
        {
            MovementList = new List<Point>()
            {
                new Point(0,1),
                new Point(0,-1),
                new Point(1,0),
                new Point(-1,0)
            };
        }
    }

    //слон
    class Bishop : FigureWithSpecificMoves
    {
        public Bishop(Point p): base(p)
        {
            MovementList = new List<Point>()
            {
                new Point(-1,1),
                new Point(1,1),
                new Point(1,-1),
                new Point(-1,-1)
            };
        }

        protected override List<Point> MovementList { get; set; }
    }

    class Queen : FigureWithSpecificMoves
    {
        public Queen(Point p):base(p)
        {
            MovementList = new List<Point>()
            {
                new Point(-1,1),
                new Point(1,1),
                new Point(1,-1),
                new Point(-1,-1),
                new Point(0,1),
                new Point(0,-1),
                new Point(1,0),
                new Point(-1,0)
            };
        }

        protected override List<Point> MovementList { get; set; }
    }

    class King : ChessFigure
    {
        public King(Point p):base(p)
        {
            
        }

        public override bool CanGetToDestination(Point p, List<Point> useless)
        {
            return (Math.Abs(p.X - Location.X) <= 1 && Math.Abs(p.Y - Location.Y) <= 1) && (Location.X != p.X || Location.Y != p.Y);
        }

    }
    //пешка
    class Pawn : ChessFigure
    {
        public Pawn(Point p):base(p)
        {
            
        }

        public override bool CanGetToDestination(Point p, List<Point> useless)
        {
            return (Math.Abs(p.X - Location.X) == 1 && p.Y - Location.Y == -1) && (Location.X != p.X || Location.Y != p.Y);
        }
    }
}
