﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problems_DigitsAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine(SumOfDigits(n));
        }

        private static int SumOfDigits(int n)
        {
            int res = 0;
            while (n != 0)
            {
                res += n % 10;
                n /= 10;
            }
            return res;
        }
    }
}
