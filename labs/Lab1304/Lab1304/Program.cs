﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1304
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] d = new int['Z' - 'A' + 1];

            int c;
            char key;
            while ((c = Console.Read()) != -1)
            {
                key = Char.ToUpper((char)c);
                if (key >= 'A' && key <= 'Z')
                {
                    d[key - 'A']++;
                }
            }

            for (int i = 0; i < d.Length; i++)
            {
                Console.WriteLine($"{(char)(i + 'A')}: {d[i]}");
            }
            
        }
    }
}
