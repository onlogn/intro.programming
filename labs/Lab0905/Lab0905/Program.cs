﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0905
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = GetInteger("Input A =", "Incorrect Number");
            int b = GetInteger("Input B =", "Incorrect Number");

            if (Add(a, b, out int c))
            {
                Console.WriteLine(c);
            }
            else
            {
                Console.WriteLine("Overflow");
            }
        }

        public static int GetInteger(string promptMsg, string errorMsg)
        {
            string input;

            while (true)
            {
                Console.Write(promptMsg);
                if (int.TryParse(input = Console.ReadLine(),out int res))
                    return res;
                Console.WriteLine(errorMsg);
            }
        }

        public static bool Add(int a, int b, out int c)
        {
            c = 0;
            try
            {
                c = checked(a + b);
                return true;

            }
            catch (OverflowException)
            {
                return false;
            }
        }
    }
}
