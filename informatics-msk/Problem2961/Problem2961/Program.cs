﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem2961
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> arr = new List<int>();
            for (int i = 0; i < 3; i++)
            {
                arr.Add(int.Parse(Console.ReadLine()));
            }

            arr.Sort();

            Console.WriteLine(arr.PrintList());
        }
    }

    public static class StringExtension
    {
        public static string PrintList(this List<int> lst)
        {
            StringBuilder s = new StringBuilder();
            bool flag = false;
            foreach (int i in lst)
            {
                s.Append(i);
                s.Append(" ");
            }

            return s.ToString();
        }
    }
}
