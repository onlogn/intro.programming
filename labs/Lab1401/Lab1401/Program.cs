﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1401
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            
            string spaces = new string(' ', str.Length + 2);

            Console.WriteLine(new string('*', str.Length + 4));
            Console.WriteLine($"*{spaces}*");
            Console.WriteLine($"* {str} *");
            Console.WriteLine($"*{spaces}*");
            Console.WriteLine(new string('*', str.Length + 4));

            
        }
    }
}
