﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0405
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Min\nInput numbers: \n");
            List<int> nums = new List<int>();
            for (int i = 0; i < 4; i++)
            {
                nums.Add(int.Parse(Console.ReadLine()));
            }

            Console.WriteLine($"Min = {nums.Min()}");

        }
    }
}
