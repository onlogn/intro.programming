﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1241_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());
            for (int ind = 0; ind < num; ind++)
            {
                string inpStr = Console.ReadLine();
                string[] words = inpStr.Split(new char[] {' '});

                Console.WriteLine(words[0].EndsWith(words[1]) ? "encaixa" : "nao encaixa");
            }
        }
    }
}
