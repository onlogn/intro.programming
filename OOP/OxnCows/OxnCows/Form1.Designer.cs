﻿namespace OxnCows
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtGameRules = new System.Windows.Forms.TextBox();
            this.btnStartNewGame = new System.Windows.Forms.Button();
            this.txtUserNumber = new System.Windows.Forms.TextBox();
            this.btnSendNumber = new System.Windows.Forms.Button();
            this.txtUserRes = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtGameRules
            // 
            this.txtGameRules.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGameRules.Enabled = false;
            this.txtGameRules.Location = new System.Drawing.Point(15, 12);
            this.txtGameRules.Multiline = true;
            this.txtGameRules.Name = "txtGameRules";
            this.txtGameRules.ReadOnly = true;
            this.txtGameRules.Size = new System.Drawing.Size(486, 141);
            this.txtGameRules.TabIndex = 0;
            // 
            // btnStartNewGame
            // 
            this.btnStartNewGame.Location = new System.Drawing.Point(15, 161);
            this.btnStartNewGame.Name = "btnStartNewGame";
            this.btnStartNewGame.Size = new System.Drawing.Size(158, 22);
            this.btnStartNewGame.TabIndex = 1;
            this.btnStartNewGame.Text = "Начать новую игру";
            this.btnStartNewGame.UseVisualStyleBackColor = true;
            this.btnStartNewGame.Click += new System.EventHandler(this.btnStartNewGame_Click);
            // 
            // txtUserNumber
            // 
            this.txtUserNumber.Location = new System.Drawing.Point(246, 161);
            this.txtUserNumber.Name = "txtUserNumber";
            this.txtUserNumber.Size = new System.Drawing.Size(117, 20);
            this.txtUserNumber.TabIndex = 2;
            // 
            // btnSendNumber
            // 
            this.btnSendNumber.Location = new System.Drawing.Point(369, 160);
            this.btnSendNumber.Name = "btnSendNumber";
            this.btnSendNumber.Size = new System.Drawing.Size(107, 23);
            this.btnSendNumber.TabIndex = 3;
            this.btnSendNumber.Text = "Мне повезёт!";
            this.btnSendNumber.UseVisualStyleBackColor = true;
            this.btnSendNumber.Click += new System.EventHandler(this.btnSendNumber_Click);
            // 
            // txtUserRes
            // 
            this.txtUserRes.AcceptsReturn = true;
            this.txtUserRes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserRes.Location = new System.Drawing.Point(15, 192);
            this.txtUserRes.Multiline = true;
            this.txtUserRes.Name = "txtUserRes";
            this.txtUserRes.ReadOnly = true;
            this.txtUserRes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtUserRes.Size = new System.Drawing.Size(348, 283);
            this.txtUserRes.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::OxnCows.Properties.Resources.oxncowspic;
            this.pictureBox1.Location = new System.Drawing.Point(369, 192);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(132, 285);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnSendNumber;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 487);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtUserRes);
            this.Controls.Add(this.btnSendNumber);
            this.Controls.Add(this.txtUserNumber);
            this.Controls.Add(this.btnStartNewGame);
            this.Controls.Add(this.txtGameRules);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(525, 526);
            this.Name = "Form1";
            this.Text = "Oxes and Cows";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtGameRules;
        private System.Windows.Forms.Button btnStartNewGame;
        private System.Windows.Forms.TextBox txtUserNumber;
        private System.Windows.Forms.Button btnSendNumber;
        private System.Windows.Forms.TextBox txtUserRes;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

