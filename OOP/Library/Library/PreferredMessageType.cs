﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public enum PreferredMessageType
    {
        SMS,
        WhatsApp,
        Telegram,
        Email,
        Call
    }
}
