﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Lab19graphic
{
    public static class SaveLoad
    {
        

        public static void Save(List<Figure> list, string filename)
        {
            WriteFile(filename, list.Select(i => i.GetInfoForSerialization()).ToList());
        }

        public static List<Figure> Load(string filename)
        {
            //return ReadFile(filename).Select(i => FigureDataToFigure(i)).ToList();
            return ReadFile(filename).Select(FigureDataToFigure).ToList();
        }

        private static List<FigureData> ReadFile(string filename)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<FigureData>));

            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                return (List<FigureData>)jsonFormatter.ReadObject(fs);
            }

        }

        private static void WriteFile(string filename, List<FigureData> figures)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<FigureData>));

            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(fs, figures);
            }
        }

        private static Figure FigureDataToFigure(FigureData fd)
        {
            //activator всегда object возвращает, тут кастуем в нужный нам тип
            return (Figure)Activator.CreateInstance(fd.FigureType, fd);
        }
    }
}
