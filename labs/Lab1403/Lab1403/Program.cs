﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1403
{
    class Program
    {
        static void Main(string[] args)
        {
            // comment for delete
            string s;
            string file = @"..\..\Program.cs";
            string comment = "//";
            //somment for delete
            StringBuilder sbRes = new StringBuilder();
            using (StreamReader sr = new StreamReader(file))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    int idx = line.IndexOf(comment);
                    if (idx != -1)
                    {
                        sbRes.AppendLine(line.Remove(idx));
                    }
                    else
                    {
                        sbRes.AppendLine(line);
                    }
                }
            }

            Console.WriteLine(sbRes);
        }
    }
}
