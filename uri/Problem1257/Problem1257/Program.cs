﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1257
{
    class Program
    {
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            for (int counter = 0; counter < m; counter++)
            {
                int n = int.Parse(Console.ReadLine());
                int res = 0;
                for (int line = 0; line < n; line++)
                {
                    string str = Console.ReadLine();
                    
                    for(int i = 0; i < str.Length; i++) 
                    {
                        res +=str[i]-65 + i + line; 
                    }
                }
                Console.WriteLine(res);
            }

        }
    }
}
