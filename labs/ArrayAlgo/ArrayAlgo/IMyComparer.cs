﻿namespace ArrayAlgo
{
    public interface IMyComparer<T>
    {
        int Compare(T o1, T o2);
    }

    public interface IMyComparer
    {
        int Compare(object o1, object o2);
    }
}
