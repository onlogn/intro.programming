﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Problem1259
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            while ((s = Console.ReadLine()) != null)
            {
                int num = int.Parse(s);
                List<int> res= new List<int>();

                for (int i = 0; i < num; i++)
                {
                    res.Add(int.Parse(Console.ReadLine()));
                }

                var ans = res.OrderBy(i => i % 2 != 0).ThenBy(i => i%2 ==0 ? i : 0).ThenByDescending(i => i%2 !=0 ? i : 0).ToList();
                Console.WriteLine(String.Join("\n",ans));
            }
        }
    }
}
