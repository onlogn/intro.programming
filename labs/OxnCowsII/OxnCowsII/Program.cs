﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxnCowsII
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> guesses = GenerateListIfGuesses();

            int ox = 0, cow = 0;
            Random rnd = new Random();
            
            while (ox != 4 && guesses.Count != 0)
            {
                int computerGuess = guesses[rnd.Next(0, guesses.Count)];
                Console.WriteLine(computerGuess);
                Console.Write("Ox = ");
                ox = int.Parse(Console.ReadLine());
                Console.Write("Cows = ");
                cow = int.Parse(Console.ReadLine());

                guesses = FillterGuesses(guesses, computerGuess,ox,cow);
            }

        }

        private static List<int> FillterGuesses(List<int> guesses, int computerGuess, int ox, int cow)
        {
            List<int> res = new List<int>();

            foreach (int g in guesses)
            {
                var t = GetCntOxnCows(g,computerGuess);
                if (ox == t.Item1 && cow == t.Item2)
                {
                    res.Add(g);
                }
            }

            return res;
        }

        private static Tuple<int, int> GetCntOxnCows(int userNumber, int originalNumber)
        {
            int ox = 0;
            int cow = 0;

            for (int i = 0; i < 4; i++)
            {
                if (userNumber.ToString()[i] == originalNumber.ToString()[i])
                {
                    ox++;
                }

                if (originalNumber.ToString().Contains(userNumber.ToString()[i]))
                {
                    cow++;
                }
            }

            return Tuple.Create(ox, cow);
        }

        private static bool HasNotSameDigits(int a)
        {
            string s1 = a.ToString();
            string s2 = a.ToString();
            return s1.Length == s2.Distinct().Count();
        }

        private static List<int> GenerateListIfGuesses()
        {
            List<int> guesses = new List<int>();

            for (int i = 1000; i < 10000; i++)
            {
                if (HasNotSameDigits(i))
                {
                    guesses.Add(i);
                }
            }

            return guesses;
        }

        

        private static bool IsCorrectData(string number)
        {
            return int.TryParse(number, out int res) && (res > 0 && res < 5);
        }
    }
}
