﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1501
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            StringBuilder sb = new StringBuilder();

            foreach (char s in str)
            {
                if (Char.IsLetter(s))
                {
                    sb.Append(Char.ToLower(s));
                }
            }

            Console.WriteLine(IsPalindrome(sb.ToString()) ? "Yes" : "No");
        }

        private static bool IsPalindrome(string str)
        {
            for (int i = 0; i < str.Length / 2; i++)
            {
                if (str[i] != str[str.Length - 1 - i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
