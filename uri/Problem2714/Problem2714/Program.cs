﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Problem2714
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());

            for (int nt = 0; nt < num; nt++)
            {
                string str = Console.ReadLine();

                Regex r = new Regex(@"^RA(\d{18})");

                var res = r.Match(str);

                Console.WriteLine(res.Groups[1].Value == String.Empty ? "INVALID DATA" : res.Groups[1].Value.TrimStart('0'));


            }
        }
    }
}
