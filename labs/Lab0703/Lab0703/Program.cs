﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0703
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Numerator: ");
                int numerator = int.Parse(Console.ReadLine());

                Console.Write("Denominator: ");
                int denomenator = int.Parse(Console.ReadLine());

                if (denomenator == 0) continue;
                Console.WriteLine($"Result = {numerator / denomenator}");
                Console.WriteLine($"Modulo = {numerator % denomenator}");

            }
        }
    }
}
