﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1478
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();

            string[] res = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            var ans = res.Select(x => int.Parse(x)).ToArray();

            Console.WriteLine((ans[0]-1)*6+ans[1]);
        }
    }
}
