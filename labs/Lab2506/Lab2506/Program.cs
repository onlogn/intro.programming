﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2506
{
    class Program
    {
        static void Main(string[] args)
        {

            Dictionary<string, SortedSet<int>> dict = new Dictionary<string, SortedSet<int>>();

            string file = "file.txt";
            HashSet<string> languages = new HashSet<string>()
            {
                "c++", "c#", "java", "c"
            };

            int cnt = 0;
            string line;
            while ((line = Console.ReadLine()) != null)
            {
                string[] words = line.ToLower().Split(new[] { ' ', '.', ',', ':', '!', ';', '?' }, StringSplitOptions.RemoveEmptyEntries);
                cnt++;
                foreach (string word in words)
                {
                    if (!languages.Contains(word)) continue;
                    if (!dict.ContainsKey(word))
                    {
                        dict[word] = new SortedSet<int>();
                    }
                    dict[word].Add(cnt);
                }
            }


            foreach (var pair in dict.OrderByDescending(t=>t.Key))
            {
                Console.WriteLine($"{pair.Key} => ");
                foreach (int i in pair.Value)
                {
                    Console.WriteLine($"Line: {i}");
                }
            }

        }
    }
}
