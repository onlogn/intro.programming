﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2301
{
    class Program
    {
        static void Main(string[] args)
        {
            string file = "output.txt";
            using (StreamWriter sw = new StreamWriter(file))
            {
                for (int i = 1; i <= 100; i++)
                {
                    if (i == 21) throw new Exception("lol");
                    sw.Write($"{i,4}");
                    if (i % 10 == 0) sw.WriteLine();
                }

            }
        }
    }
}
