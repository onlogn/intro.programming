﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0103
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What's your name? ");
            string userName = Console.ReadLine();
            Console.WriteLine($"Hello, {userName}!");
        }
    }
}
