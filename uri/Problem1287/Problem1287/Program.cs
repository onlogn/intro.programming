﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1287
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            while ((str = Console.ReadLine())!=null)
            {
                str = str.Replace(",", "").Replace("o", "0").Replace("O", "0").Replace("l", "1").Replace(" ", "");

                if (Int32.TryParse(str, out int res))
                {
                    Console.WriteLine(res);
                }
                else
                {
                    Console.WriteLine("error");
                }
            } 
        }
    }
}
