﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab0903
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input numbers, separate them by SPACE: ");

            string str = Console.ReadLine();

            string[] s = str.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            var arr = s.Select(i => int.Parse(i)).ToArray();

            BubbleSort(arr);

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
        }

        static void Swap(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }

        static void BubbleSort(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = array.Length - 1; j > i; j--)
                {
                    if (array[j - 1] > array[j])
                    {
                        Swap(ref array[j - 1], ref array[j]);
                    }
                }
            }
        }
    }
}
