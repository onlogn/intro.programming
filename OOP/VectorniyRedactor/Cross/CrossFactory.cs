﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Figures;
using Point = Figures.Point;

namespace Cross
{
    public class CrossFactory: IFigureFactory
    {
        public string FigureName => "Cross";

        public Type FigureType => typeof(Cross);

        public IFigure CreateFigure(Point p, int w, int h, Color c)
        {
            Cross tmp = new Cross(p, c);
            tmp.SetSize(w, h);
            return tmp;
        }

        public IFigure CreateFigure(Point p, Color c)
        {
            return new Cross(p, c);
        }

        public IFigure CreateFigure(FigureData f)
        {
            return new Cross(f);
        }
    }
}
