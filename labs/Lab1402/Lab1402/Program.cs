﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1402
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            using (StreamReader sr = new StreamReader("file.txt", System.Text.Encoding.Default))
            {
                s = sr.ReadToEnd();
            }

            s = s.Trim();

            using (StreamWriter sw = new StreamWriter("file2.txt"))
            {
                sw.Write(s);
            }
        }
    }
}
