﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1052
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            DateTime d = new DateTime(2019,n,1);

            Console.WriteLine(d.Date.ToString("MMMM"));
        }
    }
}
