﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem119
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int cnt = 0;

            for (int i = 1; i <= n; i++)
            {
                cnt += IsPalindrom(i) ? 1 : 0;
            }
            Console.WriteLine(cnt);

        }
        
        private static bool IsPalindrom(int n)
        {
            string s = n.ToString();
            return s.Equals(String.Join("", n.ToString().Reverse()));
        }
    }
}
