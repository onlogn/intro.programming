﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class MessageInfo
    {
        public string Text { get; }
        public DateTime SendingTime { get; }

        public MessageInfo(string text, DateTime sendingTime)
        {
            Text = text;
            SendingTime = sendingTime;
        }
    }
}
