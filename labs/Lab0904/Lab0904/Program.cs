﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0904
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("a/b. \nInput a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Input b = ");
            int b = int.Parse(Console.ReadLine());

            Divide(a,b,out int q, out int r);

            Console.WriteLine($"{q} {r}");
        }

        public static void Divide(int a, int b, out int q, out int r)
        {
            if (b == 0)
            {
                throw new DivideByZeroException("b must be !=0");
            }
            q = 0;
            while (a >= b)
            {
                a -= b;
                q++;
                
            }
            r = a;
        }
    }
}
