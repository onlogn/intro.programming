﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3064
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            int cnt = 0;
            while ((s = Console.ReadLine()) != "0")
            {
                cnt++;
            }
            Console.WriteLine(cnt);
        }
    }
}
