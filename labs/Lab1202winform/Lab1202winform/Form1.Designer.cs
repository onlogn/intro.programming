﻿namespace Lab1202winform
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFigure = new System.Windows.Forms.Label();
            this.btnGenerateFigure = new System.Windows.Forms.Button();
            this.btnTurnLeft = new System.Windows.Forms.Button();
            this.btnTurnRight = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFigure
            // 
            this.lblFigure.AutoSize = true;
            this.lblFigure.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFigure.Location = new System.Drawing.Point(63, 20);
            this.lblFigure.Name = "lblFigure";
            this.lblFigure.Size = new System.Drawing.Size(0, 23);
            this.lblFigure.TabIndex = 0;
            // 
            // btnGenerateFigure
            // 
            this.btnGenerateFigure.Location = new System.Drawing.Point(57, 205);
            this.btnGenerateFigure.Name = "btnGenerateFigure";
            this.btnGenerateFigure.Size = new System.Drawing.Size(169, 43);
            this.btnGenerateFigure.TabIndex = 1;
            this.btnGenerateFigure.Text = "Generate Figure";
            this.btnGenerateFigure.UseVisualStyleBackColor = true;
            this.btnGenerateFigure.Click += new System.EventHandler(this.btnGenerateFigure_Click);
            // 
            // btnTurnLeft
            // 
            this.btnTurnLeft.Location = new System.Drawing.Point(57, 254);
            this.btnTurnLeft.Name = "btnTurnLeft";
            this.btnTurnLeft.Size = new System.Drawing.Size(81, 23);
            this.btnTurnLeft.TabIndex = 2;
            this.btnTurnLeft.Text = "TurnLeft - A";
            this.btnTurnLeft.UseVisualStyleBackColor = true;
            this.btnTurnLeft.Visible = false;
            this.btnTurnLeft.Click += new System.EventHandler(this.btnTurnLeft_Click);
            // 
            // btnTurnRight
            // 
            this.btnTurnRight.Location = new System.Drawing.Point(144, 254);
            this.btnTurnRight.Name = "btnTurnRight";
            this.btnTurnRight.Size = new System.Drawing.Size(82, 23);
            this.btnTurnRight.TabIndex = 3;
            this.btnTurnRight.Text = "TurnRight - D";
            this.btnTurnRight.UseVisualStyleBackColor = true;
            this.btnTurnRight.Visible = false;
            this.btnTurnRight.Click += new System.EventHandler(this.btnTurnRight_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 297);
            this.Controls.Add(this.btnTurnRight);
            this.Controls.Add(this.btnTurnLeft);
            this.Controls.Add(this.btnGenerateFigure);
            this.Controls.Add(this.lblFigure);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFigure;
        private System.Windows.Forms.Button btnGenerateFigure;
        private System.Windows.Forms.Button btnTurnLeft;
        private System.Windows.Forms.Button btnTurnRight;
    }
}

