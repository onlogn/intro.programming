﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1527
{
    class Guild
    {
        public List<int> Users { get; private set; }
        public int GuildPower { get; private set; }
        public bool IsRalph = false;
        private const int RalfId = 1;

        public Guild(int userId, int userLvl)
        {
            Users = new List<int>
            {
                userId
            };
            GuildPower = userLvl;
            if (userId == RalfId) IsRalph = true;
        }

        public Guild(Guild g)
        {
            Users = g.Users;
            GuildPower = g.GuildPower;
            IsRalph = g.IsRalph;
        }

        public Guild AddGuild(Guild a)
        {
            Users.AddRange(a.Users);
            GuildPower += a.GuildPower;
            if (a.IsRalph)
            {
                IsRalph = true;
            }
            return this;
        }
    }
}
