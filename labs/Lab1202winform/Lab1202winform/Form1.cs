﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1202winform
{
    public partial class Form1 : Form
    {
        private Matrix a;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnGenerateFigure_Click(object sender, EventArgs e)
        {
            int[,] arr = new int[,]
            {
                {0, 1, 0, 0},
                {0, 1, 0, 0},
                {0, 1, 1, 1},
                {0, 1, 0, 0},
                {0, 1, 0, 0}
            };
            a = new Matrix(arr);
            lblFigure.Text = a.ToString();

            btnTurnRight.Visible = btnTurnLeft.Visible = true;
            
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A)
            {
                btnTurnLeft.PerformClick();
            }
            else if (e.KeyCode == Keys.D)
            {
                btnTurnRight.PerformClick();
            }
        }

        private void btnTurnLeft_Click(object sender, EventArgs e)
        {
            a = a.RotateToLeft();
            lblFigure.Text = a.ToString();
        }

        private void btnTurnRight_Click(object sender, EventArgs e)
        {
            a = a.RotateToRight();
            lblFigure.Text = a.ToString();
        }
    }
}
