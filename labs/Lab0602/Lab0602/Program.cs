﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0602
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());

            int res = 0;

            while (num!=0)
            {
                res += num % 10;
                num /= 10;
            }

            Console.WriteLine(res);
        }
    }
}
