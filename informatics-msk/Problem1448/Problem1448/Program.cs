﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1448
{
    class Program
    {
        static void Main(string[] args)
        {

            int n = int.Parse(Console.ReadLine());
                string str = String.Empty;
                int lastD = n % 10;
                int sto = n % 100;

                if (sto >= 11 && sto <= 14)
                {
                    str = "bochek";
                }
                else if (n == 1 || lastD == 1)
                {
                    str = "bochka";
                }
                else if (lastD >= 2 && lastD <= 4)
                {
                    str = "bochki";
                }
                else
                {
                    str = "bochek";
                }

                Console.WriteLine($"{n} {str}");
        }
    }
}
