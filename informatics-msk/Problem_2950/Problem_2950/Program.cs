﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_2950
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int lessons = n * 45;
            int p_short = n / 2 * 5;
            int p_long = (n / 2 - (n + 1) % 2) * 15;

            int time = lessons + p_short + p_long;
            int hh = time / 60;
            int mm = time % 60;
            Console.WriteLine($"{hh+9} {mm.ToString("D2")}");



        }
    }
}
