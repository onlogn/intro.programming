﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0401
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input number: ");
            double a = double.Parse(Console.ReadLine());

            Console.WriteLine($"|{a:f4}| = {Math.Abs(a):f4}");
        }
    }
}
