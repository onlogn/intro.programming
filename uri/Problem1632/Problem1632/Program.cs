﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1632
{
    class Program
    {
        static void Main(string[] args)
        {
            string s;
            List<char> arr = new List<char>()
            {
                'a', 'A', 'E', 'e', 'i', 'I', 'O', 'o', 'S', 's'
            };
            while ((s = Console.ReadLine()) != null)
            {
                int res = 1;
                int n = int.Parse(s);
                for (int i = 0; i < n; i++)
                {
                    string str = Console.ReadLine();
                    List<int> tmp = new List<int>();
                    for (int j = 0; j < str.Length; j++)
                    {
                        tmp.Add(arr.Contains(str[j]) ? 3 : 2);
                    }
                    Console.WriteLine(tmp.Aggregate((x,y) => x*y));
                }
            }
        }
    }
}
