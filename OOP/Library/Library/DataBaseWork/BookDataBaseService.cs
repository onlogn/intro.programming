﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Library.DataBaseModels;
using System.Data.Entity;

namespace Library.DataBaseWork
{
    public static class BookDataBaseService
    {

        public static async Task<Book> AddBook(Book book)
        {
            using (LibraryContext db = new LibraryContext())
            {
                BookData tmp = new BookData();
                tmp.Name = book.Name;
                //tmp.Category = await db.Categories.FirstAsync(i => i.Id == book.Category.Id);
                tmp.CategoryId = book.Category.Id;

                db.Books.Add(tmp);
                await db.SaveChangesAsync();
                return new Book(tmp.Id, tmp.Name, book.Category);
            }
        }

        public static async Task EditBook(Book book)
        {
            using (LibraryContext db = new LibraryContext())
            {
                BookData res = await db.Books.FirstAsync(item => item.Id == book.Id);
                
                if(res == null) throw new Exception("This book is not exist!");
                
                res.Name = book.Name;
                res.CategoryId = book.Category.Id;
                //res.Category = await db.Categories.FirstAsync(item => item.Id == book.Category.Id);

                await db.SaveChangesAsync();
               
            }
        }

        public static async Task<List<Book>> GetBooksList()
        {
            List<BookData> tmpList;
            using (LibraryContext db = new LibraryContext())
            {
                //SELECT * FROM Books INNER JOIN Categories ON Books.Category_Id = Categories.Id
                tmpList = await db.Books.Include(item => item.Category).ToListAsync();
            }

            return tmpList.Select(item => new Book(item.Id, item.Name, new Category(item.Category.Id, item.Category.Name))).ToList();
        }

        

        public static async Task DeleteBook(Book book)
        {
            using (LibraryContext db = new LibraryContext())
            {
                BookData res = await db.Books.FindAsync(book.Id);
                if (res == null) return;
                db.Books.Remove(res);
                await db.SaveChangesAsync();
            }
        }

    }
}
