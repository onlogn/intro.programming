﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Problem1243
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = String.Empty;

            while ((str = Console.ReadLine()) != null)
            {

                string[] s = str.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                int sum = 0;
                int wordCounter = 0;
                foreach (string x in s)
                {
                    if (IsWord(x))
                    {
                        sum += WordLength(x);
                        wordCounter++;
                    }
                }

                int res = 0;
                if (wordCounter != 0)
                    res = sum / wordCounter;
                
                if (res <= 3)
                {
                    Console.WriteLine("250");
                }
                else if (res == 4 || res == 5)
                {
                    Console.WriteLine("500");
                }
                else
                {
                    Console.WriteLine("1000");
                }
            }


        }

        public static bool IsWord(string word)
        {
            bool res = false;
            int lim = word.EndsWith(".") ? word.Length - 1 : word.Length;
            for (int i = 0; i < lim; i++)
            {
                if (Char.IsLetter(word[i]))
                    res = true;
                else return false;
            }

            return res;
        }

        public static int WordLength(string s)
        {
            int len = s.Length;
            if (s.EndsWith("."))
                len -= 1;
            return len;
        }
    }
}
