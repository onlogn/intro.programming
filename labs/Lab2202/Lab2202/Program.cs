﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2202
{
    public enum TrafficLightState
    {
        Red,
        Yellow,
        Green,
        Broken
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            TrafficLight light = new TrafficLight();

            for (int i = 0; i < 20; i++)
            {
                light.Show();
                light.Next();
            }
        }
    }

    public class TrafficLight
    {
        private TrafficLightState _currentState = TrafficLightState.Red;
        private Random _rnd = new Random();

        public void Show()
        {
            switch (_currentState)
            {
                case TrafficLightState.Broken:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("#");
                    Console.WriteLine("#");
                    Console.WriteLine("#");
                    break;
                case TrafficLightState.Red:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("#");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("#");
                    Console.WriteLine("#");
                    break;
                case TrafficLightState.Yellow:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("#");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("#");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("#");
                    break;
                case TrafficLightState.Green:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("#");
                    Console.WriteLine("#");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("#");
                    break;
            }
            Console.WriteLine("---");
        }

        public void Next()
        {
            if (_rnd.Next(1, 11) == 1)
                _currentState = TrafficLightState.Broken;

            switch (_currentState)
            {
                case TrafficLightState.Red:
                    _currentState = TrafficLightState.Yellow;
                    break;
                case TrafficLightState.Yellow:
                    _currentState = TrafficLightState.Green;
                    break;
                case TrafficLightState.Green:
                    _currentState = TrafficLightState.Red;
                    break;

            }
        }
    }
}
